<?php

use App\Http\Controllers\API\AuthController;
use App\Http\Controllers\API\CompanyCloseScheduleController;
use App\Http\Controllers\API\ExecutiveTransactionController;
use App\Http\Controllers\API\InitialController;
use App\Http\Controllers\API\OwnerCompanyController;
use App\Http\Controllers\API\OwnerTransactionController;
use App\Http\Controllers\API\ProfileController;
use App\Http\Controllers\API\StaffBalanceController;
use App\Http\Controllers\API\StaffCompanyController;
use App\Http\Controllers\API\StaffController;
use App\Http\Controllers\API\StaffTransactionController;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('artisan')->group(function () {
    Route::get('/cache', function () {
        Artisan::call('optimize');
        dd("Cache cleared");
    });
});


Route::prefix('staff')->group(function () {
    Route::prefix("password")->group(function () {
        Route::post('send-email-forgot-password', [\App\Http\Controllers\Auth\ForgotPasswordController::class, 'sendResetLinkEmail']);
    });
    Route::get('initial', [InitialController::class, 'index']);
    Route::post('/login', [AuthController::class, 'loginStaff']);
    Route::middleware(['auth:api'])->group(function () {
        Route::get('/profile', [StaffController::class, 'show']);
        Route::patch('/profile', [StaffController::class, 'update']);

        Route::prefix('/company')->group(function () {
            Route::get('/', [StaffCompanyController::class, 'index']);
            Route::get('/{company}', [StaffCompanyController::class, 'show']);
        });

        Route::prefix('/balance')->group(function () {
            Route::get('/', [StaffBalanceController::class, 'index']);
            Route::post('/', [StaffBalanceController::class, 'store']);
        });

        // Transaction
        Route::prefix('/transaction')->group(function () {
            Route::get('/', [StaffTransactionController::class, 'index']);
            Route::post('/', [StaffTransactionController::class, 'store']);
            Route::post('/bulk', [StaffTransactionController::class, 'bulkStoreTransaction']);
            Route::post('/instalment',[StaffTransactionController::class, 'storeInstalmentTransaction']);
        });
    });

    Route::prefix('report')->group(function () {
        Route::get('daily', [ExecutiveTransactionController::class, 'getDailyTransaction']);
        Route::get('monthly', [ExecutiveTransactionController::class, 'getMonthlyTransaction']);
        Route::get('staff', [ExecutiveTransactionController::class, 'getStaffTransaction']);
        Route::get('staff/{id}', [ExecutiveTransactionController::class, 'getStaffDetailTransaction']);
    });
});

Route::prefix('owner')->group(function () {
    Route::get('initial', [InitialController::class, 'index']);
    Route::post('/login', [AuthController::class, 'login']);
    Route::prefix("password")->group(function () {
        Route::post('send-email-forgot-password', [\App\Http\Controllers\Auth\ForgotPasswordController::class, 'sendResetLinkEmail']);
    });

    Route::post('/register', [AuthController::class, 'registerOwner']);
    Route::middleware(['auth:api'])->group(function () {
        Route::get('/profile', [ProfileController::class, 'show']);
        Route::patch('/profile', [ProfileController::class, 'update']);
        Route::prefix('company')->group(function () {
            Route::get('/', [OwnerCompanyController::class, 'index']);
            Route::post('/', [OwnerCompanyController::class, 'store']);
            Route::get('/{company}', [OwnerCompanyController::class, 'show']);
            Route::patch('/{company}', [OwnerCompanyController::class, 'update']);
            Route::delete('/{company}', [OwnerCompanyController::class, 'destroy']);
            Route::prefix('{company}/close-schedule')->group(function () {
                Route::get('/', [CompanyCloseScheduleController::class, 'index']);
                Route::delete('/{company_close_schedule}', [CompanyCloseScheduleController::class, 'destroy']);
                Route::post('/', [CompanyCloseScheduleController::class, 'store']);
                Route::patch('/{companyCloseSchedule}', [CompanyCloseScheduleController::class, 'update']);
            });
        });
        Route::prefix('transaction')->group(function () {
            Route::get('/', [OwnerTransactionController::class, 'index']);
        });
    });
});

Route::get('check', [\App\Http\Controllers\API\TestingController::class, 'checkTransaction']);
Route::get("migrateCompanySubscription", [\App\Http\Controllers\API\TestingController::class, 'migrateCompanySubscription']);


// Route::resource('company', CompanyController::class);
//Route::resource('profile', ProfileController::class);
