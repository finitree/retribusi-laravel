<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();
Route::get('company-export/{id}', [\App\Http\Controllers\API\TestingController::class, 'company_export'])->name('company-export');
Route::get('company-agreement-export/{id}', [\App\Http\Controllers\API\TestingController::class, 'company_agreement_export'])->name('company-agreement-export');
Route::get('transaction-pdf', [\App\Http\Controllers\API\TestingController::class, 'transaction_pdf']);
Route::get("migrate-company-id", [\App\Http\Controllers\API\TestingController::class, 'migrate_company_id']);
Route::middleware('auth')->group(function () {
    Route::get('/', \App\Http\Livewire\Home\Index::class)->name("index");
    Route::get('/home', \App\Http\Livewire\Home\Index::class)->name('home');
    Route::view('about', 'about')->name('about');

    Route::get('users', [\App\Http\Controllers\UserController::class, 'index'])->name('users.index');


//    Route::resource('staff', AdminStaffController::class);
//    Route::resource('company', AdminCompanyController::class);
    //    Route::resource("banjar", BanjarController::class);
    Route::prefix('banjar')->group(function () {
        Route::get('/', \App\Http\Livewire\Banjar\Index::class)->name('banjar.index');
    });
    Route::prefix('company-type')->group(function () {
        Route::get('/', \App\Http\Livewire\CompanyType\Index::class)->name('company_type.index');
    });
    Route::prefix('subscription')->group(function () {
        Route::get('/', \App\Http\Livewire\Subscription\Index::class)->name('subscription.index');
    });
    Route::prefix("balance")->group(function () {
        Route::get("/", \App\Http\Livewire\Balance\Index::class)->name("balance.index");
    });
    Route::prefix("transaction")->group(function () {
        Route::get("/", \App\Http\Livewire\Transaction\Index::class)->name("transaction.index");
    });

    Route::prefix("executive")->group(function () {
        Route::get("/", \App\Http\Livewire\Executive\Index::class)->name("executive.index");
    });

    Route::prefix("owner")->group(function () {
        Route::get("/", \App\Http\Livewire\Owner\Index::class)->name("owner.index");
        Route::get("/create", \App\Http\Livewire\Owner\Create::class)->name("owner.create");
        Route::get("/{id}/edit", \App\Http\Livewire\Owner\Edit::class)->name("owner.edit");

    });

    Route::prefix('staff')->group(function () {
        Route::get('/', \App\Http\Livewire\Petugas\Index::class)->name('staff.index');
        Route::get('/create', \App\Http\Livewire\Petugas\Create::class)->name('staff.create');
        Route::get('/{id}/edit', \App\Http\Livewire\Petugas\Edit::class)->name('staff.edit');
        Route::get('/mapping', \App\Http\Livewire\Petugas\Mapping::class)->name("staff.mapping");
    });

    Route::prefix('company')->group(function () {
        Route::get('/', \App\Http\Livewire\Company\Index::class)->name('company.index');
        Route::get('/create', \App\Http\Livewire\Company\Form::class)->name('company.create');
        Route::get('/{id?}/edit', \App\Http\Livewire\Company\Form::class)->name('company.edit');
    });

    Route::prefix("profile")->group(function () {
        Route::get('/', \App\Http\Livewire\Profile\Index::class)->name("profile.index");
    });

    Route::prefix("setting")->group(function () {
        Route::get('/', \App\Http\Livewire\Setting\Index::class)->name("setting.index");
    });
});
