<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
    <title>{{ $title ?? "Admin"}}</title>

    <!-- General CSS Files -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://kit.fontawesome.com/b34d9ac5d0.js" crossorigin="anonymous"></script>

    <!-- CSS Libraries -->
    {{--    <link rel="stylesheet" href="../../../node_modules/jqvmap/dist/jqvmap.min.css">--}}
    {{--    <link rel="stylesheet" href="../../../node_modules/summernote/dist/summernote-bs4.css">--}}
    {{--    <link rel="stylesheet" href="../../../node_modules/owl.carousel/dist/assets/owl.carousel.min.css">--}}
    {{--    <link rel="stylesheet" href="../../../node_modules/owl.carousel/dist/assets/owl.theme.default.min.css">--}}

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.22/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/pikaday/css/pikaday.css">
    <link rel="stylesheet" href="{{asset('admin/select2/dist/css/select2.css')}}">

    <!-- Template CSS -->
    <link rel="stylesheet" href="{{ asset('admin/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('admin/css/components.css') }}">
    <style>
        .text-base {
            font-size: 1rem !important; /* 16px */
            line-height: 1.5rem; /* 24px */
        }
    </style>
    {{-- Leaflet MAP --}}
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.8.0/dist/leaflet.css"
          integrity="sha512-hoalWLoI8r4UszCkZ5kL8vayOGVae1oxXe/2A4AO6J9+580uKHDO3JdHb7NzwwzK5xr/Fs0W40kiNHxM9vyTtQ=="
          crossorigin=""/>
    @stack('css')
    @livewireStyles
</head>

<body>
<div id="app">
    <div class="main-wrapper">
        <div class="navbar-bg"></div>
        <nav class="navbar navbar-expand-lg main-navbar">
            <form class="form-inline mr-auto">
                <ul class="navbar-nav mr-3">
                    <li><a href="#" data-toggle="sidebar" class="nav-link nav-link-lg"><i
                                class="fas fa-bars"></i></a></li>
                    <li><a href="#" data-toggle="search" class="nav-link nav-link-lg d-sm-none"><i
                                class="fas fa-search"></i></a></li>
                </ul>

            </form>
            {{--            <ul class="navbar-nav navbar-right">--}}
            {{--                <li class="dropdown"><a href="#" data-toggle="dropdown"--}}
            {{--                                        class="nav-link dropdown-toggle nav-link-lg nav-link-user">--}}

            {{--                        <div class="d-sm-none d-lg-inline-block">{{auth()->user()->name}}</div>--}}
            {{--                    </a>--}}
            {{--                    <div class="dropdown-menu dropdown-menu-right">--}}

            {{--                        --}}{{-- <a href="features-profile.html" class="dropdown-item has-icon">--}}
            {{--                            <i class="far fa-user"></i> Profile--}}
            {{--                        </a>--}}

            {{--                        <div class="dropdown-divider"></div> --}}
            {{--                        <a href="{{ route('logout') }}" onclick="event.preventDefault();--}}
            {{--        document.getElementById('logout-form').submit();" class="dropdown-item has-icon text-danger">--}}
            {{--                            <i class="fas fa-sign-out-alt"></i> Logout--}}
            {{--                        </a>--}}
            {{--                        <form id="logout-form" action="{{ route('logout') }}" method="POST"--}}
            {{--                              style="display: none;">--}}
            {{--                            @csrf--}}
            {{--                        </form>--}}
            {{--                    </div>--}}
            {{--                </li>--}}
            {{--            </ul>--}}

            <livewire:appbar.profile/>
        </nav>
    @include('layouts.sidebar')

    <!-- Main Content -->
        <div class="main-content">
            {{$slot}}
        </div>
        <footer class="main-footer">
            <div class="footer-left">
                Copyright &copy; 2022
            </div>
        </footer>
    </div>
</div>

<!-- General JS Scripts -->
<script src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<script src="https://kit.fontawesome.com/b34d9ac5d0.js" crossorigin="anonymous"></script>
<script defer src="https://unpkg.com/alpinejs@3.x.x/dist/cdn.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/apexcharts"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
</script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.nicescroll/3.7.6/jquery.nicescroll.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/pikaday/pikaday.js"></script>
<script src="{{ asset('admin/js/stisla.js') }}"></script>
<!-- JS Libraies -->
{{--<script src="../../../node_modules/jquery-sparkline/jquery.sparkline.min.js"></script>--}}
{{--<script src="../../../node_modules/chart.js/dist/Chart.min.js"></script>--}}
{{--<script src="../../../node_modules/owl.carousel/dist/owl.carousel.min.js"></script>--}}
{{--<script src="../../../node_modules/summernote/dist/summernote-bs4.js"></script>--}}
{{--<script src="../../../node_modules/chocolat/dist/js/jquery.chocolat.min.js"></script>--}}
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.js">
</script>
<script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>
<script src="{{asset("admin/select2/dist/js/select2.min.js")}}"></script>

<!-- Template JS File -->
<script src="{{ asset('admin/js/scripts.js') }}"></script>
<script src="{{ asset('admin/js/custom.js') }}"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
{{-- leaflet map --}}
<script src="https://unpkg.com/leaflet@1.8.0/dist/leaflet.js"
        integrity="sha512-BB3hKbKWOc9Ez/TAwyWxNXeoV9c1v6FIeYiBieIWkpLjauysF18NzgR1MBNBXf8/KABdlkX68nAhlwcDFLGPCQ=="
        crossorigin=""></script>

@livewireScripts
@livewireChartsScripts
<script>
    window.addEventListener('swal:toast', event => {
        const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 3000,
            timerProgressBar: true,
            didOpen: (toast) => {
                toast.addEventListener('mouseenter', Swal.stopTimer)
                toast.addEventListener('mouseleave', Swal.resumeTimer)
            }
        });
        Toast.fire({
            icon: event.detail.type,
            title: event.detail.title
        })
    })

    window.addEventListener("swal:logoutConfirmation", event => {
        Swal.fire({
            title: "Konfirmasi Logout",
            text: "Apakah anda yakin ingin keluar dari akun ini?",
            icon: "warning",
            showCancelButton: true,
        }).then((value) => {
            if (value.isConfirmed) {
                window.livewire.emit('confirmLogout');
            }
        })

    })


    window.addEventListener("swal:deleteConfirmation", event => {
        Swal.fire({
            title: "Konfirmasi Hapus",
            text: "Apakah anda yakin ingin menghapus data ini?",
            icon: "warning",
            showCancelButton: true,
        }).then((value) => {
            if (value.isConfirmed) {
                window.livewire.emit('confirm', event.detail.id);
            }
        })

    })

    window.addEventListener("swal:declineConfirmation", event => {
        Swal.fire({
            title: "Konfirmasi Ditolak",
            text: "Apakah anda yakin ingin menolak data ini?",
            icon: "warning",
            showCancelButton: true,
        }).then((value) => {
            if (value.isConfirmed) {
                window.livewire.emit('decline', event.detail.id);
            }
        })

    })
</script>

<script>
    window.addEventListener('formModal:close', event => {
        $('#formModal').modal('hide');
    })
    window.addEventListener('editModal:close', event => {
        $('#editModal').modal('hide');
    })

</script>


@stack('js')

</body>

</html>
