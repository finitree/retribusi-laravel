<div class="main-sidebar">
    <aside id="sidebar-wrapper">
        <div class="sidebar-brand">
            <a href="{{route("index")}}">Dudukan PAD</a>
        </div>
        <div class="sidebar-brand sidebar-brand-sm">
            <a href="{{route("index")}}">DP</a>
        </div>
        <ul class="sidebar-menu">
            <li class="menu-header">Dashboard</li>
            <li class="@if (request()->routeIs('home') || request()->routeIs("index")) active @endif"><a
                    class="nav-link"
                    href="{{ route('home') }}"><i class="fas fa-fire"></i>
                    <span>Dashboard</span></a></li>

            <li class="menu-header">Transaksi</li>
            <li class="@if (request()->routeIs('balance.index') ) active @endif"><a
                    class="nav-link"
                    href="{{ route('balance.index') }}">
                    <i class="fa-solid fa-cash-register"></i>
                    <span>Setoran Petugas</span></a></li>

            <li class="@if (request()->routeIs('transaction.index') ) active @endif"><a
                    class="nav-link"
                    href="{{ route('transaction.index') }}">
                    <i class="fas fa-money-bill-transfer"></i>
                    <span>Laporan Dudukan</span></a>
            </li>

            {{-- <li class="nav-item dropdown active">
                <a href="#" class="nav-link has-dropdown"><i class="fas fa-fire"></i><span>Dashboard</span></a>
                <ul class="dropdown-menu">
                    <li><a class="nav-link" href="index-0.html">General Dashboard</a></li>
                    <li class="active"><a class="nav-link" href="index.html">Ecommerce
                            Dashboard</a></li>
                </ul>
            </li> --}}
            <li class="menu-header">Master Data</li>
            <li class="@if (request()->routeIs('banjar.index')) active @endif"><a class="nav-link"
                                                                                  href="{{ route('banjar.index') }}">
                    <i class="fas fa-synagogue"></i>
                    <span>Banjar</span></a></li>
            <li class="@if (request()->routeIs('company_type.index')) active @endif"><a class="nav-link"
                                                                                        href="{{ route('company_type.index') }}">
                    <i class="fas fa-table-cells-large"></i> <span>Jenis Usaha</span></a></li>

            <li class="@if (request()->routeIs('subscription.index')) active @endif"><a class="nav-link"
                                                                                        href="{{ route('subscription.index') }}">
                    <i class="fas fa-money-bills"></i>
                    <span>Iuran</span></a></li>

            <li class="menu-header">User</li>
            <li class="@if (request()->route()->getPrefix() == "owner") active @endif">
                <a class="nav-link" href="{{ route('owner.index') }}">
                    <i class="fas fa-user-group"></i>
                    <span>Pemilik Usaha</span></a>
            </li>
            {{-- <li class=""><a class="nav-link" href="{{ route('home') }}">
                    <i class="fas fa-user-gear"></i>
                    <span>Petugas</span></a></li> --}}
            <li class="nav-item dropdown @if(request()->route()->getPrefix() == "staff") active @endif">
                <a href="#" class="nav-link has-dropdown "><i class="fas fa-user-gear"></i>
                    <span>Petugas</span></a>
                <ul class="dropdown-menu">
                    <li class="@if(request()->routeIs("staff.mapping")) active @endif"><a class="nav-link"
                                                                                          href="{{route("staff.mapping")}}">Mapping
                            Petugas</a></li>
                    <li class="@if(request()->route()->getPrefix() == "staff" && !request()->routeIs("staff.mapping")) active @endif">
                        <a class="nav-link" href="{{route("staff.index")}}">Data Petugas</a>
                    </li>
                </ul>
            </li>
            <li class="@if (request()->routeIs('executive.index')) active @endif">
                <a class="nav-link"
                   href="{{ route('executive.index') }}">
                    <i class="fas fa-user-tie"></i>
                    <span>Atasan</span></a>
            </li>

            <li class="menu-header">Company</li>
            <li class="@if(request()->route()->getPrefix() == "company") active @endif">
                <a class="nav-link" href="{{ route('company.index') }}">
                    <i class="fas fa-building"></i>
                    <span>Usaha</span></a></li>


            <li class="menu-header">Setting</li>
            <li class="@if(request()->route()->getPrefix() == "setting") active @endif">
                <a class="nav-link" href="{{ route('setting.index') }}">
                    <i class="fas fa-gear"></i>
                    <span>Pengaturan</span></a></li>
            {{--            <li class="menu-header">Profile</li>--}}
            {{--            <li class="@if(request()->routeIs("profile.index")) active @endif">--}}
            {{--                <a class="nav-link" href="{{ route('profile.index') }}">--}}
            {{--                    <i class="fas fa-user"></i>--}}
            {{--                    <span>Profile</span></a></li>--}}

        </ul>

        <div class="mt-4 mb-4 p-3 hide-sidebar-mini">
            {{--            <a href="https://getstisla.com/docs" class="btn btn-primary btn-lg btn-block btn-icon-split">--}}
            {{--                <i class="fas fa-rocket"></i> Documentation--}}
            {{--            </a>--}}
        </div>
    </aside>
</div>
