@props(['placeholder' => 'Select Options', 'id'])

<div wire:ignore>
    <select {{ $attributes }} id="{{ $id }}" multiple="multiple" data-placeholder="{{ $placeholder }}"
            style="width: 100%;">
        {{ $slot }}
    </select>
</div>

@once
    @push('styles')
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
              integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
              crossorigin="anonymous">

        <!-- Select2 -->
        <link rel="stylesheet" href="{{asset('admin/select2/dist/css/select2.css')}}">

        <!-- Template CSS -->
        <link rel="stylesheet" href="{{ asset('admin/css/style.css') }}">
        <link rel="stylesheet" href="{{ asset('admin/css/components.css') }}">
    @endpush
@endonce

@once
    @push('js')
        <!-- Select2 -->
        <script src="{{asset("admin/select2/dist/js/select2.min.js")}}"></script>

        <!-- Template JS File -->
        <script src="{{ asset('admin/js/scripts.js') }}"></script>
        <script src="{{ asset('admin/js/custom.js') }}"></script>
    @endpush
@endonce

@push('js')
    <script>
        $(function () {
            $('#{{ $id }}').select2()
            .on('change', function(){

            })
        })
    </script>
@endpush
