<input
    style="background-color: white; {{$attributes['style']}}"
    readonly="readonly"
    x-data
    x-ref="input"
    {{--        x-init="new Pikaday({ field: $refs.input, 'format': 'MM/DD/YYYY', firstDay: 1, maxDate: new Date(), });"--}}
    x-init="new Pikaday({ field: $refs.input, maxDate: new Date(),'format': 'YYYY-MM-DD' ,onSelect: function (date) { $dispatch('input', moment(date.toString()).format('YYYY-MM-DD')) }  })"
    type="text"
    {{ $attributes }}
>
