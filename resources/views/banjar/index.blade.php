@extends('layouts.admin')
@section("title","Banjar")
@section("content")

    <div class="modal fade" tabindex="-1" role="dialog" id="createModal">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <form action="{{route("banjar.store")}}" method="post">
                    @csrf
                    <div class="modal-header">
                        <h5 class="modal-title">Tambah Banjar</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Banjar</label>
                            <input type="text" class="form-control" required name="banjar">
                        </div>
                        <div class="form-group">
                            <label>Alamat</label>
                            <textarea class="form-control" name="address" required rows="10"></textarea>
                        </div>
                    </div>
                    <div class="modal-footer bg-whitesmoke br">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <section class="section">

        <div class="section-header">
            <h1>Banjar</h1>
            {{-- <div class="section-header-breadcrumb">
                <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
                <div class="breadcrumb-item"><a href="#">Layout</a></div>
                <div class="breadcrumb-item">Default Layout</div>
            </div> --}}
        </div>

        <div class="section-body">
            <div class="card">
                <div class="card-body p-4">
                    <button class="btn btn-success" data-toggle="modal" data-target="#createModal">+ Tambah Banjar
                    </button>
                    <div class="table-responsive mt-4">
                        <table class="table table-striped" id="table-data">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Banjar</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($banjars as $banjar)
                                <tr>
                                    <td>{{$loop->iteration}}</td>
                                    <td>{{$banjar->name}}</td>
                                    <td>
                                        <button class="btn btn-warning"><i class="fa-solid fa-pen-to-square"></i>
                                        </button>
                                        <button class="btn btn-danger"><i class="fa fa-trash"></i></button>
                                    </td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>


@endsection

@section("js")
    <script>
        $(document).ready(function () {
            $('#table-data').DataTable();
        });
    </script>

@endsection
