<html>
<head>
    <title>Usaha</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>

<div class="container">
    <center>
        <h4 class="p-0 m-0">{{$company->name}}</h4>
        @if($company->status == \App\Models\Company::WAIT_VERIFIED)
            <p>(Menunggu Verifikasi)</p>
        @elseif($company->status == \App\Models\Company::VERIFIED)
            <p>(Terverifikasi)</p>
        @else
            <p>(Tidak Terverifikasi)</p>
        @endif
    </center>
    <br/>
    <table class='table table-bordered'>
        <tbody>
        <tr>
            <td>Nama</td>
            <td>{{$company->name}}</td>
        </tr>

        <tr>
            <td>Pemilik usaha</td>
            <td>{{$company->user->name}}</td>
        </tr>

        <tr>
            <td>Telepon</td>
            <td>{{$company->user->phone}}</td>
        </tr>

        <tr>
            <td>Alamat</td>
            <td>{{$company->address}}</td>
        </tr>

        <tr>
            <td>Jenis Usaha</td>
            <td>{{$company->company_type->type}}</td>
        </tr>

        <tr>
            <td>Tipe Pembayaran</td>
            <td>{{$company->subscription->subscription_type->category}}</td>
        </tr>

        <tr>
            <td>Nominal Pembayaran</td>
            <td>@currency($company->subscription->subscription_amount)</td>
        </tr>

        </tbody>
    </table>

    <div class="mt-4">
        <p>Foto</p>
        <img src="{{$company->photos}}" class="img-fluid">
    </div>

</div>

</body>
</html>
