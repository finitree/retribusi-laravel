<html >
<head>
    <title>Usaha</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>

<div class="container" style="font-size: 12px">
    <center>
        <h4 class="p-0 m-0">{{$company->name}}</h4>
    </center>
    <br/>
    <table class='table table-bordered' style="margin-top: 10px">
        <tbody>
        <tr>
            <td>Nama</td>
            <td>{{$company->name}}</td>
        </tr>

        <tr>
            <td>Pemilik usaha</td>
            <td>{{$company->user->name}}</td>
        </tr>

        <tr>
            <td>Telepon</td>
            <td>{{$company->user->phone}}</td>
        </tr>

        <tr>
            <td>Alamat</td>
            <td>{{$company->address}}</td>
        </tr>

        <tr>
            <td>Jenis Usaha</td>
            <td>{{$company->company_type->type}}</td>
        </tr>

        <tr>
            <td>Tipe Pembayaran</td>
            <td>{{$company->subscription->subscription_type->category}}</td>
        </tr>
        <tr>
            <td>Tipe Pembayaran Disetujui</td>
            <td></td>
        </tr>

        <tr>
            <td>Nominal Pembayaran</td>
            <td>@currency($company->subscription->subscription_amount)</td>
        </tr>
        <tr>
            <td>Nominal Pembayaran Disetujui</td>
            <td>Rp.</td>
        </tr>


        </tbody>
    </table>

    <div class="mt-4">
        <p>Foto</p>
        <img src="{{$company->photos}}" class="img-fluid" style="max-height: 150px">
    </div>

    <div class="mt-4">
        <p>Sebagai bukti sahnya persetujuan di atas, surat perjanjian ini ditandatangani oleh kedua belah pihak</p>

        <p class="m-0" style="margin-top: 40px">Hari, Tanggal</p>
        <p style="margin-top: 10px">________, _________________________</p>
        <div style="margin-top: 120px"></div>
        <table class='table table-borderless' border="0" style="border: 0">
            <tbody>
            <tr>
                <td width="210">
                    <div class="d-flex-column align-items-center justify-content-center">
                        <p class="m-0">Prajuru Desa</p>
                        <p></p>
                    </div>

                </td>
                <td style="text-align: center">
                    <div class="d-flex-column align-items-center justify-content-center" style="margin-left: 20px">
                        <p class="m-0">Pemilik Usaha</p>
                        <p>{{$company->user->name}}</p>
                    </div>
                </td>
            </tr>

            </tbody>
        </table>
    </div>

</div>

</body>
</html>
