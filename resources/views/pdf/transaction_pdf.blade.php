<html>
<head>
    <title>Dudukan PAD</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>

<div class="container">
    <center>
        <h4>Laporan Dudukan PAD</h4>
    </center>
    <br/>
    <table class='table table-bordered'>
        <thead>
        <tr>
            <th>No</th>
            <th>Petugas</th>
            <th>Usaha</th>
            <th>Tanggal</th>
            <th>Total Nominal</th>
        </tr>
        </thead>
        <tbody>
        @foreach($transactions as $transaction)
            <tr>
                <td>{{ $loop->iteration }}</td>
                <td>{{$transaction->staff->user->name}}</td>
                <td>
                    @if($transaction->detail)
                        @foreach($transaction->detail->unique('company_id') as $detail)
                            <li>
                                {{$detail->company->name}} - {{$detail->company->user->name}}
                            </li>
                        @endforeach
                    @endif
                </td>
                <td>{{$transaction->created_at->format("d M Y")}}</td>
                <td>@currency($transaction->total_amount)</td>
            </tr>
        @endforeach
        <tr>
            <td colspan="5">
                Total
            </td>
            <td>
                @currency($transactions->sum('total_amount'))
            </td>
        </tr>
        </tbody>
    </table>

</div>

</body>
</html>
