@extends('layouts.app')

@section('content')
    <!-- ========== title-wrapper start ========== -->
    <div class="title-wrapper pt-30">
        <div class="row align-items-center">
            <div class="col-md-6">
                <div class="title mb-30">
                    <h2>Registrasi Petugas Retribusi</h2>
                </div>
            </div>
            <!-- end col -->
        </div>
        <!-- end row -->
    </div>
    <!-- ========== title-wrapper end ========== -->

    <div class="card-styles">
        <div class="card-style-1 mb-30">
            <div class="card-content">
                <form action="{{route('staff.store')}}" method="post">
                    @csrf
                <div class="input-style-1">
                    <label>Nama</label>
                    <input required name="name" type="text" placeholder="Nama" />
                  </div>

                  <div class="input-style-1">
                    <label>Email</label>
                    <input required name="email" type="email" placeholder="Email" />
                  </div>

                  <div class="input-style-1">
                    <label>Telepon</label>
                    <input required name="phone" type="phone" placeholder="Telepon" />
                  </div>

                  <div class="input-style-1">
                    <label>Password</label>
                    <input required name="password" type="password" placeholder="Password" />
                  </div>

                  <div class="select-style-1">
                    <label>Banjar</label>
                    <div class="select-position">
                      <select required name="banjar_id">
                        <option value="">Pilih banjar</option>
                        @foreach ($banjars as $item)
                            <option value="{{$item->id}}">{{$item->name}}</option>
                        @endforeach

                      </select>
                    </div>
                  </div>

                  <button type="submit" class="main-btn primary-btn btn-hover mt-3">Simpan</button>
                </form>
            </div>
        </div>
    </div>
@endsection
