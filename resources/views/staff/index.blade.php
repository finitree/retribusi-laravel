@extends('layouts.app')

@section('content')
    <!-- ========== title-wrapper start ========== -->
    <div class="title-wrapper pt-30">
        <div class="row align-items-center">
            <div class="col-md-6">
                <div class="title mb-30">
                    <h2>Petugas Retribusi</h2>
                </div>
            </div>
            <!-- end col -->
        </div>
        <!-- end row -->
    </div>
    <!-- ========== title-wrapper end ========== -->

    <div class="card-styles">
        <div class="card-style-3 mb-30">
            <div class="card-content">
                <div class="table-wrapper table-responsive">
                    <a href="{{route('staff.create')}}">
                        <button class="btn btn-success">+ Tambah</button>
                    </a>
                    <table class="table striped-table mt-3">
                        <thead>
                        <tr>
                            <th><h6>Nama</h6></th>
                            <th><h6>Email</h6></th>
                            <th>Banjar</th>
                            <th>Saldo</th>
                            <th><h6>Aksi</h6></th>
                        </tr>
                        <!-- end table row-->
                        </thead>
                        <tbody>
                        @foreach($staffs as $user)
                            <tr>
                                <td>
                                    <p>{{ $user->user->name }}</p>
                                </td>
                                <td>
                                    <p>{{ $user->user->email }}</p>
                                </td>
                                <td>
                                    <p>{{$user->banjar->name}}</p>
                                </td>
                                <td>
                                    <form action="{{route('staff.destroy',['staff' => $user])}}" method="POST">
                                        @method("DELETE")
                                        @csrf
                                        <button class="btn btn-danger" type="submit"
                                                onclick="return confirm('Apakah anda yakin?')">Hapus
                                        </button>
                                    </form>

                                </td>
                            </tr>
                        @endforeach
                        <!-- end table row -->
                        </tbody>
                    </table>
                    <!-- end table -->

                    {{ $staffs->links() }}
                </div>

            </div>
        </div>
    </div>
@endsection
