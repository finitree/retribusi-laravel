<div>
    <section class="section">

        <div class="section-header">
            <h1>Pemilik Usaha</h1>
        </div>

        <div class="section-body">
            @include("common.alert")
            <div class="card">
                <div class="card-header">
                    <h4>
                        @if(auth()->user()->isAdmin)
                            <a href="{{route("owner.create")}}">
                                <button class="btn btn-success">
                                    + Tambah Pemilik Usaha
                                </button>
                            </a>
                        @endif
                    </h4>
                    <div class="card-header-action">
                        <div class="input-group">
                            <input type="text" class="form-control" wire:model="search" placeholder="Search">
                            <div class="input-group-btn">
                                <button class="btn btn-primary"><i class="fas fa-search"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body p-0">
                    <div class="table-responsive">
                        <table class="table table-striped" id="table-data">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama</th>
                                <th>Telepon</th>
                                <th>Usaha</th>
                                @if(auth()->user()->isAdmin)
                                    <th>Action</th>
                                @endif
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($owners as $owner)
                                <tr>
                                    <td>{{ ($owners->currentpage()-1) * $owners->perpage() + $loop->index + 1 }}</td>
                                    <td>{{$owner->name}}</td>
                                    <td>{{$owner->phone}}</td>
                                    <td>
                                        <ul class="p-0">
                                            @foreach($owner->companies as $company)
                                                <li>
                                                    {{$company->name}}
                                                </li>
                                            @endforeach
                                        </ul>
                                    </td>
                                    @if(auth()->user()->isAdmin)
                                        <td>
                                            <a href="{{route("owner.edit",["id"=>$owner->id])}}"
                                               class="btn btn-primary m-1">
                                                <i class="fa-solid fa-eye"></i>
                                            </a>

                                            <a href="{{route("owner.edit",["id"=>$owner->id])}}"
                                               class="btn btn-warning m-1">
                                                <i class="fa-solid fa-pen-to-square"></i>
                                            </a>

                                            <button class="btn btn-danger m-1"
                                                    wire:click.prevent="deleteConfirmation({{$owner->id}})"><i
                                                    class="fa fa-trash"></i></button>
                                        </td>
                                    @endif
                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                        <div class="float-right mx-4">
                            {{$owners->links()}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@push("js")
    <script>
        Livewire.on('editModal:open', event => {
            $('#editModal').modal('show');
        })
    </script>
@endpush
