<div>
    <section class="section">
        <div class="section-header">
            <h1>Tambah Pemilik Usaha</h1>
            {{-- <div class="section-header-breadcrumb">
                <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
                <div class="breadcrumb-item"><a href="#">Layout</a></div>
                <div class="breadcrumb-item">Default Layout</div>
            </div> --}}
        </div>

        <div class="section-body">

            <div class="section-body">
                <div class="card">
                    <form wire:submit.prevent="post()">

                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Nama</label>
                                        <input wire:model.defer="name" type="text"
                                               class="form-control @error("name") is-invalid @enderror">
                                        @error("name")
                                        <div class="invalid-feedback">
                                            <strong>{{ $message }}</strong>
                                        </div>
                                        @enderror
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Telepon</label>
                                        <input
                                            wire:model.defer="phone"
                                            type="text"
                                            class="form-control @error("phone") is-invalid @enderror">
                                        @error("phone")
                                        <div class="invalid-feedback" style="display: block;">
                                            <strong>{{ $message}}</strong>
                                        </div>
                                        @enderror
                                    </div>
                                </div>


                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Email</label>
                                        <input
                                            wire:model.defer="email"
                                            type="email"
                                            class="form-control @error("email") is-invalid @enderror">
                                        @error("email")
                                        <div class="invalid-feedback">
                                            <strong>{{ $message }}</strong>
                                        </div>
                                        @enderror
                                    </div>
                                </div>


                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Kata Sandi</label>
                                        <input wire:model.defer="password"
                                               type="password"
                                               class="form-control @error("password") is-invalid @enderror">
                                        @error("password")
                                        <div class="invalid-feedback">
                                            <strong>{{ $message }}</strong>
                                        </div>
                                        @enderror
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Foto</label>
                                        <input type="file"
                                               class="form-control @error("photo") is-invalid @enderror"
                                               accept="image/*"
                                               wire:model.defer="photo">
                                        @error("photo")
                                        <div class="invalid-feedback" style="display: block;">
                                            <strong>
                                                {{$message}}
                                            </strong>
                                        </div>
                                        @enderror

                                        <div wire:loading wire:target="photo">
                                            Uploading Image...
                                        </div>
                                        @if ($photo)
                                            <img alt="image" width="200" src="{{ $photo->temporaryUrl() }}"
                                                 class="img-fluid mt-4">
                                        @endif

                                    </div>
                                </div>


                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>KTP</label>
                                        <input type="file"
                                               class="form-control @error("ktp") is-invalid @enderror"
                                               accept="image/*"
                                               wire:model.defer="ktp">
                                        @error("ktp")
                                        <div class="invalid-feedback" style="display: block;">
                                            <strong>
                                                {{$message}}
                                            </strong>
                                        </div>
                                        @enderror


                                        <div wire:loading wire:target="ktp">
                                            Uploading Image...
                                        </div>
                                        @if ($ktp)
                                            <img alt="image" width="200" src="{{ $ktp->temporaryUrl() }}"
                                                 class="img-fluid mt-4">
                                        @endif

                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="card-footer text-right">
                            <div wire:loading wire:target="submit">
                                <img width="75" src="{{asset('svg/pulse.svg')}}" alt="">
                            </div>
                            <button type="submit" class="btn btn-primary">
                                Submit
                            </button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </section>
</div>
