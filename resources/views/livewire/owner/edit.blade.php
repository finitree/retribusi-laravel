<div>
    {{--    history modal--}}
    <div wire:ignore.self class="modal fade" tabindex="-1" role="dialog" id="showModal">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Transaksi Detail</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="table-responsive">
                        <table class="table table-striped" id="table-data">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Usaha</th>
                                <th>Nominal</th>
                                <th>Tanggal</th>
                            </tr>
                            </thead>
                            <tbody>
                            @php
                                $detail = $transactionShow ? $transactionShow->transaction_details : collect()
                            @endphp
                            @foreach($detail as $item)
                                <tr>
                                    <td>{{ $loop->iteration}}</td>
                                    <td>{{$item->company->name}}</td>
                                    <td>@currency($item->amount)</td>
                                    <td>{{ date("d M Y",strtotime($item->transaction_at))}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <section class="section">
        <div class="section-header">
            <h1>Ubah Pemilik Usaha</h1>
        </div>

        <div class="section-body">
            <div class="card">
                <form wire:submit.prevent="patch()">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Nama</label>
                                    <input wire:model.defer="name" type="text"
                                           class="form-control @error("name") is-invalid @enderror">
                                    @error("name")
                                    <div class="invalid-feedback">
                                        <strong>{{ $message }}</strong>
                                    </div>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Telepon</label>
                                    <input
                                        wire:model.defer="phone"
                                        type="text"
                                        class="form-control @error("phone") is-invalid @enderror">
                                    @error("phone")
                                    <div class="invalid-feedback" style="display: block;">
                                        <strong>{{ $message}}</strong>
                                    </div>
                                    @enderror
                                </div>
                            </div>


                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Email</label>
                                    <input
                                        wire:model.defer="email"
                                        type="email"
                                        class="form-control @error("email") is-invalid @enderror">
                                    @error("email")
                                    <div class="invalid-feedback">
                                        <strong>{{ $message }}</strong>
                                    </div>
                                    @enderror
                                </div>
                            </div>


                        </div>
                        <div class="row mt-3">
                            <div class="col-md-6" x-data="{show : false}">
                                <div class="form-group">
                                    <label class="custom-switch" style="padding-left: 0px">
                                        <input
                                            x-model="show"
                                            type="checkbox"
                                            name="custom-switch-checkbox"
                                            class="custom-switch-input"
                                        />
                                        <span class="custom-switch-indicator"></span>
                                        <span class="custom-switch-description">Ubah Kata Sandi</span>
                                    </label>
                                </div>

                                <div class="form-group" x-show="show">
                                    <label>Kata Sandi</label>
                                    <input wire:model.defer="password"
                                           type="password"
                                           class="form-control @error("password") is-invalid @enderror">
                                    @error("password")
                                    <div class="invalid-feedback">
                                        <strong>{{ $message }}</strong>
                                    </div>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Foto</label>
                                    <input type="file"
                                           class="form-control @error("photo") is-invalid @enderror"
                                           accept="image/*"
                                           wire:model.defer="photo">
                                    @error("photo")
                                    <div class="invalid-feedback" style="display: block;">
                                        <strong>
                                            {{$message}}
                                        </strong>
                                    </div>
                                    @enderror

                                    <div wire:loading wire:target="photo">
                                        Uploading Image...
                                    </div>
                                    <div wire:loading.remove wire:target="photo">
                                        @if($old_photo && !$photo)
                                            <img alt="image" width="200" src="{{ $old_photo }}"
                                                 class="img-fluid mt-4">
                                        @endif
                                        @if ($photo)
                                            <img alt="image" width="200" src="{{ $photo->temporaryUrl() }}"
                                                 class="img-fluid mt-4">
                                        @endif
                                    </div>

                                </div>
                            </div>


                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>KTP</label>
                                    <input type="file"
                                           class="form-control @error("ktp") is-invalid @enderror"
                                           accept="image/*"
                                           wire:model.defer="ktp">
                                    @error("ktp")
                                    <div class="invalid-feedback" style="display: block;">
                                        <strong>
                                            {{$message}}
                                        </strong>
                                    </div>
                                    @enderror


                                    <div wire:loading wire:target="ktp">
                                        Uploading Image...
                                    </div>

                                    <div wire:loading.remove wire:target="ktp">
                                        @if($old_ktp && !$ktp)
                                            <img alt="image" width="200" src="{{ $old_ktp}}"
                                                 class="img-fluid mt-4">

                                        @endif
                                        @if($ktp)
                                            <img alt="image" width="200" src="{{ $ktp->temporaryUrl()}}"
                                                 class="img-fluid mt-4">
                                        @endif
                                    </div>

                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="card-footer text-right">
                        <div wire:loading wire:target="patch">
                            <img width="75" src="{{asset('svg/pulse.svg')}}" alt="">
                        </div>
                        <button type="submit" class="btn btn-primary">
                            Submit
                        </button>
                    </div>

                </form>
            </div>

            <div class="card">
                <div class="card-header">
                    <h4>
                        Daftar Usaha
                        <a class="btn btn-success ml-3" href="{{route('company.create')}}">+ Tambah Usaha</a>
                    </h4>

                    <div class="card-header-action">
                        <div class="input-group">
                            <input type="text" class="form-control" wire:model="search" placeholder="Search">
                            <div class="input-group-btn">
                                <button class="btn btn-primary"><i class="fas fa-search"></i></button>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="card-body p-0">
                    <div class="table-responsive">
                        <table class="table table-striped" id="table-data">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($companies as $company)
                                <tr>
                                    <td>{{ ($companies->currentpage()-1) * $companies->perpage() + $loop->index + 1 }}</td>
                                    <td>{{$company->name}}</td>
                                    <td>
                                        @if($company->status == \App\Models\Company::WAIT_VERIFIED)
                                            <div class="badge badge-warning">Menunggu Verifikasi</div>
                                        @elseif($company->status == \App\Models\Company::VERIFIED)
                                            <div class="badge badge-primary">Terverifikasi</div>
                                        @endif
                                    </td>
                                    <td>
                                        <a class="btn btn-primary m-1"
                                           href="{{route("company.edit",["id" => $company->id])}}">
                                            <i class="fa fa-eye"></i>
                                            Detail
                                        </a>
                                    </td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                        <div class="float-right mx-4">
                            {{$companies->links()}}
                        </div>
                    </div>
                </div>
            </div>


            {{--            history--}}
            <div class="card">
                <div class="card-header">
                    <h4>
                        Riwayat Transaksi
                        {{--                        <button class="btn btn-success ml-3">+ Tambah Usaha</button>--}}
                    </h4>

                    <div class="card-header-action">
                        <div class="input-group">
                            <x-forms.date-picker
                                class="form-control"
                                wire:model="date" placeholder="Pilih tanggal"
                                autocomplete="off"/>
                            <div class="input-group-btn">
                                <button class="btn btn-danger" wire:click.prevent="removeFilter()"><i
                                        class="fas fa-times"></i></button>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="card-body p-0">
                    <div class="table-responsive">
                        <table class="table table-striped" id="table-data">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Petugas</th>
                                <th>Usaha</th>
                                <th>Nominal</th>
                                <th>Tanggal</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($transactions as $transaction)
                                <tr>
                                    <td>{{ ($transactions->currentpage()-1) * $transactions->perpage() + $loop->index + 1 }}</td>
                                    <td>
                                        {{$transaction->staff->user->name}}
                                    </td>
                                    <td>
                                        @if($transaction->detail)
                                            @foreach($transaction->detail->unique('company_id') as $detail)
                                                <li>
                                                    {{$detail->company->name}}
                                                </li>
                                            @endforeach
                                        @endif                                    </td>
                                    <td>
                                        @currency($transaction->total_amount)
                                    </td>
                                    <td>
                                        {{$transaction->created_at->format("d M Y")}}
                                    </td>
                                    <td>
                                        <button class="btn btn-primary"
                                                wire:click="showDetailTransaction({{$transaction->id}})">
                                            <i class="fa fa-eye"></i>
                                            Detail
                                        </button>
                                    </td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                        <div class="float-right mx-4">
                            {{$transactions->links()}}
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
</div>

@push("js")
    <script>
        Livewire.on('showModal:open', event => {
            $("#showModal").modal("show");
        })
    </script>

@endpush
