<div>
    <div wire:ignore.self class="modal fade" tabindex="-1" role="dialog" id="showModal">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Transaksi Detail</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="table-responsive">
                        <table class="table table-striped" id="table-data">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Usaha</th>
                                <th>Pemilik Usaha</th>
                                <th>Petugas</th>
                                <th>Nominal</th>
                                <th>Tanggal</th>
                            </tr>
                            </thead>
                            <tbody>
                            @php
                                $detail = $transactionShow ? $transactionShow->transaction_details : collect()
                            @endphp
                            @foreach($detail as $item)
                                <tr>
                                    <td>{{ $loop->iteration}}</td>
                                    <td>{{$item->company->name}}</td>
                                    <td>{{$item->company->user->name}}</td>
                                    <td>{{$transactionShow->staff->user->name}}</td>
                                    <td>@currency($item->amount)</td>
                                    <td>{{ date("d M Y",strtotime($item->transaction_at))}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{--    <div wire:ignore.self class="modal fade" tabindex="-1" role="dialog" id="editModal">--}}
    {{--        <div class="modal-dialog modal-dialog-centered" role="document">--}}
    {{--            <div class="modal-content">--}}
    {{--                <form wire:submit.prevent="edit()">--}}
    {{--                    <div class="modal-header">--}}
    {{--                        <h5 class="modal-title">Ubah Setoran Petugas</h5>--}}
    {{--                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">--}}
    {{--                            <span aria-hidden="true">&times;</span>--}}
    {{--                        </button>--}}
    {{--                    </div>--}}
    {{--                    <div class="modal-body">--}}
    {{--                        <div class="form-group">--}}
    {{--                            <label>Petugas</label>--}}
    {{--                            <select wire:model.defer="staffIdEdit" class="form-control">--}}
    {{--                                <option value="">Pilih</option>--}}
    {{--                                @foreach($staffs as $staff)--}}
    {{--                                    <option value="{{$staff->id}}"> {{$staff->user->name}}</option>--}}
    {{--                                @endforeach--}}
    {{--                            </select>--}}
    {{--                            @if ($errors->has('staffIdEdit'))--}}
    {{--                                <div class="invalid-feedback" style="display: block;">--}}
    {{--                                    <strong>{{ $errors->first('staffIdEdit') }}</strong>--}}
    {{--                                </div>--}}
    {{--                            @endif--}}
    {{--                        </div>--}}
    {{--                        <div class="row">--}}
    {{--                            <div class="col-6">--}}
    {{--                                <div class="form-group">--}}
    {{--                                    <label>Nominal Setoran</label>--}}
    {{--                                    <input wire:model.defer="amountEdit" type="number" class="form-control">--}}
    {{--                                    @if ($errors->has('amountEdit'))--}}
    {{--                                        <div class="invalid-feedback" style="display: block;">--}}
    {{--                                            <strong>{{ $errors->first('amountEdit') }}</strong>--}}
    {{--                                        </div>--}}
    {{--                                    @endif--}}
    {{--                                </div>--}}
    {{--                            </div>--}}
    {{--                            <div class="col-6">--}}
    {{--                                <div class="form-group">--}}
    {{--                                    <label>Fee Petugas (opsional)</label>--}}
    {{--                                    <input wire:model.defer="staffAmountEdit" type="number" class="form-control">--}}
    {{--                                    @if ($errors->has('staffAmountEdit'))--}}
    {{--                                        <div class="invalid-feedback" style="display: block;">--}}
    {{--                                            <strong>{{ $errors->first('staffAmountEdit') }}</strong>--}}
    {{--                                        </div>--}}
    {{--                                    @endif--}}
    {{--                                </div>--}}
    {{--                            </div>--}}
    {{--                        </div>--}}

    {{--                        <div class="form-group">--}}
    {{--                            <label>Deskripsi (opsional)</label>--}}
    {{--                            <textarea class="form-control" wire:model.defer="descriptionEdit"--}}
    {{--                                      rows="3" style="height:100%"--}}
    {{--                            ></textarea>--}}
    {{--                            @if ($errors->has('descriptionEdit'))--}}
    {{--                                <div class="invalid-feedback" style="display: block;">--}}
    {{--                                    <strong>{{ $errors->first('descriptionEdit') }}</strong>--}}
    {{--                                </div>--}}
    {{--                            @endif--}}
    {{--                        </div>--}}

    {{--                        <div class="form-group">--}}
    {{--                            <label>Tanggal</label>--}}
    {{--                            <x-forms.date-picker--}}
    {{--                                class="form-control"--}}
    {{--                                wire:model.defer="balanceDateEdit"/>--}}
    {{--                            @if ($errors->has('balanceDateEdit'))--}}
    {{--                                <div class="invalid-feedback" style="display: block;">--}}
    {{--                                    <strong>{{ $errors->first('balanceDateEdit') }}</strong>--}}
    {{--                                </div>--}}
    {{--                            @endif--}}
    {{--                        </div>--}}


    {{--                    </div>--}}
    {{--                    <div class="modal-footer bg-whitesmoke br">--}}
    {{--                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>--}}
    {{--                        <button type="submit" class="btn btn-primary">Simpan</button>--}}
    {{--                    </div>--}}
    {{--                </form>--}}
    {{--            </div>--}}
    {{--        </div>--}}
    {{--    </div>--}}


    <section class="section">

        <div class="section-header">
            <h1>Dudukan Usaha</h1>
            {{-- <div class="section-header-breadcrumb">
                <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
                <div class="breadcrumb-item"><a href="#">Layout</a></div>
                <div class="breadcrumb-item">Default Layout</div>
            </div> --}}
        </div>
        <div class="row">
            <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                <div class="card card-statistic-1">
                    <div class="card-icon bg-primary">
                        <i class="fas fa-money-bills"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header">
                            <h4>Total Transaksi Tahun Ini</h4>
                        </div>
                        <div class="card-body">
                            ({{$totalTransactionYear}}) @currency($totalAmountTransactionYear)
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                <div class="card card-statistic-1">
                    <div class="card-icon bg-success">
                        <i class="fas fa-money-bills"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header">
                            <h4>Transaksi Bulan Ini</h4>
                        </div>
                        <div class="card-body">
                            ({{$totalTransactionMonth}}) @currency($totalAmountTransactionMonth)
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                <div class="card card-statistic-1">
                    <div class="card-icon bg-warning">
                        <i class="fas fa-money-bills"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header">
                            <h4>Transaksi Hari Ini</h4>
                        </div>
                        <div class="card-body">
                            ({{$totalTransactionToday}}) @currency($totalAmountTransactionToday)
                        </div>
                    </div>
                </div>
            </div>

        </div>


        <div class="card">
            <div class="card-header">
                <h4>
                    <div class="d-flex flex-row">
                        <div class="input-group d-flex flex-column">
                            <label style="font-size: 12px" class="m-0">Filter Tanggal</label>
                            <div class="d-flex flex-row">
                                <x-forms.date-picker
                                    class="form-control"
                                    style="border-radius: 30px 0 0 30px !important; background-color: white"
                                    wire:model="date" placeholder="Pilih tanggal"
                                    autocomplete="off"/>
                                <div class="input-group-btn">
                                    <button
                                        class="btn btn-danger"
                                        style="border-radius: 0 30px 30px 0 !important; margin-top:0.3px;"
                                        wire:click.prevent="removeFilter()"><i
                                            class="fas fa-times"></i></button>
                                </div>
                            </div>
                        </div>

                        <div class="input-group ml-3 d-flex flex-column">
                            <label style="font-size: 12px" class="m-0">Filter Bulan</label>
                            <div class="d-flex flex-row">
                                <input
                                    wire:model="month"
                                    style="border-radius: 30px 0 0 30px !important; background-color: white"
                                    placeholder="Pilih Bulan"
                                    type="month"
                                    class="form-control">
                                <div class="input-group-btn">
                                    <button
                                        style="border-radius: 0 30px 30px 0 !important; margin-top:0.3px;"
                                        class="btn btn-danger" wire:click.prevent="removeMonthFilter()"><i
                                            class="fas fa-times"></i></button>
                                </div>
                            </div>

                        </div>

                        <div class="input-group ml-3 d-flex flex-column">
                            <label style="font-size: 12px" class="m-0">Filter Tahun</label>
                            <div class="d-flex flex-row">
                                <select
                                    wire:model="year"
                                    class="form-control py-0"
                                    style="border-radius: 30px 0 0 30px !important; background-color: white; height: 31px !important;"
                                >
                                    <option value="" style="padding: 0px">Pilih tahun</option>
                                    @foreach($years as $i)
                                        <option style="padding: 0px" value="{{$i->year}}">{{$i->year}}</option>
                                    @endforeach
                                </select>
                                <div class="input-group-btn">
                                    <button
                                        style="border-radius: 0 30px 30px 0 !important; margin-top:0.3px;"
                                        class="btn btn-danger" wire:click.prevent="removeYearFilter()"><i
                                            class="fas fa-times"></i></button>
                                </div>
                            </div>

                        </div>

                    </div>
                </h4>
                <div class="card-header-action">
                    <button class="btn btn-success m-1" wire:click="exportExcel()">
                        <i class="fas fa-file-export"></i>
                        Expor Excel
                    </button>

                    <button class="btn btn-danger m-1" wire:click="exportPDF()">
                        <i class="fas fa-file-export"></i>
                        Expor PDF
                    </button>


                </div>
            </div>
            <div class="card-body p-0">
                <div class="table-responsive">
                    <table class="table table-striped" id="table-data">
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>Petugas</th>
                            <th>Usaha</th>
                            <th>Total Nominal</th>
                            <th>Tanggal</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($transactions as $transaction)
                            <tr>
                                <td>{{ ($transactions->currentpage()-1) * $transactions->perpage() + $loop->index + 1 }}</td>
                                <td>{{$transaction->staff->user->name}}</td>
                                <td>
                                    @if($transaction->detail)
                                        @foreach($transaction->detail->unique('company_id') as $detail)
                                            <li>
                                                {{$detail->company->name}} - {{$detail->company->user->name}}
                                            </li>
                                        @endforeach
                                    @endif
                                </td>
                                <td>@currency($transaction->total_amount)</td>
                                <td>{{$transaction->created_at->format("d M Y")}}</td>
                                <td>
                                    <button class="btn btn-primary m-1"
                                            wire:click="showDetail({{$transaction->id}})">
                                        <i class="fas fa-eye"></i>Detail
                                    </button>
                                    {{--                                                                    <button class="btn btn-warning"--}}
                                    {{--                                                                            wire:click.prevent="openEdit({{$balance->id}})"><i--}}
                                    {{--                                                                            class="fa-solid fa-pen-to-square"></i>--}}
                                    {{--                                                                    </button>--}}
                                    {{--                                                                    <button class="btn btn-danger"--}}
                                    {{--                                                                            wire:click.prevent="deleteConfirmation({{$balance->id}})"><i--}}
                                    {{--                                                                            class="fa fa-trash"></i></button>--}}
                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                    <div class="float-right mx-4">
                        {{$transactions->links()}}
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
</section>
</div>
@push("js")
    <script>
        Livewire.on('showModal:open', event => {
            $('#showModal').modal('show');
        })
    </script>
@endpush
