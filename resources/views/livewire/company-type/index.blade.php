<div>
    <div wire:ignore.self class="modal fade" tabindex="-1" role="dialog" id="formModal">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <form wire:submit.prevent="post()">
                    <div class="modal-header">
                        <h5 class="modal-title">Tambah Jenis Usaha</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Jenis Usaha</label>
                            <input wire:model.defer="type" type="text" class="form-control">
                            @if ($errors->has('type'))
                                <div class="invalid-feedback" style="display: block;">
                                    <strong>{{ $errors->first('type') }}</strong>
                                </div>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>Surat Ijin Usaha</label>
                            <select wire:model.defer="is_document_required" class="form-control">
                                <option value="">Pilih</option>
                                <option value="0">Tidak Dibutuhkan</option>
                                <option value="1">Dibutuhkan</option>
                            </select>
                            @if ($errors->has('is_document_required'))
                                <div class="invalid-feedback" style="display: block;">
                                    <strong>{{ $errors->first('is_document_required') }}</strong>
                                </div>
                            @endif
                        </div>
                    </div>
                    <div class="modal-footer bg-whitesmoke br">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div wire:ignore.self class="modal fade" tabindex="-1" role="dialog" id="editModal">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <form wire:submit.prevent="edit()">
                    <div class="modal-header">
                        <h5 class="modal-title">Ubah Jenis Usaha</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Jenis Usaha</label>
                            <input wire:model.defer="type_edit" type="text" class="form-control">
                            @if ($errors->has('type_edit'))
                                <div class="invalid-feedback" style="display: block;">
                                    <strong>{{ $errors->first('type_edit') }}</strong>
                                </div>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>Surat Ijin Usaha</label>
                            <select wire:model.defer="is_document_required_edit" class="form-control">
                                <option value="">Pilih</option>
                                <option value="0">Tidak Dibutuhkan</option>
                                <option value="1">Dibutuhkan</option>
                            </select>
                            @if ($errors->has('is_document_required_edit'))
                                <div class="invalid-feedback" style="display: block;">
                                    <strong>{{ $errors->first('is_document_required_edit') }}</strong>
                                </div>
                            @endif
                        </div>
                    </div>
                    <div class="modal-footer bg-whitesmoke br">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <section class="section">

        <div class="section-header">
            <h1>Jenis Usaha</h1>
            {{-- <div class="section-header-breadcrumb">
                <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
                <div class="breadcrumb-item"><a href="#">Layout</a></div>
                <div class="breadcrumb-item">Default Layout</div>
            </div> --}}
        </div>

        <div class="section-body">
            <div class="card">
                <div class="card-header">
                    <h4>
                        @if(auth()->user()->isAdmin)
                            <button class="btn btn-success" data-toggle="modal" data-target="#formModal">+ Tambah Jenis
                                Usaha
                            </button>
                        @endif
                    </h4>
                    <div class="card-header-action">
                        <div class="input-group">
                            <input type="text" class="form-control" wire:model="search" placeholder="Search">
                            <div class="input-group-btn">
                                <button class="btn btn-primary"><i class="fas fa-search"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body p-0">
                    <div class="table-responsive">
                        <table class="table table-striped" id="table-data">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Jenis</th>
                                <th>Surat Ijin usaha</th>
                                @if(auth()->user()->isAdmin)
                                    <th>Action</th>
                                @endif
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($company_types as $company_type)
                                <tr>
                                    <td>{{ ($company_types->currentpage()-1) * $company_types->perpage() + $loop->index + 1 }}</td>
                                    <td>{{$company_type->type}}</td>
                                    <td>
                                        @if($company_type->is_document_required)
                                            <div class="badge badge-primary">Diperlukan</div>
                                        @else
                                            <div class="badge badge-warning">Tidak Diperlukan</div>
                                    @endif
                                    @if(auth()->user()->isAdmin)
                                        <td>
                                            <button class="btn btn-warning m-1"
                                                    wire:click.prevent="openEdit({{$company_type->id}})"><i
                                                    class="fa-solid fa-pen-to-square"></i>
                                            </button>
                                            <button class="btn btn-danger m-1"
                                                    wire:click.prevent="deleteConfirmation({{$company_type->id}})"><i
                                                    class="fa fa-trash"></i></button>
                                        </td>
                                    @endif
                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                        <div class="float-right mx-4">
                            {{$company_types->links()}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@push("js")
    <script>
        Livewire.on('editModal:open', event => {
            $('#editModal').modal('show');
        })
    </script>
@endpush
