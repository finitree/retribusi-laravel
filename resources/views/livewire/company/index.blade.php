<div>
    <section class="section">
        <div class="section-header">
            <h1>Daftar Usaha</h1>
        </div>
        <div class="section-body">
            @include("common.alert")
            <div class="card">
                <div class="card-header">
                    <h4>
                        @if(auth()->user()->isAdmin)
                            <a class="btn btn-success mb-4" href="{{route('company.create')}}">+ Tambah Usaha</a>
                        @endif
                        <div class="d-flex">
                            <select class="form-control py-0 mr-2" wire:model="transaction">
                                <option value="" selected>Transaksi</option>
                                <option value="paid">Sudah Bayar</option>
                                <option value="unpaid">Belum Bayar</option>
                            </select>
                            <select class="form-control py-0" wire:model="status">
                                <option value="" selected>Status Usaha</option>
                                <option value="blocked">Ditolak</option>
                                <option value="wait_verified">Menunggu Verifikasi</option>
                                <option value="verified">Terverifikasi</option>
                            </select>
                        </div>
                    </h4>
                    <div class="card-header-action">
                        <div class="input-group">
                            <input type="text" class="form-control" wire:model.debounce.200ms="search"
                                   placeholder="Search">

                            <div class="input-group-btn">
                                <button class="btn btn-primary"><i class="fas fa-search"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th scope="col">No</th>
                                <th scope="col">Nama Usaha</th>
                                <th scope="col">Pemilik Usaha</th>
                                <th scope="col">Banjar</th>
                                <th scope="col">Status Transaksi</th>
                                <th scope="col">Status Perusahaan</th>

                                @if(auth()->user()->isAdmin)
                                    <th scope="col">Action</th>
                                @endif
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($companies as $company)
                                <tr>
                                    <td>{{ ($companies->currentpage()-1) * $companies->perpage() + $loop->index + 1 }}</td>
                                    <td>{{ $company->name }}</td>
                                    <td>{{$company->user->name}}</td>
                                    <td>{{$company->banjar->name ?? "-"}}</td>
                                    <td>
                                        @if ($company->transaction_status == 'unpaid')
                                            Belum Bayar
                                        @else
                                            Sudah Bayar
                                        @endif
                                    </td>
                                    <td>{{ $company->company_status }}</td>
                                    @if(auth()->user()->isAdmin)
                                        <td>
                                            @if($company->status == \App\Models\Company::WAIT_VERIFIED)
                                                <a class="btn btn-success m-1"
                                                   href="{{route('company.edit', ['id' => $company->id])}}">
                                                    <i class="fas fa-check-circle"></i>
                                                    Verifikasi
                                                </a>
                                                <button class="btn btn-danger m-1"
                                                        wire:click.prevent="declineConfirmationCompany({{$company->id}})">
                                                    <i class="fa fa-times-circle"></i>
                                                    Tolak
                                                </button>
                                            @elseif($company->status == \App\Models\Company::VERIFIED)
                                                <a class="btn btn-warning m-1"
                                                   href="{{route('company.edit', ['id' => $company->id])}}">
                                                    <i class="fa-solid fa-pen-to-square"></i>
                                                </a>
                                                <button class="btn btn-danger m-1"
                                                        wire:click.prevent="deleteConfirmationCompany({{$company->id}})">
                                                    <i
                                                        class="fa fa-trash"></i></button>
                                            @else
                                                <a class="btn btn-success m-1"
                                                   href="{{route('company.edit', ['id' => $company->id])}}">
                                                    <i class="fas fa-check-circle"></i>
                                                    Verifikasi
                                                </a>
                                                <button class="btn btn-danger m-1"
                                                        wire:click.prevent="deleteConfirmationCompany({{$company->id}})">
                                                    <i
                                                        class="fa fa-trash"></i></button>
                                            @endif
                                        </td>
                                    @endif
                                </tr>
                            @empty
                                <p class="text-center">No Data</p>
                            @endforelse
                            </tbody>
                        </table>
                        <div class="float-right mx-4">
                            {{$companies->links()}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
