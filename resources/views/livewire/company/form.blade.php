<div>
    <div wire:ignore.self class="modal fade" tabindex="-1" role="dialog" id="modalCloseSchedule">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <form wire:submit.prevent="updateCloseSchedule">
                    <div class="modal-header">
                        <h5 class="modal-title">Ubah Jadwal Tutup</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label class="text-base">Tanggal Mulai</label>
                            <input
                                placeholder="" type="date"
                                class="form-control @error('date_start') is-invalid @enderror"
                                wire:model="date_start">
                            @error('date_start')
                            <div class="invalid-feedback">
                                {{$message}}
                            </div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label class="text-base">Tanggal Mulai</label>
                            <input placeholder="" type="date"
                                   class="form-control @error('date_end') is-invalid @enderror" wire:model="date_end">
                            @error('date_end')
                            <div class="invalid-feedback">
                                {{$message}}
                            </div>
                            @enderror
                        </div>
                    </div>
                    <div class="modal-footer bg-whitesmoke br">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="card">
        <form wire:submit.prevent="submit()">
            <div class="card-header">
                <h4>{{$idCompany ? $company->status == \App\Models\Company::WAIT_VERIFIED ? "Verifikasi": 'Edit' : 'Tambah'}}
                    Usaha</h4>
                @if($idCompany)
                    <div class="card-header-action">
                        <a class="btn btn-warning mr-1" href="{{route('company-agreement-export',['id' => $idCompany])}}"
                           target="_blank">
                            <i class="fas fa-file-export"></i>
                            Cetak Formulir Persetujuan
                        </a>

                        <a class="btn btn-primary" href="{{route('company-export',['id' => $idCompany])}}"
                           target="_blank">
                            <i class="fas fa-file-export"></i>
                            Cetak Data Usaha
                        </a>
                    </div>
                @endif
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="text-base">Nama Usaha</label>
                            <input placeholder="" type="text"
                                   class="form-control @error('company.name') is-invalid @enderror"
                                   wire:model="company.name">
                            @error('company.name')
                            <div class="invalid-feedback">
                                {{$message}}
                            </div>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="text-base">Jenis Usaha</label>
                            <select class="form-control @error('company.company_type_id') is-invalid @enderror"
                                    wire:model="company.company_type_id">
                                <option value="">Pilih Jenis Usaha</option>
                                @foreach ($company_types as $type)
                                    <option value="{{$type->id}}">{{$type->type}}</option>
                                @endforeach
                            </select>
                            @error('company.company_type_id')
                            <div class="invalid-feedback">
                                {{$message}}
                            </div>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="text-base">Surat Ijin Usaha</label>
                            <span class="badge badge-success">Opsional</span>
                            <input type="file" accept="application/pdf"
                                   class="form-control @error('documents') is-invalid @enderror"
                                   wire:model="documents"
                                   name="documents" id="upload">
                            @if ($old_docs)
                                <div class="mt-3">
                                    <a class="text-link" href="{{$old_docs}}" target="blank"> Lihat Surat Ijin Usaha
                                        <i
                                            class="fa-solid fa-lg fa-file-pdf"></i></a>
                                </div>
                            @endif
                            @error('documents')
                            <div class="invalid-feedback">
                                {{$message}}
                            </div>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="text-base">Banjar</label>
                            <select class="form-control @error('company.banjar_id') is-invalid @enderror"
                                    wire:model="company.banjar_id">
                                <option value="">Pilih Banjar</option>
                                @foreach ($banjars as $banjar)
                                    <option value="{{$banjar->id}}">{{$banjar->name}}</option>
                                @endforeach
                            </select>
                            @error('company.banjar_id')
                            <div class="invalid-feedback">
                                {{$message}}
                            </div>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="text-base">Foto</label>
                            <input type="file" accept="image/*"
                                   class="form-control @error('photos') is-invalid @enderror"
                                   wire:model="photos">
                            @error('photos')
                            <div class="invalid-feedback">
                                {{$message}}
                            </div>
                            @enderror
                            <div wire:loading wire:target="photos">
                                Uploading Image...
                            </div>
                            <div wire:loading.remove wire:target="photos">
                                @if ($old_photo && $old_photo === $photos)
                                    <img class="mt-3" src="{{$old_photo}}" class="img-fluid " alt="photo"
                                         width="200">
                                @elseif ($photos)
                                    <img alt="image" width="200" src="{{ $photos->temporaryUrl() }}"
                                         class="img-fluid">
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="text-base">Pemilik Usaha</label>
                            <select class="form-control @error('company.user_id') is-invalid @enderror"
                                    wire:model="company.user_id">
                                <option value="">Pilih Pemilik Usaha</option>
                                @foreach ($owners as $owner)
                                    <option value="{{$owner->id}}">{{$owner->name}}</option>
                                @endforeach
                            </select>
                            @error('company.user_id')
                            <div class="invalid-feedback">
                                {{$message}}
                            </div>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="text-base">Tipe Pembayaran</label>
                            <select class="form-control @error('type_subscription') is-invalid @enderror"
                                    wire:model="type_subscription">
                                <option value="">Pilih Tipe Pembayaran</option>
                                @foreach ($subscription_types as $item)
                                    <option value="{{$item->id}}">{{$item->category}}</option>
                                @endforeach
                            </select>
                            @error('type_subscription')
                            <div class="invalid-feedback">
                                {{$message}}
                            </div>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="text-base">Nominal Pembayaran</label>
                            <input placeholder="" type="number"
                                   class="form-control @error('company.subscription_amount') is-invalid @enderror"
                                   wire:model="company.subscription_amount">
                            @error('company.subscription_amount')
                            <div class="invalid-feedback">
                                {{$message}}
                            </div>
                            @enderror
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <label class="text-base">Lokasi</label>
                            <div wire:ignore>
                                <div id="mapid" style="height: 400px;"></div>
                            </div>
                        </div>
                        <input
                            id="latitude"
                            type="hidden"
                            wire:model.defer="company.latitude"
                            value="{{$company->latitude ?? "-8.630223693798095"}}"
                        >
                        <input
                            id="longitude"
                            type="hidden"
                            wire:model.defer="company.longitude"
                            value="{{$company->longitude ?? "115.1876207394696"}}"
                        >

                        {{--                        <div class="row">--}}
                        {{--                            <div class="col-md-6">--}}
                        {{--                                <div class="form-group">--}}
                        {{--                                    <label class="text-base">Latitude</label>--}}

                        {{--                                    <input type="text"--}}
                        {{--                                           class="form-control @error('company.latitude') is-invalid @enderror"--}}
                        {{--                                           wire:model.defer="company.latitude" value="-8.655924" name="company.latitude"--}}
                        {{--                                           id="latitude">--}}
                        {{--                                    @error('company.latitude')--}}
                        {{--                                    <div class="invalid-feedback">--}}
                        {{--                                        {{$message}}--}}
                        {{--                                    </div>--}}
                        {{--                                    @enderror--}}
                        {{--                                </div>--}}
                        {{--                            </div>--}}
                        {{--                            <div class="col-md-6">--}}
                        {{--                                <div class="form-group">--}}
                        {{--                                    <label class="text-base">Longitude</label>--}}

                        {{--                                    <input type="text"--}}
                        {{--                                           class="form-control @error('company.longitude') is-invalid @enderror"--}}
                        {{--                                           wire:model.defer="company.longitude" value="115.216934"--}}
                        {{--                                           name="company.longitude" id="longitude">--}}
                        {{--                                    @error('company.longitude')--}}
                        {{--                                    <div class="invalid-feedback">--}}
                        {{--                                        {{$message}}--}}
                        {{--                                    </div>--}}
                        {{--                                    @enderror--}}
                        {{--                                </div>--}}
                        {{--                            </div>--}}
                        {{--                            <div class="col">--}}
                        {{--                                <button class="btn btn-success" id="btnSync" type="button">Sinkronisasi Koordinat--}}
                        {{--                                </button>--}}
                        {{--                            </div>--}}
                        {{--                        </div>--}}

                    </div>
                    <div class="col-12 mt-3">
                        <div class="form-group">
                            <label class="text-base">Alamat</label>
                            <input placeholder="" type="text"
                                   class="form-control @error('company.address') is-invalid @enderror"
                                   wire:model="company.address">
                            @error('company.address')
                            <div class="invalid-feedback">
                                {{$message}}
                            </div>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label class="text-base">Dokumen Persetujuan</label>
                            <input type="file" accept="application/pdf"
                                   class="form-control @error('agreement_document') is-invalid @enderror"
                                   wire:model="agreement_document"
                                   name="agreement_document" id="upload">
                            @if ($old_agreement_document)
                                <div class="mt-3">
                                    <a class="text-link" href="{{$old_agreement_document}}" target="blank"> Lihat
                                        Dokumen Persetujuan
                                        <i
                                            class="fa-solid fa-lg fa-file-pdf"></i></a>
                                </div>
                            @endif
                            @error('agreement_document')
                            <div class="invalid-feedback">
                                {{$message}}
                            </div>
                            @enderror

                        </div>

                    </div>
                    <div class="col-12">
                        <p style="font-size: 24px;">Jam Operasional</p>
                        @error('schedules')
                        <div class="alert alert-warning alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;
                            </button>
                            <h5><i class="icon fas fa-ban"></i> Validation!</h5>
                            {{$message}}
                        </div>
                        @enderror
                    </div>
                    @for($i=0; $i <= 6; $i++)
                        <div class="col-lg-8 col-12">
                            <label class="text-base">{{getDays($i+1)}}</label>
                            <div class="row align-items-center">
                                <div class="col-md-4">
                                    <div class="form-group mb-1 mb-lg-4">
                                        <label>Jam Buka</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <div class="input-group-text">
                                                    <i class="fas fa-clock"></i>
                                                </div>
                                            </div>
                                            <input id="time-{{$i}}" type="time"
                                                   class="form-control @error('schedules.'.$i.'.opening_hours') is-invalid @enderror timepicker"
                                                   wire:model.defer="schedules.{{$i}}.opening_hours">
                                            @error('schedules'.$i.'opening_hours')
                                            <div class="invalid-feedback">
                                                {{$message}}
                                            </div>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group mb-1 mb-lg-4">
                                        <label>Jam Tutup</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <div class="input-group-text">
                                                    <i class="fas fa-clock"></i>
                                                </div>
                                            </div>
                                            <input id="time-{{$i}}" type="time"
                                                   class="form-control @error('schedules'.$i.'closing_hours') is-invalid @enderror timepicker"
                                                   wire:model.defer="schedules.{{$i}}.closing_hours">
                                            @error('schedules'.$i.'closing_hours')
                                            <div class="invalid-feedback">
                                                {{$message}}
                                            </div>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2 mb-3 mb-lg-0">
                                    <label class="custom-switch pl-0 pt-2">
                                        <input wire:model.defer="schedules.{{$i}}.is_open" id="open-{{$i}}"
                                               name="open-{{$i}}" type="checkbox" class="custom-switch-input">
                                        <span class="custom-switch-indicator"></span>
                                        <span class="custom-switch-description">Buka</span>
                                    </label>
                                </div>
                            </div>
                        </div>
                    @endfor
                </div>
            </div>
            <div class="card-footer text-right">
                <div wire:loading wire:target="submit">
                    <img width="75" src="{{asset('svg/pulse.svg')}}" alt="">
                </div>
                @if ($idCompany)
                    @if($company->status == \App\Models\Company::WAIT_VERIFIED)
                        <button
                            wire:click.prevent="declineConfirmationCompany({{$idCompany}})"
                            class="btn btn-danger">
                            <i class="fas fa-times-circle"></i>
                            Tolak
                        </button>

                        <button
                            type="submit"
                            class="btn btn-success">
                            <i class="fas fa-check-circle"></i>
                            Verifikasi
                        </button>
                    @elseif($company->status == \App\Models\Company::BLOCKED)
                        <button
                            type="submit"
                            class="btn btn-success">
                            <i class="fas fa-check-circle"></i>
                            Verifikasi
                        </button>
                    @else
                        <button type="submit" class="btn btn-primary">
                            Simpan Perubahan
                        </button>
                    @endif
                @else
                    <button type="submit" class="btn btn-primary">
                        Submit
                    </button>
                @endif
            </div>
        </form>
    </div>
    {{--    --}}
    {{--    @if ($idCompany)--}}
    {{--        <div class="card">--}}
    {{--            <div class="card-header">--}}
    {{--                <h4>Jadwal Tutup</h4>--}}
    {{--            </div>--}}
    {{--            <div class="card-body">--}}
    {{--                <div class="row">--}}
    {{--                    <div class="col-6">--}}
    {{--                        <div class="form-group">--}}
    {{--                            <label class="text-base">Tanggal Mulai</label>--}}
    {{--                            <input placeholder="" type="date"--}}
    {{--                                   class="form-control @error('date_start') is-invalid @enderror"--}}
    {{--                                   wire:model="date_start">--}}
    {{--                            @error('date_start')--}}
    {{--                            <div class="invalid-feedback">--}}
    {{--                                {{$message}}--}}
    {{--                            </div>--}}
    {{--                            @enderror--}}
    {{--                        </div>--}}
    {{--                    </div>--}}
    {{--                    <div class="col-6">--}}
    {{--                        <div class="form-group">--}}
    {{--                            <label class="text-base">Tanggal Selesai</label>--}}
    {{--                            <input placeholder="" type="date"--}}
    {{--                                   class="form-control @error('date_end') is-invalid @enderror" wire:model="date_end">--}}
    {{--                            @error('date_end')--}}
    {{--                            <div class="invalid-feedback">--}}
    {{--                                {{$message}}--}}
    {{--                            </div>--}}
    {{--                            @enderror--}}
    {{--                        </div>--}}
    {{--                    </div>--}}
    {{--                </div>--}}
    {{--                <p class="text-base">History Jadwal Tutup</p>--}}
    {{--                <ul class="list-group list-group-flush">--}}
    {{--                    @forelse ($closed as $item)--}}
    {{--                        <li class="list-group-item">--}}
    {{--                            <div class="row align-items-center">--}}
    {{--                                <div>--}}
    {{--                                    <p class="mb-0" style="font-size: 16px">{{$loop->iteration}}</p>--}}
    {{--                                </div>--}}
    {{--                                <div class="col-md-4 col-8">--}}
    {{--                                    <p class="mb-0" style="font-size: 16px">{{$item->date_start_clear}}--}}
    {{--                                        - {{$item->date_end_clear}}</p>--}}
    {{--                                </div>--}}
    {{--                                <div class="col-md-2 col-4">--}}
    {{--                                    <button class="btn btn-warning m-1"--}}
    {{--                                            wire:click.prevent="openModal({{$item->id}})">--}}
    {{--                                        <i class="fa-solid fa-pen-to-square"></i>--}}
    {{--                                    </button>--}}
    {{--                                    <button class="btn btn-danger m-1"--}}
    {{--                                            wire:click.prevent="deleteConfirmationSchedule({{$item->id}})"><i--}}
    {{--                                            class="fa fa-trash"></i>--}}
    {{--                                    </button>--}}
    {{--                                </div>--}}
    {{--                            </div>--}}
    {{--                        </li>--}}
    {{--                    @empty--}}
    {{--                        <p>Belum ada jadwal tutup</p>--}}
    {{--                    @endforelse--}}
    {{--                </ul>--}}
    {{--            </div>--}}
    {{--            <div class="card-footer text-right">--}}
    {{--                <div wire:loading wire:target="submitCloseSchedule">--}}
    {{--                    <img width="75" src="{{asset('svg/pulse.svg')}}" alt="">--}}
    {{--                </div>--}}
    {{--                <button type="button" wire:click.prevent="submitCloseSchedule" class="btn btn-primary">--}}
    {{--                    Tambah Jadwal Tutup--}}
    {{--                </button>--}}
    {{--            </div>--}}
    {{--        </div>--}}
    {{--    @endif--}}

</div>

@push('css')
    <style>
        .badge {
            padding: 4px 10px !important;
            font-size: 10px !important;
        }
    </style>
@endpush

@push('js')
    <script>
        Livewire.on('modalCloseSchedule:open', event => {
            $('#modalCloseSchedule').modal('show');
        })

        // Livewire.on('loadMap', event => {
        //     loadMap();
        // })
    </script>
    <script>
        var latitude = $('#latitude').val();
        var longitude = $('#longitude').val();
        console.log(latitude)
        console.log(longitude)
        var mymap = L.map('mapid').setView([latitude, longitude], 13);
        const btnSync = $('#btnSync')

        L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
            attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
            maxZoom: 20,
            id: 'mapbox/streets-v11',
            tileSize: 512,
            zoomOffset: -1,
            accessToken: 'pk.eyJ1Ijoid2lkaWFuYXB3IiwiYSI6ImNrNm95c2pydjFnbWczbHBibGNtMDNoZzMifQ.kHoE5-gMwNgEDCrJQ3fqkQ'
        }).addTo(mymap);


        marker = new L.marker([latitude, longitude], {
            draggable: 'true'
        }).addTo(mymap);


        function onLocationFound(e) {
            var radius = e.accuracy / 2;

            marker.setLatLng(e.latlng)
                .bindPopup("Lokasi Anda +-" + radius + " meters dari titik ini, geser marker untuk mengubah posisi")
                .openPopup();
        }

        function onLocationError(e) {
            alert(e.message);
        }

        // mymap.on('locationfound', onLocationFound);
        // mymap.on('locationerror', onLocationError);

        // mymap.locate({
        //     setView: true,
        //     maxZoom: 16
        // });

        marker.on('dragend', function (event) {
            var marker = event.target;
            var position = marker.getLatLng();
            console.log(position);
            marker.setLatLng(new L.LatLng(position.lat, position.lng), {
                draggable: 'true'
            });

            setMapFocus(new L.LatLng(position.lat, position.lng))
        });
        console.log(marker)

        btnSync.on('click', function (event) {
            event.preventDefault();
            const newLat = $('#latitude').val();
            const newLng = $('#longitude').val();
            var newLatLng = new L.LatLng(newLat, newLng);
            marker.setLatLng(newLatLng)
            setMapFocus(newLatLng)
        })

        function setMapFocus(latLng) {
            mymap.panTo(latLng)
            $('#latitude').val(latLng.lat);
            $('#longitude').val(latLng.lng);
            var latEl = document.getElementById('latitude');
            latEl.dispatchEvent(new Event('input'));
            var lonEl = document.getElementById('longitude');
            lonEl.dispatchEvent(new Event('input'));
        }
    </script>
@endpush
