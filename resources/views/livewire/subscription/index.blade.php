<div>
    <div wire:ignore.self class="modal fade" tabindex="-1" role="dialog" id="formModal">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <form wire:submit.prevent="post()">
                    <div class="modal-header">
                        <h5 class="modal-title">Tambah Iuran</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Jenis Usaha</label>
                            <select
                                class="form-control"
                                wire:model.defer="company_type_id">
                                <option value="">Pilih jenis usaha</option>
                                @foreach($companyTypeOptions as $companyType)
                                    <option value="{{$companyType->id}}">{{$companyType->type}}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('company_type_id'))
                                <div class="invalid-feedback" style="display: block;">
                                    <strong>{{ $errors->first('company_type_id') }}</strong>
                                </div>
                            @endif
                        </div>

                        <div class="form-group">
                            <label>Harian</label>
                            <input type="number" class="form-control" wire:model.defer="daily_amount">
                            @if ($errors->has('daily_amount'))
                                <div class="invalid-feedback" style="display: block;">
                                    <strong>{{ $errors->first('daily_amount') }}</strong>
                                </div>
                            @endif
                        </div>

                        <div class="form-group">
                            <label>Bulanan</label>
                            <input type="number" class="form-control" wire:model.defer="monthly_amount">
                            @if ($errors->has('monthly_amount'))
                                <div class="invalid-feedback" style="display: block;">
                                    <strong>{{ $errors->first('monthly_amount') }}</strong>
                                </div>
                            @endif
                        </div>

                    </div>
                    <div class="modal-footer bg-whitesmoke br">
                        <button type="button" class="btn btn-warning" data-dismiss="modal">Tutup</button>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div wire:ignore.self class="modal fade" tabindex="-1" role="dialog" id="editModal">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <form wire:submit.prevent="edit()">
                    <div class="modal-header">
                        <h5 class="modal-title">Ubah Iuran</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Jenis Usaha</label>
                            <select
                                disabled
                                class="form-control"
                                wire:model.defer="company_type_id_edit">
                                <option value="">Pilih jenis usaha</option>
                                @foreach($companyTypes as $companyType)
                                    <option value="{{$companyType->id}}">{{$companyType->type}}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('company_type_id_edit'))
                                <div class="invalid-feedback" style="display: block;">
                                    <strong>{{ $errors->first('company_type_id_edit') }}</strong>
                                </div>
                            @endif
                        </div>

                        <div class="form-group">
                            <label>Nominal Harian</label>
                            <input type="number" class="form-control" wire:model.defer="daily_amount_edit">
                            @if ($errors->has('daily_amount_edit'))
                                <div class="invalid-feedback" style="display: block;">
                                    <strong>{{ $errors->first('daily_amount_edit') }}</strong>
                                </div>
                            @endif
                        </div>

                        <div class="form-group">
                            <label>Nominal Bulanan</label>
                            <input type="number" class="form-control" wire:model.defer="monthly_amount_edit">
                            @if ($errors->has('monthly_amount_edit'))
                                <div class="invalid-feedback" style="display: block;">
                                    <strong>{{ $errors->first('monthly_amount_edit') }}</strong>
                                </div>
                            @endif
                        </div>

                    </div>
                    <div class="modal-footer bg-whitesmoke br">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <section class="section">

        <div class="section-header">
            <h1>Iuran</h1>
            {{-- <div class="section-header-breadcrumb">
                <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
                <div class="breadcrumb-item"><a href="#">Layout</a></div>
                <div class="breadcrumb-item">Default Layout</div>
            </div> --}}
        </div>

        <div class="section-body">
            <div class="card">
                <div class="card-header">
                    <h4>
                        @if(auth()->user()->isAdmin)
                            <button class="btn btn-success" data-toggle="modal" data-target="#formModal">+ Tambah Iuran
                            </button>
                        @endif
                    </h4>
                    <div class="card-header-action">
                        <div class="input-group">
                            <input type="text" class="form-control" wire:model="search" placeholder="Search">
                            <div class="input-group-btn">
                                <button class="btn btn-primary"><i class="fas fa-search"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body p-0">
                    <div class="table-responsive">
                        <table class="table table-striped" id="table-data">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Jenis Usaha</th>
                                <th>Harian</th>
                                <th>Bulanan</th>
                                @if(auth()->user()->isAdmin)
                                    <th>Aksi</th>
                                @endif
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($subscriptions as $subscription)
                                <tr>
                                    <td>{{ ($subscriptions->currentpage()-1) * $subscriptions->perpage() + $loop->index + 1 }}</td>
                                    <td>{{$subscription->type}}</td>
                                    <td>@currency($subscription->daily_subscription)</td>
                                    <td>@currency($subscription->monthly_subscription)</td>
                                    @if(auth()->user()->isAdmin)
                                        <td>
                                            <button class="btn btn-warning m-1"
                                                    wire:click.prevent="openEdit({{$subscription->id}})"><i
                                                    class="fa-solid fa-pen-to-square"></i>
                                            </button>
                                            <button class="btn btn-danger m-1"
                                                    wire:click.prevent="deleteConfirmation({{$subscription->id}})"><i
                                                    class="fa fa-trash"></i></button>
                                        </td>
                                    @endif
                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                        <div class="float-right mx-4">
                            {{$subscriptions->links()}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@push("js")
    <script>
        Livewire.on('editModal:open', event => {
            $('#editModal').modal('show');
        })
    </script>
@endpush
