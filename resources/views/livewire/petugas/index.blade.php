<div>
    <section class="section">
        <div class="section-header">
            <h1>Daftar Petugas</h1>
        </div>
        <div class="section-body">
            @include("common.alert")
            <div class="card">
                <div class="card-header">
                    <h4>
                        @if(auth()->user()->isAdmin)
                            <a class="btn btn-success" href="{{route('staff.create')}}">+ Tambah Petugas</a>
                        @endif
                    </h4>
                    <div class="card-header-action">
                        <div class="input-group">
                            <input type="text" class="form-control" wire:model.debounce.500ms="search"
                                   placeholder="Search">

                            <div class="input-group-btn">
                                <button class="btn btn-primary"><i class="fas fa-search"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th scope="col">No</th>
                                <th scope="col">Nama</th>
                                <th scope="col">Email</th>
                                <th scope="col">Telepon</th>
                                <th scope="col">Saldo</th>
                                @if(auth()->user()->isAdmin)
                                    <th scope="col">Action</th>
                                @endif
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($petugas as $pt)
                                <tr>
                                    <td>{{ ($petugas->currentpage()-1) * $petugas->perpage() + $loop->index + 1 }}</td>
                                    <td>{{$pt->user->name}}</td>
                                    <td>{{$pt->user->email}}</td>
                                    <td>{{$pt->user->phone}}</td>
                                    <td>@currency($pt->balance)</td>
                                    @if(auth()->user()->isAdmin)
                                        <td>
                                            <a class="btn btn-warning m-1"
                                               href="{{route('staff.edit', ['id' => $pt->id])}}"><i
                                                    class="fa-solid fa-pen-to-square"></i>

                                            </a>
                                            <button class="btn btn-danger m-1"
                                                    wire:click.prevent="deleteConfirmation({{$pt->id}})"><i
                                                    class="fa fa-trash"></i></button>
                                        </td>
                                    @endif
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div class="float-right mx-4">
                            {{$petugas->links()}}
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>
</div>
