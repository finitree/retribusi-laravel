<div>
    <div class="card">
        <form wire:submit.prevent="submit()">
            <div class="card-header">
                <h4>Server-side Validation</h4>
            </div>
            <div class="card-body">
                <h1>{{$dummy}}</h1>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Nama</label>
                            <input type="text" class="form-control is-valid" value="Rizal Fakhri" required="">
                            <div class="valid-feedback">
                                Good job!
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>No Hp</label>
                            <input type="text" class="form-control is-valid" value="Rizal Fakhri" required="">
                            <div class="valid-feedback">
                                Good job!
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Email</label>
                            <input type="text" class="form-control is-valid" value="Rizal Fakhri" required="">
                            <div class="valid-feedback">
                                Good job!
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>KTP</label>
                            <input type="text" class="form-control is-valid" value="Rizal Fakhri" required="">
                            <div class="valid-feedback">
                                Good job!
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Password</label>
                            <input type="text" class="form-control is-valid" value="Rizal Fakhri" required="">
                            <div class="valid-feedback">
                                Good job!
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Role</label>
                            <input type="text" class="form-control is-valid" value="Rizal Fakhri" required="">
                            <div class="valid-feedback">
                                Good job!
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Photo</label>
                            <input type="text" class="form-control is-valid" value="Rizal Fakhri" required="">
                            <div class="valid-feedback">
                                Good job!
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-footer text-right">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </form>
    </div>
</div>
