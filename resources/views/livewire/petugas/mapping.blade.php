<div>
    <div wire:ignore.self class="modal fade" tabindex="-1" role="dialog" id="mappingModal">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <form wire:submit.prevent="submit()">
                    <div class="modal-header">
                        <h5 class="modal-title">Mapping Petugas</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Usaha</label>
                            <div wire:ignore>
                                <select
                                    class="select2 form-control"
                                    id="company_select"
                                    multiple="multiple"
                                    data-placeholder="Pilih usaha">
                                    @foreach($company_options as $company)
                                        <option value="{{$company->id}}">{{$company->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            @if ($errors->has('selected_companies'))
                                <div class="invalid-feedback" style="display: block;">
                                    <strong>{{ $errors->first('selected_companies') }}</strong>
                                </div>
                            @endif
                        </div>
                    </div>
                    <div class="modal-footer bg-whitesmoke br">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div wire:ignore.self class="modal fade" tabindex="-1" role="dialog" id="mappingCompanyModal">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <form wire:submit.prevent="submitCompanyMapping()">
                    <div class="modal-header">
                        <h5 class="modal-title">Mapping Usaha</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Petugas</label>

                            <select class="form-control" wire:model="selected_staff">
                                <option value="">Pilih Petugas</option>
                                @foreach($staff_options as $staff)
                                    <option value="{{$staff->id}}">{{$staff->user->name}}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('selected_staff'))
                                <div class="invalid-feedback" style="display: block;">
                                    <strong>{{ $errors->first('selected_staff') }}</strong>
                                </div>
                            @endif
                        </div>
                    </div>
                    <div class="modal-footer bg-whitesmoke br">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <section class="section">
        <div class="section-header">
            <h1>Mapping Petugas</h1>
        </div>
        <div class="section-body">
            <div class="card">
                <div class="card-header">
                    <h4>
                        Mapping Usaha
                        {{--                        <a class="btn btn-success" href="{{route('staff.create')}}">+ Tambah Petugas</a>--}}
                    </h4>
                    <div class="card-header-action">
                        <div class="input-group">
                            <input type="text" class="form-control" wire:model.debounce.500ms="search_company"
                                   placeholder="Search">

                            <div class="input-group-btn">
                                <button class="btn btn-primary"><i class="fas fa-search"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Usaha</th>
                                <th>Pemilik Usaha</th>
                                <th>Petugas</th>
                                <th>Banjar</th>
                                @if(auth()->user()->isAdmin)
                                    <th>Action</th>
                                @endif
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($companies as $company)
                                <tr>
                                    <td>{{ ($companies->currentpage()-1) * $companies->perpage() + $loop->index + 1 }}</td>
                                    <td>{{$company->name}}</td>
                                    <td>{{$company->user->name}}</td>
                                    <td>
                                        <ul class="p-0">
                                            @if($company->staffs->count() > 0)
                                                @foreach($company->staffs as $staff)
                                                    <li>
                                                        {{$staff->user->name}}
                                                    </li>
                                                @endforeach
                                            @else
                                                -
                                            @endif
                                        </ul>
                                    </td>
                                    <td>{{$company->banjar->name ?? "-"}}</td>
                                    @if(auth()->user()->isAdmin)
                                        <td>
                                            <button class="btn btn-warning m-1"
                                                    wire:click="openMappingCompanyModal({{$company->id}})">
                                                <i class="fas fa-pencil-square"></i>
                                                Ubah
                                            </button>
                                        </td>
                                    @endif
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div class="float-right mx-4">
                            {{$companies->links()}}
                        </div>
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="card-header">
                    <h4>
                        Mapping Petugas
                        {{--                        <a class="btn btn-success" href="{{route('staff.create')}}">+ Tambah Petugas</a>--}}
                    </h4>
                    <div class="card-header-action">
                        <div class="input-group">
                            <input type="text" class="form-control" wire:model.debounce.500ms="search"
                                   placeholder="Search">

                            <div class="input-group-btn">
                                <button class="btn btn-primary"><i class="fas fa-search"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama</th>
                                <th>Telepon</th>
                                <th>Usaha - Pemilik Usaha</th>
                                @if(auth()->user()->isAdmin)
                                    <th>Action</th>
                                @endif
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($staffs as $staff)
                                <tr>
                                    <td>{{ ($staffs->currentpage()-1) * $staffs->perpage() + $loop->index + 1 }}</td>
                                    <td>{{$staff->user->name}}</td>
                                    <td>{{$staff->user->phone}}</td>
                                    <td>
                                        <ul class="p-0">
                                            @foreach($staff->companies as $company)
                                                <li>
                                                    {{$company->name}} - {{$company->user->name}}
                                                </li>
                                            @endforeach
                                        </ul>
                                    </td>
                                    @if(auth()->user()->isAdmin)
                                        <td>
                                            <button class="btn btn-warning m-1"
                                                    wire:click="openMappingModal({{$staff->id}})">
                                                <i class="fas fa-pencil-square"></i>
                                                Ubah
                                            </button>
                                        </td>
                                    @endif
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div class="float-right mx-4">
                            {{$staffs->links()}}
                        </div>
                    </div>
                </div>
            </div>


        </div>

    </section>
</div>

@push("js")
    <script>
        $("#company_select").select2({
            multiple: true,
        }).on('change', function () {
            @this.
            set('selected_companies', $(this).val())
        });

        Livewire.on('mappingModal:open', event => {
            $("#mappingModal").modal("show");
            let selected = @this.
            get("selected_companies");
            console.log(selected)
            $("#company_select").val(selected).trigger("change");
        });
        Livewire.on('mappingModal:close', event => {
            $("#company_select").val('').trigger('change')
            $("#mappingModal").modal("hide");
        });
        Livewire.on('mappingCompanyModal:open', event => {
            $("#mappingCompanyModal").modal("show");
        })
        Livewire.on('mappingCompanyModal:close', event => {
            $("#mappingCompanyModal").modal("hide");
        })

    </script>
@endpush
