<div>
    <div class="card">
        <form wire:submit.prevent="submit()">
            <div class="card-header">
                <h4>Ubah Petugas</h4>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Nama</label>
                            <input type="text" class="form-control @error('name') is-invalid @enderror"
                                   wire:model="name">
                            @error('name')
                            <div class="invalid-feedback">
                                {{$message}}
                            </div>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Telepon</label>
                            <input type="text" class="form-control @error('no_hp') is-invalid @enderror"
                                   wire:model="no_hp">
                            @error('no_hp')
                            <div class="invalid-feedback">
                                {{$message}}
                            </div>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Email</label>
                            <input type="email" class="form-control @error('email') is-invalid @enderror"
                                   wire:model="email">
                            @error('email')
                            <div class="invalid-feedback">
                                {{$message}}
                            </div>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Banjar</label>
                            <select class="form-control @error('banjar') is-invalid @enderror" wire:model="banjar">
                                <option value="">Pilih Banjar</option>
                                @foreach ($listBanjar as $banjar)
                                    <option value="{{$banjar->id}}">{{$banjar->name}}</option>
                                @endforeach
                            </select>
                            @error('banjar')
                            <div class="invalid-feedback">
                                {{$message}}
                            </div>
                            @enderror
                        </div>
                    </div>

                    <div class="col-md-6" x-data="{show: false}">
                        <div class="form-group">
                            <label class="custom-switch" style="padding-left: 0px">
                                <input
                                    x-model="show"
                                    type="checkbox"
                                    name="custom-switch-checkbox"
                                    class="custom-switch-input"
                                />
                                <span class="custom-switch-indicator"></span>
                                <span class="custom-switch-description">Ubah Kata Sandi</span>
                            </label>
                        </div>

                        <div class="form-group" x-show="show">
                            <label>Kata Sandi</label>
                            <input type="password" class="form-control @error('password') is-invalid @enderror"
                                   wire:model="password">
                            @error('password')
                            <div class="invalid-feedback">
                                {{$message}}
                            </div>
                            @enderror
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Saldo</label>
                            <input type="number" class="form-control @error('balance') is-invalid @enderror"
                                   wire:model="balance">
                            @error('balance')
                            <div class="invalid-feedback">
                                {{$message}}
                            </div>
                            @enderror
                        </div>
                    </div>

                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Photo</label>
                            <input type="file" accept="image/*"
                                   class="form-control @error('photo') is-invalid @enderror"
                                   wire:model="photo">
                            @error('photo')
                            <div class="invalid-feedback">
                                {{$message}}
                            </div>
                            @enderror
                            <div wire:loading wire:target="photo">
                                Uploading Image...
                            </div>
                            <div class="mt-4">
                                <div wire:loading.remove wire:target="photo">
                                    @if ($old_photo && !$photo)
                                        <img src="{{$old_photo}}" class="img-fluid " alt="photo" width="200">
                                    @elseif ($photo)
                                        <img alt="image" width="200" src="{{ $photo->temporaryUrl() }}"
                                             class="img-fluid">
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label>KTP</label>
                            <input type="file" accept="image/*" class="form-control @error('ktp') is-invalid @enderror"
                                   wire:model="ktp">
                            @error('ktp')
                            <div class="invalid-feedback">
                                {{$message}}
                            </div>
                            @enderror

                            <div wire:loading wire:target="ktp">
                                Uploading Image...
                            </div>
                            <div class="mt-4">
                                <div wire:loading.remove wire:target="ktp">
                                    @if ($old_ktp && !$ktp)
                                        <img src="{{$old_ktp}}" class="img-fluid " alt="photo" width="200">
                                    @elseif ($ktp)
                                        <img alt="image" width="200" src="{{ $ktp->temporaryUrl() }}"
                                             class="img-fluid">
                                    @endif
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div class="card-footer text-right">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </form>
    </div>
</div>
