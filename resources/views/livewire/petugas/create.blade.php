<div>
    <div class="card">
        <form wire:submit.prevent="submit()">
            <div class="card-header">
                <h4>Tambah Petugas</h4>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="text-base">Nama</label>
                            <input type="text" class="form-control @error('name') is-invalid @enderror"
                                   wire:model="name">
                            @error('name')
                            <div class="invalid-feedback">
                                {{$message}}
                            </div>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="text-base">Telepon</label>
                            <input type="text" class="form-control @error('no_hp') is-invalid @enderror"
                                   wire:model="no_hp">
                            @error('no_hp')
                            <div class="invalid-feedback">
                                {{$message}}
                            </div>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="text-base">Email</label>
                            <input type="email" class="form-control @error('email') is-invalid @enderror"
                                   wire:model="email">
                            @error('email')
                            <div class="invalid-feedback">
                                {{$message}}
                            </div>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="text-base">Password</label>
                            <input type="password" class="form-control @error('password') is-invalid @enderror"
                                   wire:model="password">
                            @error('password')
                            <div class="invalid-feedback">
                                {{$message}}
                            </div>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="text-base">Banjar</label>
                            <select class="form-control @error('banjar') is-invalid @enderror" wire:model="banjar">
                                <option value="">Pilih Banjar</option>
                                @foreach ($listBanjar as $banjar)
                                    <option value="{{$banjar->id}}">{{$banjar->name}}</option>
                                @endforeach
                            </select>
                            @error('banjar')
                            <div class="invalid-feedback">
                                {{$message}}
                            </div>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="text-base">Photo</label>
                            <span class="badge badge-success">Opsional</span>
                            <input type="file" accept="image/*"
                                   class="form-control @error('photo') is-invalid @enderror"
                                   wire:model="photo" name="photo" id="upload{{ $iteration }}">
                            @error('photo')
                            <div class="invalid-feedback">
                                {{$message}}
                            </div>
                            @enderror
                            <div wire:loading wire:target="photo">
                                Uploading Image...
                            </div>
                            <div wire:loading.remove wire:target="photo">
                                @if ($photo)
                                    <img alt="image" width="200" src="{{ $photo->temporaryUrl() }}"
                                         class="img-fluid">
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="text-base">KTP</label>
                            <input type="file" accept="image/*"
                                   class="form-control @error('ktp') is-invalid @enderror"
                                   wire:model="ktp">
                            @error('ktp')
                            <div class="invalid-feedback">
                                {{$message}}
                            </div>
                            @enderror
                            <div wire:loading wire:target="ktp">
                                Uploading Image...
                            </div>
                            <div wire:loading.remove wire:target="ktp">
                                @if ($ktp)
                                    <img alt="image" width="200" src="{{ $ktp->temporaryUrl() }}" class="img-fluid">
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-footer text-right">
                <div wire:loading wire:target="submit">
                    <img width="75" src="{{asset('svg/pulse.svg')}}" alt="">
                </div>
                <button type="submit" class="btn btn-primary">
                    Submit
                </button>
            </div>
        </form>
    </div>
</div>

@push('css')
    <style>
        .badge {
            padding: 4px 10px !important;
            font-size: 10px !important;
        }
    </style>
@endpush
