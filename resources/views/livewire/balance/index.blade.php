<div>
    <div wire:ignore.self class="modal fade" tabindex="-1" role="dialog" id="formModal">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <form wire:submit.prevent="post()">
                    <div class="modal-header">
                        <h5 class="modal-title">Tambah Setoran Petugas</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Petugas</label>
                            <select wire:model.defer="staffId" class="form-control">
                                <option value="">Pilih</option>
                                @foreach($staffs as $staff)
                                    <option value="{{$staff->id}}"> {{$staff->user->name}}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('staffId'))
                                <div class="invalid-feedback" style="display: block;">
                                    <strong>{{ $errors->first('staffId') }}</strong>
                                </div>
                            @endif
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label>Nominal Setoran</label>
                                    <input wire:model.defer="amount" type="number" class="form-control">
                                    @if ($errors->has('amount'))
                                        <div class="invalid-feedback" style="display: block;">
                                            <strong>{{ $errors->first('amount') }}</strong>
                                        </div>
                                    @endif
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label>Fee Petugas (opsional)</label>
                                    <input wire:model.defer="staffAmount" type="number" class="form-control">
                                    @if ($errors->has('staffAmount'))
                                        <div class="invalid-feedback" style="display: block;">
                                            <strong>{{ $errors->first('staffAmount') }}</strong>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Deskripsi (opsional)</label>
                            <textarea class="form-control" wire:model.defer="description"
                                      rows="3" style="height:100%"
                            ></textarea>
                            @if ($errors->has('description'))
                                <div class="invalid-feedback" style="display: block;">
                                    <strong>{{ $errors->first('description') }}</strong>
                                </div>
                            @endif
                        </div>

                        <div class="form-group">
                            <label>Tanggal</label>
                            <x-forms.date-picker
                                class="form-control"
                                wire:model.defer="balanceDate"/>
                            @if ($errors->has('balanceDate'))
                                <div class="invalid-feedback" style="display: block;">
                                    <strong>{{ $errors->first('balanceDate') }}</strong>
                                </div>
                            @endif
                        </div>


                    </div>
                    <div class="modal-footer bg-whitesmoke br">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div wire:ignore.self class="modal fade" tabindex="-1" role="dialog" id="editModal">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <form wire:submit.prevent="edit()">
                    <div class="modal-header">
                        <h5 class="modal-title">Ubah Setoran Petugas</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Petugas</label>
                            <select wire:model.defer="staffIdEdit" class="form-control">
                                <option value="">Pilih</option>
                                @foreach($staffs as $staff)
                                    <option value="{{$staff->id}}"> {{$staff->user->name}}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('staffIdEdit'))
                                <div class="invalid-feedback" style="display: block;">
                                    <strong>{{ $errors->first('staffIdEdit') }}</strong>
                                </div>
                            @endif
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label>Nominal Setoran</label>
                                    <input wire:model.defer="amountEdit" type="number" class="form-control">
                                    @if ($errors->has('amountEdit'))
                                        <div class="invalid-feedback" style="display: block;">
                                            <strong>{{ $errors->first('amountEdit') }}</strong>
                                        </div>
                                    @endif
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label>Fee Petugas (opsional)</label>
                                    <input wire:model.defer="staffAmountEdit" type="number" class="form-control">
                                    @if ($errors->has('staffAmountEdit'))
                                        <div class="invalid-feedback" style="display: block;">
                                            <strong>{{ $errors->first('staffAmountEdit') }}</strong>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Deskripsi (opsional)</label>
                            <textarea class="form-control" wire:model.defer="descriptionEdit"
                                      rows="3" style="height:100%"
                            ></textarea>
                            @if ($errors->has('descriptionEdit'))
                                <div class="invalid-feedback" style="display: block;">
                                    <strong>{{ $errors->first('descriptionEdit') }}</strong>
                                </div>
                            @endif
                        </div>

                        <div class="form-group">
                            <label>Tanggal</label>
                            <x-forms.date-picker
                                class="form-control"
                                wire:model.defer="balanceDateEdit"/>
                            @if ($errors->has('balanceDateEdit'))
                                <div class="invalid-feedback" style="display: block;">
                                    <strong>{{ $errors->first('balanceDateEdit') }}</strong>
                                </div>
                            @endif
                        </div>


                    </div>
                    <div class="modal-footer bg-whitesmoke br">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <section class="section">

        <div class="section-header">
            <h1>Setoran Petugas</h1>
            {{-- <div class="section-header-breadcrumb">
                <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
                <div class="breadcrumb-item"><a href="#">Layout</a></div>
                <div class="breadcrumb-item">Default Layout</div>
            </div> --}}
        </div>


        <div class="card">
            <div class="card-header">
                <h4>
                    @if(auth()->user()->isAdmin)
                        <button class="btn btn-success" data-toggle="modal" data-target="#formModal">
                            + Tambah Setoran Petugas
                        </button>
                    @endif
                </h4>
                <div class="card-header-action">
                    <div class="input-group">
                        <x-forms.date-picker
                            class="form-control"
                            wire:model="date" placeholder="Pilih tanggal"
                            autocomplete="off"/>
                        <div class="input-group-btn">
                            <button class="btn btn-danger" wire:click.prevent="removeFilter()"><i
                                    class="fas fa-times"></i></button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-body p-0">
                <div class="table-responsive">
                    <table class="table table-striped" id="table-data">
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>Petugas</th>
                            <th>Nominal Setoran</th>
                            <th>Fee Petugas</th>
                            <th>Deskripsi</th>
                            <th>Tanggal</th>
                            @if(auth()->user()->isAdmin)
                                <th>Action</th>
                            @endif
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($balances as $balance)
                            <tr>
                                <td>{{ ($balances->currentpage()-1) * $balances->perpage() + $loop->index + 1 }}</td>
                                <td>{{$balance->staff->user->name}}</td>
                                <td>@currency($balance->amount)</td>
                                <td>@currency($balance->staff_amount ?? 0)</td>
                                <td>{{$balance->notes ?? "-"}}</td>
                                <td>{{$balance->created_at->format("d M Y")}}</td>
                                @if(auth()->user()->isAdmin)
                                    <td>
                                        <button class="btn btn-warning m-1"
                                                wire:click.prevent="openEdit({{$balance->id}})"><i
                                                class="fa-solid fa-pen-to-square"></i>
                                        </button>
                                        <button class="btn btn-danger m-1"
                                                wire:click.prevent="deleteConfirmation({{$balance->id}})"><i
                                                class="fa fa-trash"></i></button>
                                    </td>
                                @endif
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                    <div class="float-right mx-4">
                        {{$balances->links()}}
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
</section>
</div>
@push("js")
    <script>
        Livewire.on('editModal:open', event => {
            $('#editModal').modal('show');
        })
    </script>
@endpush
