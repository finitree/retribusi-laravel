<div>
    <div>
        <section class="section">
            <div class="section-header">
                <h1>Pengaturan</h1>
            </div>
            <div class="section-body">
                @include("common.alert")
                <div class="card">
                    <div class="card-header">
                        <h4>
                            Pengaturan Sistem
                        </h4>
                    </div>
                    <div class="card-body">
                        <form wire:submit.prevent="submit()">
                            <label>Pembayaran Cicilan</label>
                            <div class="form-group mt-1">
                                <label class="custom-switch" style="padding-left: 0px">
                                    <input
                                        {{--                                    checked="{{ $setting->is_instalment_available == 1 ? 'checked' : '' }}"--}}
                                        wire:model.defer="is_instalment_available"
                                        type="checkbox"
                                        name="custom-switch-checkbox"
                                        class="custom-switch-input"
                                    />
                                    <span class="custom-switch-indicator"></span>
                                    <span class="custom-switch-description">Pembayaran Cicilan</span>
                                </label>
                            </div>
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </form>


                    </div>
                </div>

        </section>

    </div>
</div>
