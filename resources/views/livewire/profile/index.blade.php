<div>
    <section class="section">
        <div class="section-header">
            <h1>Ubah Profile</h1>
        </div>

        <div class="section-body">
            <div class="card">
                <form wire:submit.prevent="patch()">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Nama</label>
                                    <input wire:model.defer="name" type="text"
                                           class="form-control @error("name") is-invalid @enderror">
                                    @error("name")
                                    <div class="invalid-feedback">
                                        <strong>{{ $message }}</strong>
                                    </div>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Telepon</label>
                                    <input
                                        wire:model.defer="phone"
                                        type="text"
                                        class="form-control @error("phone") is-invalid @enderror">
                                    @error("phone")
                                    <div class="invalid-feedback" style="display: block;">
                                        <strong>{{ $message}}</strong>
                                    </div>
                                    @enderror
                                </div>
                            </div>


                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Email</label>
                                    <input
                                        wire:model.defer="email"
                                        type="email"
                                        class="form-control @error("email") is-invalid @enderror">
                                    @error("email")
                                    <div class="invalid-feedback">
                                        <strong>{{ $message }}</strong>
                                    </div>
                                    @enderror
                                </div>
                            </div>


                        </div>
                        <div class="row mt-3">
                            <div class="col-md-6" x-data="{show : false}">
                                <div class="form-group">
                                    <label class="custom-switch" style="padding-left: 0px">
                                        <input
                                            x-model="show"
                                            type="checkbox"
                                            name="custom-switch-checkbox"
                                            class="custom-switch-input"
                                        />
                                        <span class="custom-switch-indicator"></span>
                                        <span class="custom-switch-description">Ubah Kata Sandi</span>
                                    </label>
                                </div>

                                <div class="form-group" x-show="show">
                                    <label>Kata Sandi</label>
                                    <input wire:model.defer="password"
                                           type="password"
                                           class="form-control @error("password") is-invalid @enderror">
                                    @error("password")
                                    <div class="invalid-feedback">
                                        <strong>{{ $message }}</strong>
                                    </div>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Foto</label>
                                    <input type="file"
                                           class="form-control @error("photo") is-invalid @enderror"
                                           accept="image/*"
                                           wire:model.defer="photo">
                                    @error("photo")
                                    <div class="invalid-feedback" style="display: block;">
                                        <strong>
                                            {{$message}}
                                        </strong>
                                    </div>
                                    @enderror

                                    <div wire:loading wire:target="photo">
                                        Uploading Image...
                                    </div>
                                    <div wire:loading.remove wire:target="photo">
                                        @if($old_photo && !$photo)
                                            <img alt="image" width="200" src="{{ $old_photo }}"
                                                 class="img-fluid mt-4">
                                        @endif
                                        @if ($photo)
                                            <img alt="image" width="200" src="{{ $photo->temporaryUrl() }}"
                                                 class="img-fluid mt-4">
                                        @endif
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer text-right">
                        <div wire:loading wire:target="patch">
                            <img width="75" src="{{asset('svg/pulse.svg')}}" alt="">
                        </div>
                        <button type="submit" class="btn btn-primary">
                            Submit
                        </button>
                    </div>

                </form>
            </div>

        </div>
    </section>
</div>
