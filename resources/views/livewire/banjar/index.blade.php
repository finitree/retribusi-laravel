<div>
    <div wire:ignore.self class="modal fade" tabindex="-1" role="dialog" id="formModal">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <form wire:submit.prevent="postBanjar()">
                    <div class="modal-header">
                        <h5 class="modal-title">Tambah Banjar</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Banjar</label>
                            <input wire:model.defer="name" type="text" class="form-control" name="banjar">
                            @if ($errors->has('name'))
                                <div class="invalid-feedback" style="display: block;">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </div>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>Alamat</label>
                            <textarea class="form-control" wire:model.defer="address" name="address"
                                      rows="3" style="height:100%"
                            ></textarea>
                            @if ($errors->has('address'))
                                <div class="invalid-feedback" style="display: block;">
                                    <strong>{{ $errors->first('address') }}</strong>
                                </div>
                            @endif
                        </div>
                    </div>
                    <div class="modal-footer bg-whitesmoke br">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div wire:ignore.self class="modal fade" tabindex="-1" role="dialog" id="editModal">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <form wire:submit.prevent="editBanjar()">
                    <div class="modal-header">
                        <h5 class="modal-title">Ubah Banjar</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Banjar</label>
                            <input wire:model.defer="nameEdit" type="text" class="form-control" name="nameEdit">
                            @if ($errors->has('nameEdit'))
                                <div class="invalid-feedback" style="display: block;">
                                    <strong>{{ $errors->first('nameEdit') }}</strong>
                                </div>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>Alamat</label>
                            <textarea class="form-control" wire:model.defer="addressEdit" name="address"
                                      rows="3" style="height:100%"
                            ></textarea>
                            @if ($errors->has('addressEdit'))
                                <div class="invalid-feedback" style="display: block;">
                                    <strong>{{ $errors->first('addressEdit') }}</strong>
                                </div>
                            @endif
                        </div>
                    </div>
                    <div class="modal-footer bg-whitesmoke br">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <section class="section">

        <div class="section-header">
            <h1>Banjar</h1>
            {{-- <div class="section-header-breadcrumb">
                <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
                <div class="breadcrumb-item"><a href="#">Layout</a></div>
                <div class="breadcrumb-item">Default Layout</div>
            </div> --}}
        </div>

        <div class="section-body">
            <div class="card">
                <div class="card-header">
                    <h4>
                        @if(auth()->user()->isAdmin)
                            <button class="btn btn-success" data-toggle="modal" data-target="#formModal">+ Tambah Banjar
                            </button>
                        @endif
                    </h4>
                    <div class="card-header-action">
                        <div class="input-group">
                            <input type="text" class="form-control" wire:model="search" placeholder="Search">
                            <div class="input-group-btn">
                                <button class="btn btn-primary"><i class="fas fa-search"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body p-0">
                    <div class="table-responsive">
                        <table class="table table-striped" id="table-data">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Banjar</th>
                                <th>Alamat</th>
                                @if(auth()->user()->isAdmin)
                                    <th>Action</th>
                                @endif
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($banjars as $banjar)
                                <tr>
                                    <td>{{ ($banjars->currentpage()-1) * $banjars->perpage() + $loop->index + 1 }}</td>
                                    <td>{{$banjar->name}}</td>
                                    <td>{{$banjar->address}}</td>
                                    @if(auth()->user()->isAdmin)
                                        <td>
                                            <button class="btn btn-warning m-1"
                                                    wire:click.prevent="openEdit({{$banjar->id}})">
                                                <i class="fa-solid fa-pen-to-square"></i>
                                            </button>
                                            <button class="btn btn-danger m-1"
                                                    wire:click.prevent="deleteConfirmationBanjar({{$banjar->id}})"><i
                                                    class="fa fa-trash"></i></button>
                                        </td>
                                    @endif
                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                        <div class="float-right mx-4">
                            {{$banjars->links()}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@push("js")
    <script>
        Livewire.on('editModal:open', event => {
            $('#editModal').modal('show');
        })
    </script>
@endpush
