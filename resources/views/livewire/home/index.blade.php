<div>
    <section class="section">
        <div class="section-header">
            <h1>Dashboard</h1>
        </div>
        <div class="row">
            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                <div class="card card-statistic-1">
                    <div class="card-icon bg-danger">
                        <i class="fas fa-building"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header">
                            <h4>Total Usaha</h4>
                        </div>
                        <div class="card-body">
                            {{$totalCompany}}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                <div class="card card-statistic-1">
                    <div class="card-icon bg-warning">
                        <i class="far fa-file"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header">
                            <h4>Jumlah Transaksi Hari Ini</h4>
                        </div>
                        <div class="card-body">
                            {{$totalTransactionToday}}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                <div class="card card-statistic-1">
                    <div class="card-icon bg-success">
                        <i class="fas fa-money-bills"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header">
                            <h4>Nominal Transaksi Hari Ini</h4>
                        </div>
                        <div class="card-body">
                            @currency($totalAmountTransactionToday)
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                <div class="card card-statistic-1">
                    <div class="card-icon bg-primary">
                        <i class="fas fa-user-group"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header">
                            <h4>Total Petugas</h4>
                        </div>
                        <div class="card-body">
                            {{$totalStaff}}
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="section-body">
            <div class="card" x-data="{ tab: 'daily' }">
                <div class="card-header">
                    <h4>Grafik Transaksi {{\Carbon\Carbon::now()->year}}</h4>
                    <div class="card-header-action">
                        <ul class="nav nav-pills" id="myTab3" role="tablist">
                            <li class="nav-item" @click="tab = 'daily'">
                                <a class="nav-link" :class="{ 'active': tab === 'daily' }" data-toggle="tab" href="#"
                                   role="tab"
                                   aria-controls="profile" aria-selected="false">Harian</a>
                            </li>
                            <li class="nav-item" @click="tab = 'monthly'">
                                <a class="nav-link" :class="{ 'active': tab === 'monthly' }" data-toggle="tab" href="#"
                                   role="tab"
                                   aria-controls="contact" aria-selected="false">Bulanan</a>
                            </li>
                        </ul>
                    </div>

                </div>
                <div class="card-body">
                    <div style="height: 24rem;" x-show="tab === 'daily'">
                        <livewire:livewire-column-chart
                            :column-chart-model="$dailyTransactionChart"
                        />
                    </div>
                    <div style="height: 24rem;" x-show="tab === 'monthly'">
                        <livewire:livewire-column-chart
                            :column-chart-model="$monthlyTransactionChart"
                        />
                    </div>
                </div>
            </div>


            <div class="section-body">

                <div class="card">
                    <div class="card-header">
                        <h4>Grafik Setoran {{\Carbon\Carbon::now()->year}}</h4>
                    </div>
                    <div class="card-body">
                        <div style="height: 24rem;">
                            <livewire:livewire-line-chart
                                :line-chart-model="$monthlyBalanceChart"
                            />
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-header">
                        <h4>Dudukan Usaha</h4>
                        <div class="card-header-action">
                            <div class="input-group">
                                <x-forms.date-picker
                                    class="form-control"
                                    wire:model="date" placeholder="Pilih tanggal"
                                    autocomplete="off"/>
                                <div class="input-group-btn">
                                    <button class="btn btn-danger" wire:click.prevent="removeFilter()"><i
                                            class="fas fa-times"></i></button>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="card-body p-0">
                        <div class="table-responsive">
                            <table class="table table-striped" id="table-data">
                                <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Petugas</th>
                                    <th>Usaha</th>
                                    <th>Nominal</th>
                                    <th>Tanggal</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($transactions as $transaction)
                                    <tr>
                                        <td>{{ ($transactions->currentpage()-1) * $transactions->perpage() + $loop->index + 1 }}</td>
                                        <td>{{$transaction->staff->user->name}}</td>
                                        <td>
                                            @if($transaction->detail)
                                                @foreach($transaction->detail->unique('company_id') as $detail)
                                                    <li>
                                                        {{$detail->company->name}} - {{$detail->company->user->name}}
                                                    </li>
                                                @endforeach
                                            @endif
                                        </td>
                                        <td>
                                            @currency($transaction->total_amount)
                                        </td>
                                        <td>{{$transaction->created_at->format("d M Y")}}</td>
                                    </tr>
                                @endforeach

                                </tbody>
                            </table>
                            <div class="float-right mx-4">
                                {{$transactions->links()}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </section>
</div>
