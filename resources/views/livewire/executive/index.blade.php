<div>
    <div wire:ignore.self class="modal fade" tabindex="-1" role="dialog" id="formModal">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <form wire:submit.prevent="post()">
                    <div class="modal-header">
                        <h5 class="modal-title">Tambah Atasan</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Nama</label>
                            <input wire:model.defer="name" type="text" class="form-control">
                            @if ($errors->has('name'))
                                <div class="invalid-feedback" style="display: block;">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </div>
                            @endif
                        </div>

                        <div class="form-group">
                            <label>Email</label>
                            <input wire:model.defer="email" type="email" class="form-control">
                            @if ($errors->has('email'))
                                <div class="invalid-feedback" style="display: block;">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </div>
                            @endif
                        </div>

                        <div class="form-group">
                            <label>Telepon</label>
                            <input wire:model.defer="phone" type="text" class="form-control">
                            @if ($errors->has('phone'))
                                <div class="invalid-feedback" style="display: block;">
                                    <strong>{{ $errors->first('phone') }}</strong>
                                </div>
                            @endif
                        </div>

                        <div class="form-group">
                            <label>Kata Sandi</label>
                            <input wire:model.defer="password" type="password" class="form-control">
                            @if ($errors->has('password'))
                                <div class="invalid-feedback" style="display: block;">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </div>
                            @endif
                        </div>
                    </div>
                    <div class="modal-footer bg-whitesmoke br">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div wire:ignore.self class="modal fade" tabindex="-1" role="dialog" id="editModal">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <form wire:submit.prevent="edit()">
                    <div class="modal-header">
                        <h5 class="modal-title">Ubah Atasan</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Nama</label>
                            <input wire:model.defer="nameEdit" type="text" class="form-control">
                            @if ($errors->has('nameEdit'))
                                <div class="invalid-feedback" style="display: block;">
                                    <strong>{{ $errors->first('nameEdit') }}</strong>
                                </div>
                            @endif
                        </div>

                        <div class="form-group">
                            <label>Email</label>
                            <input wire:model.defer="emailEdit" type="email" class="form-control">
                            @if ($errors->has('emailEdit'))
                                <div class="invalid-feedback" style="display: block;">
                                    <strong>{{ $errors->first('emailEdit') }}</strong>
                                </div>
                            @endif
                        </div>

                        <div class="form-group">
                            <label>Telepon</label>
                            <input wire:model.defer="phoneEdit" type="text" class="form-control">
                            @if ($errors->has('phoneEdit'))
                                <div class="invalid-feedback" style="display: block;">
                                    <strong>{{ $errors->first('phoneEdit') }}</strong>
                                </div>
                            @endif
                        </div>

                        <div x-data="{show : false}">
                            <div class="form-group">
                                <label class="custom-switch" style="padding-left: 0px">
                                    <input
                                        x-model="show"
                                        type="checkbox"
                                        name="custom-switch-checkbox"
                                        class="custom-switch-input"
                                    />
                                    <span class="custom-switch-indicator"></span>
                                    <span class="custom-switch-description">Ubah Kata Sandi</span>
                                </label>
                            </div>
                            <div class="form-group" x-show="show">
                                <label>Kata Sandi</label>
                                <input wire:model.defer="passwordEdit" type="password" class="form-control">
                                @if ($errors->has('passwordEdit'))
                                    <div class="invalid-feedback" style="display: block;">
                                        <strong>{{ $errors->first('passwordEdit') }}</strong>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer bg-whitesmoke br">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <section class="section">

        <div class="section-header">
            <h1>Atasan</h1>
            {{-- <div class="section-header-breadcrumb">
                <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
                <div class="breadcrumb-item"><a href="#">Layout</a></div>
                <div class="breadcrumb-item">Default Layout</div>
            </div> --}}
        </div>

        <div class="section-body">
            <div class="card">
                <div class="card-header">
                    <h4>
                        @if(auth()->user()->isAdmin)
                            <button class="btn btn-success" data-toggle="modal" data-target="#formModal">+ Tambah Atasan
                            </button>
                        @endif
                    </h4>
                    <div class="card-header-action">
                        <div class="input-group">
                            <input type="text" class="form-control" wire:model="search" placeholder="Search">
                            <div class="input-group-btn">
                                <button class="btn btn-primary"><i class="fas fa-search"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body p-0">
                    <div class="table-responsive">
                        <table class="table table-striped" id="table-data">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama</th>
                                <th>Email</th>
                                <th>Telepon</th>
                                @if(auth()->user()->isAdmin)
                                    <th>Action</th>
                                @endif
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($executives as $executive)
                                <tr>
                                    <td>{{ ($executives->currentpage()-1) * $executives->perpage() + $loop->index + 1 }}</td>
                                    <td>{{$executive->name}}</td>
                                    <td>{{$executive->email}}</td>
                                    <td>{{$executive->phone}}</td>
                                    @if(auth()->user()->isAdmin)
                                        <td>
                                            <button class="btn btn-warning m-1"
                                                    wire:click.prevent="openEdit({{$executive->id}})">
                                                <i class="fa-solid fa-pen-to-square"></i>
                                            </button>
                                            <button class="btn btn-danger m-1"
                                                    wire:click.prevent="deleteConfirmation({{$executive->id}})"><i
                                                    class="fa fa-trash"></i></button>
                                        </td>
                                    @endif
                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                        <div class="float-right mx-4">
                            {{$executives->links()}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@push("js")
    <script>
        Livewire.on('editModal:open', event => {
            $('#editModal').modal('show');
        })
    </script>
@endpush
