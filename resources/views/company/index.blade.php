@extends('layouts.app')

@section('content')
    <!-- ========== title-wrapper start ========== -->
    <div class="title-wrapper pt-30">
        <div class="row align-items-center">
            <div class="col-md-6">
                <div class="title mb-30">
                    <h2>Petugas Retribusi</h2>
                </div>
            </div>
            <!-- end col -->
        </div>
        <!-- end row -->
    </div>
    <!-- ========== title-wrapper end ========== -->

    <div class="card-styles">
        <div class="card-style-3 mb-30">
            <div class="card-content">
                <div class="table-wrapper table-responsive">

                    <table class="table striped-table mt-3">
                        <thead>
                        <tr>
                            <th><h6>Nama</h6></th>
                            <th><h6>Status</h6></th>
                            <th><h6>Aksi</h6></th>
                        </tr>
                        <!-- end table row-->
                        </thead>
                        <tbody>
                        @foreach($companies as $item)
                            <tr>
                                <td>
                                    <p>{{ $item->name}}</p>
                                </td>
                                <td>
                                    <p>{{ $item->status}}</p>
                                </td>
                                <td>
                                    @if ($item->status == "wait_verified")
                                        <form action="{{route('company.update',['company' => $item->id])}}" method="POST">
                                            @method("PUT")
                                            @csrf

                                            <div class="select-style-1">
                                                <label>Tempekan</label>
                                                <div class="select-position">
                                                  <select required name="tempekan_id">
                                                    <option value="">Pilih tempekan</option>
                                                    @foreach ($tempekans as $item)
                                                        <option value="{{$item->id}}">{{$item->name}}</option>
                                                    @endforeach

                                                  </select>
                                                </div>
                                              </div>

                                            <button type="submit" class="btn btn-primary">Validasi</button>
                                        </form>
                                    @endif
                                    {{-- <form action="{{route('staff.destroy',['staff' => $user])}}" method="POST">
                                        @method("DELETE")
                                        @csrf
                                        <button class="btn btn-danger" type="submit"  onclick="return confirm('Apakah anda yakin?')">Hapus</button>
                                    </form> --}}

                                </td>
                            </tr>
                        @endforeach
                        <!-- end table row -->
                        </tbody>
                    </table>
                    <!-- end table -->

                    {{ $companies->links() }}
                </div>

            </div>
        </div>
    </div>
@endsection
