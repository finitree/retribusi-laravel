@extends('layouts.auth')

@section('content')
    <div class="container mt-5">
        <div class="row">
            <div class="col-12 col-sm-8 offset-sm-2 col-md-6 offset-md-3 col-lg-6 offset-lg-3 col-xl-4 offset-xl-4">
                <div class="login-brand">
                    <img src="{{ asset('admin/img/logo.png') }}" alt="logo" width="100"
                         class="center align-self-center"
                    >
                </div>

                <div class="card card-primary">
                    <div class="card-header">
                        <h4>Reset Password</h4>
                    </div>


                    <div class="card-body">
                        @include('common.alert')
                        <form action="{{ route('password.update') }}" method="POST">
                            @csrf
                            <input type="hidden" value="{{$token}}" name="token">
                            <div class="col-12">
                                <div class="form-group">
                                    <label for="email">{{ __('Email') }}</label>
                                    <input class="form-control" type="email"
                                           name="email" id="email" value="{{ $email ?? old('email') }}"
                                           placeholder="{{ __('Email') }}" required autocomplete="email" autofocus>
                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <!-- end col -->
                            <div class="col-12">
                                <div class="form-group">
                                    <label>{{ __('Password') }}</label>
                                    <input type="password" class="form-control"
                                           name="password" id="password" placeholder="{{ __('Password') }}"
                                           required autocomplete="new-password">
                                    @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <!-- end col -->
                            <div class="col-12">
                                <div class="form-group">
                                    <label>{{ __('Confirm Password') }}</label>
                                    <input type="password" class="form-control"
                                           name="password_confirmation" id="password_confirmation"
                                           placeholder="{{ __('Confirm Password') }}" required
                                           autocomplete="new-password">
                                    @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>


                            <div class="form-group">
                                <button type="submit" class="btn btn-primary btn-lg btn-block" tabindex="4">
                                    {{ __('Reset') }}
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="simple-footer">
                    Copyright &copy; 2022
                </div>
            </div>
        </div>
    </div>

    <!-- end col -->
@endsection
