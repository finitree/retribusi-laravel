<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CompanyCloseSchedule extends Model
{
    use HasFactory;

    protected $table = "companies_close_schedule";
    protected $guarded = [];

    protected $appends = ['date_start_clear', 'date_end_clear'];

    public function getDateStartClearAttribute()
    {
        return Carbon::parse($this->date_start)->format('d F Y');
    }

    public function getDateEndClearAttribute()
    {
        return Carbon::parse($this->date_end)->format('d F Y');
    }
}
