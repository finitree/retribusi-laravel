<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tempekan extends Model
{
    use HasFactory, SoftDeletes;

    protected $guarded = [];
    // protected $with = ['banjar'];

    public function banjar()
    {
        return $this->belongsTo(Banjar::class);
    }
    public function companies()
    {
        return $this->hasMany(Company::class);
    }
    public function staffs()
    {
        return $this->hasMany(Staff::class);
    }
}
