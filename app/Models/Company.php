<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Company extends Model
{
    use HasFactory, SoftDeletes;

    const BLOCKED = 'blocked';
    const WAIT_VERIFIED = 'wait_verified';
    const VERIFIED = 'verified';

    protected $fillable = [
        'user_id',
        'banjar_id',
        'company_type_id',
        'subscription_id',
        'name',
        'latitude',
        'longitude',
        'address',
        'photos',
        'documents',
        'agreement_document',
        'subscription_amount',
        'status'
    ];

    protected $with = ['subscription', 'banjar', 'company_operational_schedule', 'unpaid_transaction', 'user'];

    protected $appends = ['transaction_status', 'company_close', 'next_transaction', 'unpaid_amount', 'company_status'];


    public function transactions()
    {
        return $this->hasMany(TransactionDetail::class, 'company_id', 'id');
    }

    public function getCompanyCloseAttribute()
    {
        $weekMap = [
            0 => 7,
            1 => 1,
            2 => 2,
            3 => 3,
            4 => 4,
            5 => 5,
            6 => 6,
        ];
        $currentTime = Carbon::parse(Carbon::now()->format('H:i'))->addHours(8);
        $day = $weekMap[Carbon::now()->dayOfWeek];
        $currentSchedule = CompanyOperationalSchedule::where('company_id', $this->id)->where('day', $day)->first();
        $open = Carbon::parse($currentSchedule->opening_hours ?? "00:00")->addHours(8);
        $close = Carbon::parse($currentSchedule->closing_hours ?? "00:00")->addHours(8);
        $isScheduleOpen = ($open->lessThan($currentTime)) && ($close->greaterThan($currentTime));
        $isOpenBySchedule = $currentSchedule->is_open == 1 && $isScheduleOpen;


        $obj = new BaseModel();
        $currentDate = date(Carbon::now()->format('Y-m-d'));
        $schedules = CompanyCloseSchedule::where('company_id', $this->id)->whereDate('date_start', '<=', $currentDate)->whereDate('date_end', '>=', $currentDate)->latest()->get();

        $obj->current_schedule = $currentSchedule;
//        cuti
        $isCuti = $schedules->count() > 0;
        if ($isCuti) {
            $obj->is_cuti = true;
            $obj->is_close = true;
            $obj->schedule = $schedules->first();
        } else {
            $obj->is_cuti = false;
            $obj->is_close = false;
            if (!$isOpenBySchedule) {
                $obj->is_close = true;
            }
        }

        return $obj;
    }


    public function getUnpaidAmountAttribute()
    {
        return $this->unpaid_transaction->sum('amount');
    }

    public function getNextTransactionAttribute()
    {
        if ($this->verified_at != null && $this->status == Company::VERIFIED) {
            $next = new NextTransaction();
            $unpaidCount = CompanyUnpaidTransaction::where('company_id', $this->id)->get()->count();
            $subscriptionType = strtolower($this->subscription->subscription_type->category);
            if ($subscriptionType == "harian") {
                $parsedDate = $this->getLastTransactionAttribute();
                if ($this->getTransactionStatusAttribute() == Transaction::PAID) {
                    $nextDate = MDate::today()->addDay();
                } else {
                    $nextDate = MDate::today();
                }
                $next->date = $nextDate;

                $next->subscription_qty = $unpaidCount;
//                $next->subscription_qty = $parsedDate->diffInDays($nextDate) + 1;
            } else if ($subscriptionType == "bulanan") {

                $parsedDate = $this->getLastTransactionAttribute();
                //if paid
                if ($parsedDate->month == date('m')) {
                    $nextMonth = $parsedDate->copy()->addMonth();
                    $next->date = $nextMonth;
                    $next->subscription_qty = 1;
                } else {
                    $diffMonth =
                        $parsedDate->copy()->diffInMonths(now());
                    $nextPaymentDate = $parsedDate->addMonths($diffMonth);
                    $next->date = $nextPaymentDate;

                    if ($nextPaymentDate->lessThan(MDate::today())) {
                        $next->date = $nextPaymentDate->copy()->addMonth();
                        $diffMonth += 1;
                    }

                    $next->subscription_qty = $unpaidCount;
//                    $next->subscription_qty = $diffMonth;
                }
            }
            $next->subscription_amount = $this->subscription_amount;

            return $next;
        } else {
            return null;
        }
    }

    public function getLastTransactionAttribute()
    {
        $lastTransaction = TransactionDetail::where('company_id', $this->id)->latest()->first();
        $lastTransactionDate = $this->getIsTransactionHappenAttribute() ? $lastTransaction->transaction_at : $this->verified_at;
        $parsedDate = $this->parseNolSDate($lastTransactionDate);
        return $parsedDate;
    }

    public function getIsTransactionHappenAttribute()
    {
        $lastTransaction = TransactionDetail::where('company_id', $this->id)->latest()->first();
        return $lastTransaction != null;
    }

    private function parseNolSDate($date)
    {
        return MDate::parse($date)->startOfDay()->addHours(8);
    }

    public function getTransactionStatusAttribute()
    {

        $unpaidCount = CompanyUnpaidTransaction::where('company_id', $this->id)->count();
        return $unpaidCount == 0 ? Transaction::PAID : Transaction::UNPAID;

//        $subscriptionType = strtolower($this->subscription->subscription_type->category);
//        if ($subscriptionType == "harian") {
//            $transactionCount = TransactionDetail::where('company_id', $this->id)->whereDate('transaction_at', '=', date('Y-m-d'))->count();
//            return $transactionCount == 0 ? Transaction::UNPAID : Transaction::PAID;
//        } else if ($subscriptionType == "bulanan") {
//            $transactionCount = TransactionDetail::where('company_id', $this->id)->whereMonth('transaction_at', '=', date('m'))->whereDay('transaction_at','>=',date('d'))->count();
//            return $transactionCount == 0 ? Transaction::UNPAID : Transaction::PAID;
//        } else {
//            return 'TransactionStatus';
//        }
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function banjar()
    {
        return $this->belongsTo(Banjar::class);
    }

    public function subscription()
    {
        return $this->belongsTo(Subscription::class);
    }

//    public function company_scale()
//    {
//        return $this->belongsTo(CompanyScale::class);
//    }

    public function getCompanyTypeAttribute()
    {
        return $this->subscription->company_type;
    }

    public function company_operational_schedule()
    {
        return $this->hasMany(CompanyOperationalSchedule::class, 'company_id');
    }

    public function unpaid_transaction()
    {
        return $this->hasMany(CompanyUnpaidTransaction::class, 'company_id');
    }

    public function getSubscriptionTypeAttribute()
    {
        return strtolower($this->subscription->subscription_type->category);
    }

    public function scopeOwnership($query)
    {
        return $query->where('user_id', auth()->id());
    }

    // Scope

    public function scopeStaffArea($query)
    {
//        $banjarId = auth()->user()->staff->banjar->id;
//        return $query->where('banjar_id', $banjarId);
        $staff = Staff::where('user_id', auth()->id())->first();
        $staffId = $staff->id;
        $companyStaffIds = StaffCompany::select("company_id")->where("staff_id", $staffId)->get();
        return $query->whereIn('id', $companyStaffIds);
    }

    public function scopeActive($query)
    {
        return $query->where('status', Company::VERIFIED);
    }

    private function parseDate($date)
    {
        return MDate::parse($date)->startOfDay()->addHours(8);
    }

    public function getCompanyStatusAttribute()
    {
        if ($this->status == Company::BLOCKED) {
            return 'Ditolak';
        } else if ($this->status == Company::WAIT_VERIFIED) {
            return 'Menunggu Verifikasi';
        } else {
            return 'Terverifikasi';
        }
    }

    public function staffs()
    {
        return $this->belongsToMany(Staff::class, 'staff_companies', 'company_id', 'staff_id');
    }
}
