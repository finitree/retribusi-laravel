<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TransactionDetail extends Model
{
    use HasFactory;

    protected $table = "transaction_details";
    protected $guarded = [];
//    protected $with = ['transaction', 'subscription', 'penalty'];
    protected $with = ['company'];
    public function transaction()
    {
        return $this->belongsTo(Transaction::class);
    }

    public function subscription()
    {
        return $this->belongsTo(Subscription::class);
    }

    public function penalty()
    {
        return $this->belongsTo(Penalty::class);
    }

    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id');
    }
}
