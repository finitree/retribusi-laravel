<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StaffCompany extends Model
{
    use HasFactory;

    protected $table = "staff_companies";
    public $timestamps = false;
    protected $guarded = [];
}
