<?php

namespace App\Models;

use Illuminate\Contracts\Auth\CanResetPassword;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable implements CanResetPassword
{
    use HasApiTokens, HasFactory, Notifiable, SoftDeletes;

    const ADMIN = 'admin';
    const OWNER = 'owner';
    const STAFF = 'staff';

    const CLOUDINARY_TRANSFORMATION_PROFILE = [
        'quality' => 'auto',
        'fetch_format' => 'auto',
        'width' => 512,
        'height' => 512,
        'crop' => 'fill',
        'gravity' => 'face'
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'phone',
        'role',
        'photo',
        'ktp'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */

//    protected $with = ['staff'];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function staff()
    {
        return $this->hasOne(Staff::class);
    }

    public function companies()
    {
        return $this->hasMany(Company::class);
    }

    public function phone()
    {
        return 'phone';
    }

    public function getIsAdminAttribute()
    {
        return $this->role == "admin";
    }
}
