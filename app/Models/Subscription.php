<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Subscription extends Model
{
    use HasFactory, SoftDeletes;

    protected $guarded = [];
    protected $with = ['subscription_type', 'company_type'];

    public function subscription_type()
    {
        return $this->belongsTo(SubscriptionType::class);
    }

    public function company_type()
    {
        return $this->belongsTo(CompanyType::class,'company_type_id');
    }

//    public function company_scale()
//    {
//        return $this->belongsTo(CompanyScale::class);
//    }
}
