<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CloudinaryTransformation extends Model
{
    use HasFactory;

    const IMAGE = [
        'quality' => 'auto:good',
        'fetch_format' => 'auto',
        'width' => 1000,
        'height' => 1000,
        'crop' => 'limit'
    ];
}
