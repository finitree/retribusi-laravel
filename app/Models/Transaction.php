<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Transaction extends Model
{
    use HasFactory, SoftDeletes;

    const PAID = 'paid';
    const UNPAID = 'unpaid';

    protected $guarded = [];

    public function transaction_details()
    {
        return $this->hasMany(TransactionDetail::class, 'transaction_id');
    }

    public function staff()
    {
        return $this->belongsTo(Staff::class);
    }

    public function detail()
    {
        return $this->hasMany(TransactionDetail::class, 'transaction_id');
    }

//    public function company()
//    {
//        return $this->belongsTo(Company::class);
//    }

    protected function serializeDate(\DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

    public function getCreatedDateAttribute()
    {
        return Carbon::parse($this->created_at)->addHours(8);
    }
}
