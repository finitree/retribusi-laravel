<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SubscriptionType extends Model
{

    const HARIAN = "harian";
    const BULANAN = "bulanan";
    use HasFactory;

    protected $table = "subscription_types";
    protected $guarded = [];
}
