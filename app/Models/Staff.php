<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Staff extends Model
{
    use HasFactory;

    protected $guarded = [];

    protected $with = ['user'];

    public function banjar()
    {
        return $this->belongsTo(Banjar::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function companies()
    {
        return $this->belongsToMany(Company::class, 'staff_companies', 'staff_id', 'company_id');
    }

    public function transactions()
    {
        return $this->hasMany(Transaction::class, 'staff_id');
    }
}
