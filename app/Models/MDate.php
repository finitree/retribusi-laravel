<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MDate extends Model
{
    use HasFactory;

    public static function now(){
        return  Carbon::now()->addHours(8);
    }

    public static function today(){
        return  Carbon::today()->addHours(8);
    }

    public static function parse($date){
      return Carbon::parse($date)->addHours(8);
    }
}
