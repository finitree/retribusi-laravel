<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CompanyOperationalSchedule extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = "companies_operational_schedule";
    protected $guarded = [];
}
