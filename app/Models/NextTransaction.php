<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NextTransaction extends Model
{
    use HasFactory;
    protected $fillable = ['date', 'subcription_amount', 'subscription_qty'];
}
