<?php

namespace App\Exports;

use App\Models\Transaction;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;

class TransactionExport implements FromCollection, WithHeadings, ShouldAutoSize
{

    protected $date, $month, $year;

    public function __construct($date, $month, $year)
    {
        $this->date = $date;
        $this->month = $month;
        $this->year = $year;
    }

    public function headings(): array
    {
        return [
            "No",
            'Petugas',
            'Usaha',
            "Tanggal",
            'Total Pembayaran',
        ];
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {

        $data = Transaction::with("staff", "detail")
            ->when($this->date, function ($q) {
                return $q->whereDate("created_at", Carbon::parse($this->date));
            })
            ->when($this->month, function ($q) {
                return $q->whereYear("created_at", Carbon::parse($this->month))->whereMonth("created_at", Carbon::parse($this->month));
            })
            ->when($this->year, function ($q) {
                return $q->whereYear("created_at", $this->year);
            })
            ->latest()->get()->map(function ($tr, $index) {
                return (object)[
                    "No" => $index + 1,
                    "Petugas" => $tr->staff->user->name,
                    "Usaha" => $tr->detail->unique('company_id')->map(function ($d) {
                        return $d->company->name;
                    })->implode(", "),
                    "Tanggal" => $tr->created_at->format("d M Y"),
                    "Total Pembayaran" => $tr->total_amount,
                ];
            })->values();
        $data->add((object)[
            "No" => "",
            "Petugas" => "",
            "Usaha" => "",
            "Tanggal" => "Total",
            "Total Pembayaran" => $data->sum("Total Pembayaran")
        ]);
        return $data;
    }
}
