<?php

namespace App\Console\Commands;

use App\Models\Company;
use App\Models\CompanyOperationalSchedule;
use App\Models\CompanyUnpaidTransaction;
use App\Models\NextTransaction;
use App\Models\Transaction;
use Carbon\Carbon;
use Illuminate\Console\Command;

class CheckTransactionCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'checktransaction:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        \Log::info("Cron is working");
        $currentMonth = Carbon::now()->format('m');
        $companies = Company::active()->get();
        $openedCompanies = $companies->filter(function ($company) {
            $day = Carbon::now()->addHours(8)->dayOfWeek;
            $currentSchedule = CompanyOperationalSchedule::where('company_id', $company->id)->where('day', $day)->first();
            return ($company->company_close->is_cuti == false) && (!$currentSchedule->is_close);
        })->values();
        $arr = [];
        foreach ($openedCompanies as $company) {
            $subscriptionType = strtolower($company->subscription->subscription_type->category);
            $newObj = new NextTransaction();
            $newObj->company_id = $company->id;
            $newObj->name = $company->name;
            $newObj->status = $company->transactionStatus;
            $newObj->subscription_type = $subscriptionType;
            $newObj->next_transaction = $company->nextTransaction;
//            $newObj->last_transaction = $company->lastTransaction;
            if ($company->transactionStatus == Transaction::UNPAID) {
                if ($subscriptionType == "harian") {
                    CompanyUnpaidTransaction::create([
                        'company_id' => $company->id,
                        'subscription_type_id' => $company->subscription->subscription_type_id,
                        'amount' => $company->subscription_amount,
                        'unpaid_at' => Carbon::parse($company->nextTransaction->date)
                    ]);
                } else {
                    $unpaidDate = CompanyUnpaidTransaction::where('company_id', $company->id)->whereMonth('created_at', $currentMonth)->latest()->first();
                    if (!$unpaidDate) {
                        CompanyUnpaidTransaction::create([
                            'company_id' => $company->id,
                            'subscription_type_id' => $company->subscription->subscription_type_id,
                            'amount' => $company->subscription_amount,
                            'unpaid_at' => Carbon::parse($company->nextTransaction->date)
                        ]);
                    }
                }
                array_push($arr, $newObj);
            }
        }
    }
}
