<?php

namespace App\Http\Livewire\Home;

use App\Models\Balance;
use App\Models\Company;
use App\Models\Staff;
use App\Models\Transaction;
use Asantibanez\LivewireCharts\Facades\LivewireCharts;
use Asantibanez\LivewireCharts\Models\LineChartModel;
use Carbon\Carbon;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use WithPagination;

    protected $paginationTheme = 'bootstrap';

    public $date;
    protected $queryString = ['date'];
    public $colors = [
        1 => '#fc8181',
        2 => '#90cdf4',
        3 => '#66DA26',
        4 => '#cbd5e0',
        5 => '#f6ad55',
    ];


    public function render()
    {
        $totalStaff = Staff::get()->count();
        $totalCompany = Company::active()->get()->count();
        $totalTransactionToday = Transaction::whereDate("created_at", Carbon::now())->get()->count();
        $totalAmountTransactionToday = Transaction::whereDate("created_at", Carbon::now())->get()->sum('total_amount');

        $transactions = Transaction::with('staff', 'detail')->when($this->date, function ($q) {
            return $q->whereDate("created_at", $this->date);
        })->latest()->paginate(10);

        $dailyTransactionChart = Transaction::selectRaw("*, SUM(total_amount) as total")
            ->whereYear("created_at", Carbon::now())
            ->whereMonth("created_at", Carbon::now())
            ->groupBy(\DB::raw("DAY(created_at)"))->get()
            ->reduce(
                function ($chartModel, $data) {
                    $date = $data->created_at->format("d M Y");
                    $value = $data->total;
                    return $chartModel->addColumn($date, $value
                        , $this->colors[random_int(1, 5)]);
                },
                LivewireCharts::columnChartModel()
                    ->setTitle("Transaksi Harian")
                    ->setAnimated(true)
                    ->setLegendVisibility(false)
                    ->setDataLabelsEnabled(true)
            );

        $monthlyTransactionChart = Transaction::selectRaw("*, MONTHNAME(created_at) as month, SUM(total_amount) as total")
            ->whereYear("created_at", Carbon::now())
            ->groupBy(\DB::raw("MONTH(created_at)"))->get()
            ->reduce(
                function ($chartModel, $data) {
                    $date = $data->created_at->format("M Y");
                    $value = $data->total;
                    return $chartModel->addColumn($date, $value
                        , $this->colors[random_int(1, 5)]);
                },
                LivewireCharts::columnChartModel()
                    ->setTitle("Transaksi Bulanan")
                    ->setAnimated(true)
                    ->setLegendVisibility(false)
                    ->setDataLabelsEnabled(true)
            );


        $monthlyBalanceChart = Balance::selectRaw("*, SUM(amount) as total")
            ->whereYear("created_at", Carbon::now())
            ->groupBy(\DB::raw("month(created_at)"))->get()
            ->reduce(
                function (LineChartModel $chartModel, $data) {
                    $date = $data->created_at->format("M");
                    $value = $data->total;
                    return $chartModel->addPoint($date, $value
                        , ['id' => $data->id]);
                },
                LivewireCharts::lineChartModel()
                    ->setTitle("Setoran Bulanan")
                    ->setAnimated(true)
                    ->withOnPointClickEvent('onPointClick')
            );

        return view('livewire.home.index',
            compact("totalStaff",
                "totalCompany",
                "totalTransactionToday",
                "totalAmountTransactionToday",
                "transactions",
                "dailyTransactionChart",
                "monthlyTransactionChart",
                "monthlyBalanceChart"
            ))
            ->layout("layouts.admin-livewire", [
                "title" => "Dashboard",
            ]);
    }

    public function removeFilter()
    {
        $this->date = "";
    }
}
