<?php

namespace App\Http\Livewire\Banjar;

use App\Models\Banjar;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use WithPagination;

    protected $paginationTheme = 'bootstrap';
    public $name, $address;
    public $nameEdit, $addressEdit, $idEdit;

    public $search;
    protected $queryString = ['search'];

    public function updatingSearch()
    {
        $this->resetPage();
    }

    protected $rules = [
        'name' => 'required|min:5',
        'address' => 'required|min:5'
    ];

    protected $validationAttributes = [
        'nameEdit' => 'name',
        'addressEdit' => 'address'
    ];

    protected $listeners = ['confirm' => 'deleteBanjar'];

    public function render()
    {
        $banjars = Banjar::where('name', 'like', '%' . $this->search . '%')->latest()->paginate(10);
        return view('livewire.banjar.index', compact("banjars"))->layout("layouts.admin-livewire", [
            "title" => "Banjar"
        ]);
    }

    public function postBanjar()
    {
        $this->validate();
        Banjar::create([
            'name' => $this->name,
            'address' => $this->address
        ]);

        $this->reset(['name', 'address']);
        $this->dispatchBrowserEvent('formModal:close');
        $this->dispatchBrowserEvent('swal:toast', [
            'title' => "Berhasil ditambahkan!",
            "type" => "success"
        ]);
    }


    public function deleteConfirmationBanjar($id)
    {
        $this->dispatchBrowserEvent('swal:deleteConfirmation', [
            'id' => $id
        ]);
    }

    public function deleteBanjar($id)
    {
        Banjar::find($id)->delete();
        $this->dispatchBrowserEvent('swal:toast', [
            'title' => "Berhasil dihapus!",
            "type" => "success"
        ]);
    }

    public function openEdit(Banjar $banjar)
    {
        $this->nameEdit = $banjar->name;
        $this->idEdit = $banjar->id;
        $this->addressEdit = $banjar->address;
        $this->emit("editModal:open");
//        $this->dispatchBrowserEvent('editModal:open', [
////            "banjar" => $banjar
//        ]);
    }

    public function editBanjar()
    {
        $validatedData = $this->validate([
            'nameEdit' => 'required|min:5',
            'addressEdit' => 'required|min:5',
        ]);

        $banjar = Banjar::find($this->idEdit);
        $banjar->name = $this->nameEdit;
        $banjar->address = $this->addressEdit;
        $banjar->save();
        $this->reset(['nameEdit', 'idEdit', 'addressEdit']);
        $this->dispatchBrowserEvent('editModal:close');

        $this->dispatchBrowserEvent('swal:toast', [
            'title' => "Berhasil diubah!",
            "type" => "success"
        ]);

    }

}
