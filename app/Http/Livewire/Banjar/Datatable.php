<?php

namespace App\Http\Livewire\Banjar;

use App\Models\Banjar;
use Mediconesystems\LivewireDatatables\Column;
use Mediconesystems\LivewireDatatables\Http\Livewire\LivewireDatatable;

class Datatable extends LivewireDatatable
{
    public $model = Banjar::class;

    public function builder()
    {
        Banjar::query();
    }

    public function columns()
    {
        return [
            Column::name('name')
                ->defaultSort('asc')
                ->searchable()
                ->filterable(),

            Column::name('address')
                ->defaultSort('asc')
                ->searchable()
                ->filterable(),
        ];
    }
}
