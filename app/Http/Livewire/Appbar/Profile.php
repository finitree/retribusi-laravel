<?php

namespace App\Http\Livewire\Appbar;

use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class Profile extends Component
{
    protected $listeners = ['confirmLogout' => 'logout'];

    public function render()
    {
        return view('livewire.appbar.profile');
    }

    public function logoutConfirmation()
    {
        $this->dispatchBrowserEvent("swal:logoutConfirmation");
    }

    public function logout()
    {
        Auth::logout();
        $this->redirectRoute("login");
    }
}
