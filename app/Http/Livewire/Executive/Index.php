<?php

namespace App\Http\Livewire\Executive;

use App\Models\User;
use Illuminate\Validation\Rule;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use WithPagination;

    protected $paginationTheme = 'bootstrap';
    public $search;
    protected $queryString = ['search'];

    public $name, $email, $phone, $password;
    public $idEdit, $nameEdit, $emailEdit, $phoneEdit, $passwordEdit;

    protected $listeners = ['confirm' => 'delete'];


    protected $validationAttributes = [
        'name' => 'Nama',
        'email' => 'Email',
        "phone" => "Telepon",
        "password" => "Kata Sandi",
        'nameEdit' => 'Nama',
        'emailEdit' => 'Email',
        "phoneEdit" => "Telepon",
        "passwordEdit" => "Kata Sandi"
    ];

    public function updatingSearch()
    {
        $this->resetPage();
    }


    public function render()
    {
        $executives = User::where("role", "executive")->where('name', 'like', '%' . $this->search . '%')->latest()->paginate(10);
        return view('livewire.executive.index',
            compact(
                "executives"
            )
        )->layout("layouts.admin-livewire", [
            "title" => "Atasan"
        ]);
    }

    public function post()
    {
        $this->validate([
            "name" => "required",
            "email" => "required|email|unique:users",
            "phone" => "required|unique:users",
            "password" => "required|min:6"
        ]);

        User::create([
            "name" => $this->name,
            "email" => $this->email,
            "phone" => $this->phone,
            "password" => bcrypt($this->password),
            "role" => "executive"
        ]);

        $this->reset([
            'name', 'email', 'phone', 'password',
        ]);
        $this->dispatchBrowserEvent('formModal:close');
        $this->dispatchBrowserEvent('swal:toast', [
            'title' => "Berhasil ditambahkan!",
            "type" => "success"
        ]);
    }

    public function openEdit(User $user)
    {
        $this->idEdit = $user->id;
        $this->nameEdit = $user->name;
        $this->phoneEdit = $user->phone;
        $this->emailEdit = $user->email;
        $this->emit("editModal:open");
    }

    public function edit()
    {
        $this->validate([
            "nameEdit" => "required",
            "emailEdit" => [
                "required",
                "email",
                Rule::unique("users", "email")->ignore($this->idEdit)
            ],
            "phoneEdit" => [
                "required",
                Rule::unique("users", "phone")->ignore($this->idEdit)
            ],
            "passwordEdit" => "nullable|min:6"
        ]);
        $user = User::find($this->idEdit);
        $user->name = $this->nameEdit;
        $user->phone = $this->phoneEdit;
        $user->email = $this->emailEdit;
        if ($this->passwordEdit) {
            $user->password = bcrypt($this->passwordEdit);
        }
        $user->save();

        $this->reset([
            'idEdit', 'nameEdit', 'passwordEdit', 'emailEdit', 'phoneEdit'
        ]);
        $this->dispatchBrowserEvent('editModal:close');
        $this->dispatchBrowserEvent('swal:toast', [
            'title' => "Berhasil diubah!",
            "type" => "success"
        ]);


    }


    public function deleteConfirmation($id)
    {
        $this->dispatchBrowserEvent('swal:deleteConfirmation', [
            'id' => $id
        ]);
    }

    public function delete(User $user)
    {
        $user->delete();
        $this->dispatchBrowserEvent('swal:toast', [
            'title' => "Berhasil dihapus!",
            "type" => "success"
        ]);
    }


}
