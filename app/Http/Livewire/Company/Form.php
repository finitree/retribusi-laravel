<?php

namespace App\Http\Livewire\Company;

use App\Models\Banjar;
use App\Models\BaseModel;
use App\Models\CloudinaryTransformation;
use App\Models\Company;
use App\Models\CompanyCloseSchedule;
use App\Models\CompanyOperationalSchedule;
use App\Models\CompanyType;
use App\Models\CompanyUnpaidTransaction;
use App\Models\Subscription;
use App\Models\SubscriptionType;
use App\Models\User;
use Barryvdh\DomPDF\Facade\Pdf;
use Carbon\Carbon;
use Livewire\Component;
use Livewire\WithFileUploads;

class Form extends Component
{
    use WithFileUploads;

    public $company;
    public $idCompany;

    // public $closing_hours;
    // public $opening_hours;
    // public $is_open;
    public $type_company_required_docs;
    public $schedules = [];
    public $old_photo;
    public $old_docs;
    public $documents;
    public $photos;

    public $agreement_document;
    public $old_agreement_document;

    public $type_subscription;

    public $date_start;
    public $date_end;
    public $idCloseSchedule;

    protected $listeners = ['confirm' => 'deleteCloseSchedule', 'decline' => 'declineCompany'];
    protected $messages = [
        'company.company_type_id.required_if' => 'Documents required for this Company Type!'
    ];

    public function rules()
    {
        return [
            'company.name' => 'required',
            'company.company_type_id' => 'required|required_if:company.company_type_id,' . $this->type_company_required_docs,
            'documents' => 'nullable',
            'agreement_document' => 'nullable',
            'photos' => 'nullable',
            'company.banjar_id' => 'required',
            'company.latitude' => 'required',
            'company.longitude' => 'required',
            'company.address' => 'required',
            'company.user_id' => 'required',
            'company.subscription_amount' => 'required',
            'type_subscription' => 'required',
            'schedules' => 'required|array|min:7',
            'schedules.*' => 'required',
            'schedules.*.opening_hours' => 'required',
            'schedules.*.closing_hours' => 'required',
            'schedules.*.is_open' => 'required',
            'date_start' => $this->idCompany ? 'required|date' : 'nullable',
            'date_end' => $this->idCompany ? 'required|date|after_or_equal:date_start' : 'nullable',
        ];
    }

    public function mount($id = null)
    {
        $companyType = CompanyType::where('is_document_required', 1)->pluck('id')->toArray();
        $arrToString = implode(",", $companyType);
        $this->type_company_required_docs = $arrToString;
        if ($id) {
            // this will run on edit
            $this->idCompany = $id;
            $this->company = Company::find($id);
            $this->schedules = CompanyOperationalSchedule::where('company_id', $id)->get();
            $this->old_photo = $this->company->photos;
            $this->old_agreement_document = $this->company->agreement_document;
            $this->photos = $this->company->photos;
            $this->old_docs = $this->company->documents;
            $this->documents = $this->company->documents;
//            $this->agreement_document = $this->company->agreement_document;
            $this->type_subscription = $this->company->subscription->subscription_type->id;

        } else {
            // this will run on create
            $arr = [];
            for ($i = 0; $i <= 6; $i++) {
                // initial schedule
                $data = new BaseModel();
                $data->is_open = true;
                $data->opening_hours = "08:00";
                $data->closing_hours = "23:59";
                array_push($arr, $data);
            }
            $this->schedules = $arr;
        }
    }

    public function declineConfirmationCompany($id)
    {
        $this->dispatchBrowserEvent('swal:declineConfirmation', [
            'id' => $id
        ]);
    }

    public function declineCompany($id)
    {
        Company::find($id)->update([
            'status' => Company::BLOCKED
        ]);
        session()->flash("success", "Berhasil ditolak!");
        $this->redirectRoute("company.index");
    }

    public function submit()
    {
        //  dd($this->company);
        $this->validate([
            'company.name' => 'required',
            'company.company_type_id' => 'required',
            'documents' => 'nullable|required_if:company.company_type_id,' . $this->type_company_required_docs,
            'agreement_document' => 'nullable',
            'photos' => 'nullable',
            'company.banjar_id' => 'required',
            'company.latitude' => 'required',
            'company.longitude' => 'required',
            'company.address' => 'required',
            'company.user_id' => 'required',
            'company.subscription_amount' => 'required',
            'type_subscription' => 'required',
            'schedules' => 'required|array|min:7',
            'schedules.*' => 'required',
            'schedules.*.opening_hours' => 'required',
            'schedules.*.closing_hours' => 'required',
            'schedules.*.is_open' => 'required',
        ]);
//        dd($this->agreement_document);

        if (!$this->idCompany) {
            $this->validate([
                'agreement_document' => 'required'
            ]);
        }


        $flashMessage = "";

        $imgName = null;
        $docUrl = null;
        // check photo and current photo company, if different, user input new image, upload to cloudinary
        // same as documents

        if ($this->photos != $this->old_photo) {
            $imgName = cloudinary()->upload($this->photos->getRealPath(), [
                'transformation' => CloudinaryTransformation::IMAGE,
                'public_id' => "company/" . uniqid()
            ])->getSecurePath();
        }
        if ($this->documents != $this->old_docs) {
            $docUrl = cloudinary()->upload($this->documents->getRealPath())->getSecurePath();
        }

        // get subscription id from model subscription base on input user
        $subscriptionId = Subscription::where('subscription_type_id', $this->type_subscription)
            ->where('company_type_id', $this->company['company_type_id'])->first();
        if ($this->idCompany) {
            // update company
            $company = Company::find($this->idCompany);
            if (!$company->agreement_document) {
                $this->validate([
                    'agreement_document' => 'required'
                ]);
            }

            $old_status = $company->status;
            $flashMessage = $company->status == Company::VERIFIED ? "Berhasil diubah!" : "Berhasil diverifikasi!";

            //            if verify first time
            if ($old_status != Company::VERIFIED && !$company->verified_at) {
                $company->update([
                    'verified_at' => Carbon::now()
                ]);
            }

            $company->update([
                'user_id' => $this->company->user_id,
                'subscription_id' => $subscriptionId->id,
                'banjar_id' => $this->company->banjar_id,
                'company_type_id' => $this->company->company_type_id,
                'name' => $this->company->name,
                'latitude' => $this->company->latitude,
                'longitude' => $this->company->longitude,
                'address' => $this->company->address,
                'subscription_amount' => $this->company->subscription_amount,
                'status' => Company::VERIFIED
            ]);

//            if verify first time
            if ($old_status != Company::VERIFIED && !$company->verified_at) {
                $company->update([
                    'verified_at' => Carbon::now()
                ]);

                CompanyUnpaidTransaction::create([
                    'company_id' => $company->id,
                    'subscription_type_id' => $company->subscription->subscription_type_id,
                    'amount' => $company->subscription_amount,
                    'unpaid_at' => Carbon::now()
                ]);
            }

            if ($imgName !== null) {
                $company->update([
                    'photos' => $imgName,
                ]);
            }
            if ($docUrl !== null) {
                $company->update([
                    'documents' => $docUrl,
                ]);
            }

            if ($this->agreement_document) {
                $url = cloudinary()->upload($this->agreement_document->getRealPath())->getSecurePath();
                $company->update([
                    'agreement_document' => $url,
                ]);
            }

            foreach ($this->schedules as $schedule) {
                $operational = CompanyOperationalSchedule::find($schedule->id);
                $operational->update([
                    'company_id' => $this->idCompany,
                    'day' => $schedule->day,
                    'opening_hours' => $schedule->opening_hours,
                    'closing_hours' => $schedule->closing_hours,
                    'is_open' => $schedule->is_open
                ]);
            }

        } else {
            $flashMessage = "Berhasil ditambahkan!";
            $agreementUrl = cloudinary()->upload($this->agreement_document->getRealPath())->getSecurePath();

            // create company
            $company = new Company();
            $company->makeHidden(['transaction_status', 'company_close']);
            $company->user_id = $this->company['user_id'];
            $company->subscription_id = $subscriptionId->id;
            $company->banjar_id = $this->company['banjar_id'];
            $company->company_type_id = $this->company['company_type_id'];
            $company->name = $this->company['name'];
            $company->latitude = $this->company['latitude'];
            $company->longitude = $this->company['longitude'];
            $company->address = $this->company['address'];
            $company->photos = $imgName;
            $company->documents = $docUrl;
            $company->agreement_document = $agreementUrl;
            $company->subscription_amount = (int)$this->company['subscription_amount'];
            $company->status = auth()->user()->isAdmin ? Company::VERIFIED : Company::WAIT_VERIFIED;
            $company->verified_at = Carbon::now();
            $company->save();

            if ($company) {
                for ($i = 0; $i <= 6; $i++) {
                    CompanyOperationalSchedule::create([
                        'company_id' => $company->id,
                        'day' => $i + 1,
                        'opening_hours' => $this->schedules[$i]['closing_hours'],
                        'closing_hours' => $this->schedules[$i]['opening_hours'],
                        'is_open' => $this->schedules[$i]['is_open'],
                    ]);
                }

                CompanyUnpaidTransaction::create([
                    'company_id' => $company->id,
                    'subscription_type_id' => $company->subscription->subscription_type_id,
                    'amount' => $company->subscription_amount,
                    'unpaid_at' => Carbon::now()
                ]);

            }
        }

        // $this->reset();
        session()->flash("success", $flashMessage);
        $this->redirectRoute("company.index");

    }

    public function submitCloseSchedule()
    {
        $this->validate([
            'date_start' => $this->idCompany ? 'required|date' : 'nullable',
            'date_end' => $this->idCompany ? 'required|date|after_or_equal:date_start' : 'nullable',
        ]);
        CompanyCloseSchedule::create([
            'company_id' => $this->idCompany,
            'date_start' => $this->date_start,
            'date_end' => $this->date_end,
        ]);

        $this->date_start = null;
        $this->date_end = null;

        $this->dispatchBrowserEvent('swal:toast', [
            'title' => "Berhasil ditambahkan!",
            "type" => "success"
        ]);

    }

    public function updateCloseSchedule()
    {
        $this->validate([
            'date_start' => $this->idCompany ? 'required|date' : 'nullable',
            'date_end' => $this->idCompany ? 'required|date|after_or_equal:date_start' : 'nullable',
        ]);
        $closeSchedule = CompanyCloseSchedule::find($this->idCloseSchedule);
        $closeSchedule->update([
            'date_start' => $this->date_start,
            'date_end' => $this->date_end,
        ]);

        $this->date_start = null;
        $this->date_end = null;

        $this->dispatchBrowserEvent('swal:toast', [
            'title' => "Berhasil diubah!",
            "type" => "success"
        ]);

    }

    public function openModal($id)
    {
        $this->idCloseSchedule = $id;
        $companyCloseSchedule = CompanyCloseSchedule::find($id);
        $this->date_start = $companyCloseSchedule->date_start;
        $this->date_end = $companyCloseSchedule->date_end;
        $this->emit("modalCloseSchedule:open");
    }

    public function deleteConfirmationSchedule($id)
    {
        $this->dispatchBrowserEvent('swal:deleteConfirmation', [
            'id' => $id
        ]);
    }

    public function deleteCloseSchedule($id)
    {
        CompanyCloseSchedule::find($id)->delete();
        $this->dispatchBrowserEvent('swal:toast', [
            'title' => "Berhasil dihapus!",
            "type" => "success"
        ]);
    }

    public function printCompany()
    {
        $company = Company::with('subscription', 'user')->find($this->idCompany);
//        return $pdf->stream();
        return response()->streamDownload(function () use ($company) {
            $pdf = PDF::loadView('pdf.company_detail', ['company' => $company]);
            echo $pdf->stream();
        }, 'test.pdf');
    }

    public function render()
    {
        $company_types = CompanyType::all();
        $banjars = Banjar::all();
        $listDays = [
            'senin', 'selasa', 'rabu', 'kamis', 'jumat', 'sabtu', 'minggu'
        ];
        $closed = CompanyCloseSchedule::where('company_id', $this->idCompany)->get();
        $owners = User::where('role', User::OWNER)->get();
        $subscription_types = SubscriptionType::all();
        $company = $this->company;
        return view('livewire.company.form',
            compact('company_types', 'banjars', 'listDays', 'closed', 'owners', 'subscription_types', 'company')
        )->layout("layouts.admin-livewire", [
            "title" => $this->idCompany ? "Edit usaha" : "Tambah Usaha"
        ]);
    }
}
