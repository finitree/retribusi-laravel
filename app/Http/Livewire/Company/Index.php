<?php

namespace App\Http\Livewire\Company;

use App\Models\Banjar;
use App\Models\Company;
use App\Models\CompanyUnpaidTransaction;
use Carbon\Carbon;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use WithPagination;

    protected $paginationTheme = 'bootstrap';
    public $search;
    public $transaction;
    public $status;
    public $perPage = 10;
    protected $queryString = ['status', 'search', 'transaction'];

    public $id_verified, $banjar_id_verified;
    protected $listeners = ['confirm' => 'deleteCompany', 'decline' => 'declineCompany'];

    public function render()
    {
        $companies = Company::with('user')
            ->when($this->status, function ($q) {
                return $q->where('status', $this->status);
            })
            ->where('name', 'like', '%' . $this->search . '%')->latest()->paginate($this->perPage);

        if ($this->transaction) {
            $companies = $companies->values()->filter(function ($value) {
                return $value->transaction_status == $this->transaction;
            })->paginate(10);
        }

        $banjars = Banjar::latest()->get();

        return view('livewire.company.index', compact('companies', "banjars"))->layout("layouts.admin-livewire", [
            "title" => "Daftar Usaha"
        ]);
    }

    public function deleteConfirmationCompany($id)
    {
        $this->dispatchBrowserEvent('swal:deleteConfirmation', [
            'id' => $id
        ]);
    }

    public function declineConfirmationCompany($id)
    {
        $this->dispatchBrowserEvent('swal:declineConfirmation', [
            'id' => $id
        ]);
    }

    public function deleteCompany($id)
    {
        Company::find($id)->delete();
        $this->dispatchBrowserEvent('swal:toast', [
            'title' => "Berhasil dihapus!",
            "type" => "success"
        ]);
    }

    public function declineCompany($id)
    {
        Company::find($id)->update([
            'status' => Company::BLOCKED
        ]);
        $this->dispatchBrowserEvent('swal:toast', [
            'title' => "Berhasil ditolak!",
            "type" => "success"
        ]);
    }

    public function verifyConfirmation($id)
    {
        $this->id_verified = $id;
        $this->emit("verifyModal:open");
    }

    public function verify()
    {
        $this->validate([
            "banjar_id_verified" => "required"
        ]);
        $company = Company::find($this->id_verified);
        $company->update([
            'status' => Company::VERIFIED,
            'banjar_id' => $this->banjar_id_verified
        ]);
        CompanyUnpaidTransaction::create([
            'company_id' => $company->id,
            'subscription_type_id' => $company->subscription->subscription_type_id,
            'amount' => $company->subscription->subscription_amount,
            'unpaid_at' => Carbon::now()
        ]);
        $this->reset();
        $this->emit("verifyModal:close");
        $this->dispatchBrowserEvent('swal:toast', [
            'title' => "Berhasil terverifikasi!",
            "type" => "success"
        ]);

    }
}
