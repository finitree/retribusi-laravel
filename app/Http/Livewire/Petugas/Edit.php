<?php

namespace App\Http\Livewire\Petugas;

use App\Models\Banjar;
use App\Models\CloudinaryTransformation;
use App\Models\Staff;
use App\Models\User;
use Illuminate\Validation\Rule;
use Livewire\Component;
use Livewire\WithFileUploads;

class Edit extends Component
{
    use WithFileUploads;

    public $id_user, $id_staff, $name, $email, $password, $old_photo, $old_ktp, $photo, $no_hp, $ktp, $banjar, $balance;

    public function rules()
    {
        return [
            'name' => 'required|min:6',
            'email' => [
                'required',
                'email',
                Rule::unique("users", "email")->ignore($this->id_user)
            ],
            'ktp' => 'nullable|image|mimes:jpeg,png,jpg|max:1024',
            'no_hp' => [
                'required',
                Rule::unique("users", "phone")->ignore($this->id_user)
            ],
            'banjar' => 'required',
            'photo' => 'nullable|image|mimes:jpeg,png,jpg|max:1024',
            'password' => 'nullable|min:6',
            'balance' => 'required|numeric|min:0'
        ];
    }

    public function mount($id)
    {
        $this->id_staff = $id;
        $staff = Staff::with('user')->find($id);

        $this->id_user = $staff->user->id;
        $this->name = $staff->user->name;
        $this->email = $staff->user->email;
        $this->old_ktp = $staff->user->ktp;
        $this->no_hp = $staff->user->phone;
        $this->banjar = $staff->banjar_id;
        $this->old_photo = $staff->user->photo;
        $this->balance = $staff->balance;
//        $this->old_balance = $staff->balance;
//        $this->balance = $staff->balance;
    }

    public function submit()
    {
        // validate all input
        $this->validate();


        // find staff
        $staff = Staff::find($this->id_staff);
        $staff->update([
            'banjar_id' => $this->banjar,
            'balance' => $this->balance
        ]);
        // check with old balance
        // if different with balance, thats mean balance changed, then update it
//        if ($this->old_balance !== $this->balance) {
//            $staff->update([
//                'balance' => $this->balance
//            ]);
//        }
        // update staff
        $staff->user()->update([
            'name' => $this->name,
            'email' => $this->email,
            'phone' => $this->no_hp,
        ]);

        if ($this->ktp) {
            $url = cloudinary()->upload($this->ktp->getRealPath(), [
                'transformation' => CloudinaryTransformation::IMAGE,
                'public_id' => "ktp_petugas/" . uniqid()
            ])->getSecurePath();
            $staff->user()->update([
                'ktp' => $url
            ]);
        }

        // update photo if changed
        $imgName = null;
        if ($this->photo) {
            $imgName = cloudinary()->upload($this->photo->getRealPath(), [
                'transformation' => User::CLOUDINARY_TRANSFORMATION_PROFILE,
                'public_id' => "petugas/" . uniqid()
            ])->getSecurePath();
            $staff->user()->update([
                'photo' => $imgName
            ]);
        }
        // if input password filled, thats mean password updated
        if ($this->password) {
            $hash = bcrypt($this->password);
            $staff->user()->update([
                'password' => $hash
            ]);
        };

        session()->flash("success", "Berhasil diubah!");

        $this->redirectRoute("staff.index");

    }

    public function render()
    {
        $listBanjar = Banjar::select('id', 'name')->get();
        return view('livewire.petugas.edit', compact('listBanjar'))->layout("layouts.admin-livewire", [
            "title" => "Ubah Petugas"
        ]);
    }
}
