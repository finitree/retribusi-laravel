<?php

namespace App\Http\Livewire\Petugas;

use App\Models\Company;
use App\Models\Staff;
use App\Models\StaffCompany;
use Illuminate\Validation\Validator;
use Livewire\Component;
use Livewire\WithPagination;

class Mapping extends Component
{
    public $search;
    public $search_company;
    public $selected_companies = [];
    public $selected_staff;
    public $available_companies = [];

    public $idStaff, $idCompany;
    protected $queryString = ['search_company', 'search'];
    protected $rules = [
        'selected_companies' => 'nullable',
    ];
    use WithPagination;

    protected $paginationTheme = 'bootstrap';

    public function render()
    {
        $staffs = Staff::with('companies')
            ->whereHas('user', function ($q) {
                $q->where('name', 'like', '%' . $this->search . '%')->orWhere('email', 'like', '%' . $this->search . '%');
            })
            ->latest()
            ->paginate(10);

        $company_options = Company::with('staffs')->select('id', 'name')->latest()->get();
        $staff_options = Staff::with('user')->latest()->get();
//        $a = StaffCompany::get()->pluck('company_id')->values()->toArray();
//        dd($a);
//        $this->selected_companies = StaffCompany::get()->pluck('company_id')->values()->toArray();

        $companies = Company::with("staffs", "user", "banjar")
            ->where('name', 'like', '%' . $this->search_company . '%')
            ->latest()
            ->paginate(10, ['*'], 'companyPage');

        return view('livewire.petugas.mapping',
            compact(
                "staffs",
                "company_options",
                "staff_options",
                "companies"
            )
        )->layout("layouts.admin-livewire", [
            "title" => "Mapping Petugas"
        ]);
    }

    public function openMappingModal($staffId)
    {
        $this->resetValidation();
        $this->idStaff = $staffId;
        $this->selected_companies = StaffCompany::where('staff_id', $staffId)->get()->pluck('company_id')->values()->toArray();
        $this->emit('mappingModal:open');
    }

    public function submit()
    {
        $this->validate([
            'selected_companies' => 'nullable'
        ]);

        if ($this->selected_companies) {
            $this->withValidator(function (Validator $validator) {
                $validator->after(function ($validator) {
                    $companies = Company::with('staffs')->whereHas('staffs')->whereIn("id", $this->selected_companies)->get();
                    $validities = [];
                    $not_valid = [];
                    foreach ($companies as $company) {
                        $is_valid = $company->staffs[0]->id == $this->idStaff;
                        if (!$is_valid) {
                            $not_valid[] = $company->name;
                        }
                        $validities[] = $is_valid;
                    }
                    if (in_array(false, $validities)) {
                        $not_valid_company = join(", ", $not_valid);
                        $validator->errors()->add('selected_companies', $not_valid_company . ' sudah termapping oleh petugas lain');
                    }
                });
            })->validate();
        }

        StaffCompany::where('staff_id', $this->idStaff)->delete();
        if ($this->selected_companies) {
            foreach ($this->selected_companies as $company) {
                StaffCompany::create([
                    'staff_id' => $this->idStaff,
                    'company_id' => $company
                ]);
            }
        }

        $this->resetPage();
        $this->selected_companies = [];
        $this->emit("mappingModal:close");
        $this->dispatchBrowserEvent('swal:toast', [
            'title' => "Berhasil diubah!",
            "type" => "success"
        ]);

    }

    public function openMappingCompanyModal($companyId)
    {
        $this->idCompany = $companyId;
        $this->selected_staff = StaffCompany::where('company_id', $companyId)->first()->staff_id ?? null;
        $this->emit("mappingCompanyModal:open");
    }

    public function submitCompanyMapping()
    {
        $this->validate([
            'selected_staff' => 'required'
        ]);
        StaffCompany::where('company_id', $this->idCompany)->delete();
        StaffCompany::create([
            'staff_id' => $this->selected_staff,
            'company_id' => $this->idCompany
        ]);
        $this->resetPage();
        $this->emit("mappingCompanyModal:close");
        $this->dispatchBrowserEvent('swal:toast', [
            'title' => "Berhasil diubah!",
            "type" => "success"
        ]);
    }

}
