<?php

namespace App\Http\Livewire\Petugas;

use App\Models\Staff;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use WithPagination;

    protected $paginationTheme = 'bootstrap';
    public $search = '';
    protected $listeners = ['confirm' => 'deletePetugas'];

    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function render()
    {
        $petugas = Staff::with('user')
            ->whereHas('user', function ($q) {
                $q->where('name', 'like', '%' . $this->search . '%')->orWhere('email', 'like', '%' . $this->search . '%');
            })
            ->latest()->paginate(10);
        return view('livewire.petugas.index', compact('petugas'))->layout("layouts.admin-livewire", [
            "title" => "Daftar Petugas"
        ]);
    }

    public function deleteConfirmation($id)
    {
        $this->dispatchBrowserEvent('swal:deleteConfirmation', [
            'id' => $id
        ]);
    }

    public function deletePetugas($id)
    {
        Staff::find($id)->delete();
        $this->dispatchBrowserEvent('swal:toast', [
            'title' => "Berhasil dihapus!",
            "type" => "success"
        ]);
    }
}
