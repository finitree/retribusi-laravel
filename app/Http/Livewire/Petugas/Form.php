<?php

namespace App\Http\Livewire\Petugas;

use App\Models\User;
use Livewire\Component;

class Form extends Component
{
    public User $user;
    public $data;
    protected $rules = [
        'user.name' => 'required|string|min:6',
        'user.email' => 'required|email|max:500',
        'user.phone' => 'required|regex:/(01)[0-9]{9}/',
        'user.password' => 'required|min:8'
    ];
    public function mount($id = null)
    {
        if ($id != null) {
            $dataUser = User::find($id);
            $this->data = $dataUser;
        }
    }
    public function render()
    {
        $dummy = $this->data;
        return view('livewire.petugas.form', compact('dummy'))->layout("layouts.admin-livewire", [
            "title" => "Edit Petugas"
        ]);;
    }
}
