<?php

namespace App\Http\Livewire\Petugas;

use App\Models\Banjar;
use App\Models\CloudinaryTransformation;
use App\Models\Staff;
use App\Models\User;
use Livewire\Component;
use Livewire\WithFileUploads;

class Create extends Component
{
    use WithFileUploads;

    public $name, $email, $password, $photo, $no_hp, $ktp, $banjar, $iteration;
//    protected $messages = [
//        'name.required' => 'Nama petugas wajib diisi!',
//        'name.min' => 'Name petugas minimal 4 karakter!',
//        'email.required' => 'Email petugas wajib diisi!',
//        'email.email' => 'Email tidak valid!',
//        'email.unique' => 'Email ini sudah digunakan!',
//        'password.required' => 'Password wajib diisi!',
//        'password.min' => 'Password minimal 8 karakter!',
//        'ktp.required' => 'KTP wajib diisi!',
//        'ktp.digits' => 'KTP harus terdiri dari 16 angka',
//        'no_hp.required' => 'No Hp wajib diisi!',
//        'no_hp.regex' => 'No Hp tidak valid',
//        'no_hp.min' => 'No Hp minimal 11 digit',
//        'no_hp.numeric' => 'No Hp hanya boleh angka',
//        'banjar.required' => 'Banjar wajib diisi!',
//        'photo.image' => 'Hanya boleh mengunggah foto',
//        'photo.max' => 'Photo tidak boleh lebih dari 1024 kb'
//    ];

    public function rules()
    {
        return [
            'name' => 'required|min:4',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|min:8',
            'ktp' => 'required|image|mimes:jpeg,png,jpg|max:1024',
            'no_hp' => 'required|numeric|min:11|regex:/(08)[0-9]/',
            'banjar' => 'required',
            'photo' => 'nullable|image|mimes:jpeg,png,jpg|max:1024',
        ];
    }

    public function submit()
    {
        $this->validate();

        // $imgName = 'petugas_'. time() . '.' . $this->photo->extension();
        // $this->photo->storeAs("public/petugas", $imgName);
        $imgName = null;
        if ($this->photo) {
            $imgName = cloudinary()->upload($this->photo->getRealPath(), [
                'transformation' => User::CLOUDINARY_TRANSFORMATION_PROFILE,
                'public_id' => "petugas/" . uniqid()
            ])->getSecurePath();
        }

        $ktpUrl = cloudinary()->upload($this->ktp->getRealPath(), [
            'transformation' => CloudinaryTransformation::IMAGE,
            'public_id' => "ktp_petugas/" . uniqid()
        ])->getSecurePath();

        $hash = bcrypt($this->password);
        $user = User::create([
            'name' => $this->name,
            'email' => $this->email,
            'password' => $hash,
            'phone' => $this->no_hp,
            'role' => 'staff',
            'photo' => $imgName ?? null,
            'ktp' => $ktpUrl,
        ]);
        if ($user) {
            Staff::create([
                'user_id' => $user->id,
                'banjar_id' => $this->banjar,
            ]);
        }
        $this->reset();
        session()->flash("success", "Berhasil ditambahkan!");
        $this->redirectRoute("staff.index");
    }

    public function render()
    {
        $listBanjar = Banjar::select('id', 'name')->get();
        return view('livewire.petugas.create', compact('listBanjar'))->layout("layouts.admin-livewire", [
            "title" => "Tambah Petugas"
        ]);;
    }
}
