<?php

namespace App\Http\Livewire\Transaction;

use App\Exports\TransactionExport;
use App\Models\Transaction;
use Barryvdh\DomPDF\Facade\Pdf;
use Carbon\Carbon;
use Livewire\Component;
use Livewire\WithPagination;
use Maatwebsite\Excel\Facades\Excel;

class Index extends Component
{
    use WithPagination;

    protected $paginationTheme = 'bootstrap';
    protected $listeners = ['confirm' => 'delete'];

    public $date;
    public $month;
    public $year;
    protected $queryString = ['date', 'month', 'year'];

    public $transactionShow;


    public function render()
    {
        $transactions = Transaction::with('staff', 'detail')
            ->when($this->date, function ($q) {
                return $q->whereDate("created_at", Carbon::parse($this->date));
            })
            ->when($this->month, function ($q) {
                return $q->whereYear("created_at", Carbon::parse($this->month))->whereMonth("created_at", Carbon::parse($this->month));
            })
            ->when($this->year, function ($q) {
                return $q->whereYear("created_at", $this->year);
            })
            ->latest()->paginate(10);
        $years = Transaction::selectRaw('YEAR(created_at) as year')->groupBy(\DB::raw("YEAR(created_at)"))->get();
        $transactionToday = Transaction::whereDate("created_at", Carbon::now())->get();
        $totalTransactionToday = $transactionToday->count();
        $totalAmountTransactionToday = $transactionToday->sum('total_amount');

        $transactionMonth = Transaction::whereYear("created_at", Carbon::now()->year)->whereMonth("created_at", Carbon::now())->get();
        $totalTransactionMonth = $transactionMonth->count();
        $totalAmountTransactionMonth = $transactionMonth->sum('total_amount');

        $transactionYear = Transaction::whereYear("created_at", Carbon::now()->year)->get();
        $totalTransactionYear = $transactionYear->count();
        $totalAmountTransactionYear = $transactionYear->sum('total_amount');


        return view('livewire.transaction.index',
            compact(
                "transactions",
                "years",
                "totalTransactionToday", "totalAmountTransactionToday",
                "totalAmountTransactionMonth", "totalTransactionMonth",
                "totalTransactionYear", "totalAmountTransactionYear"
            )
        )->layout("layouts.admin-livewire", [
            "title" => "Dudukan usaha"
        ]);
    }

    public function mount()
    {
        $this->year = Carbon::now()->year;
    }

    public function showDetail(Transaction $transaction)
    {
        $this->transactionShow = $transaction;
        $this->emit("showModal:open");
    }

    public function removeFilter()
    {
        $this->date = "";
    }

    public function removeMonthFilter()
    {
        $this->month = "";
    }

    public function removeYearFilter()
    {
        $this->year = "";
    }

    public function exportExcel()
    {
        $name = "dudukan";
        if ($this->date) {
            $name .= "_" . $this->date;
        }
        if ($this->month) {
            $name .= "_" . $this->month;
        }
        if ($this->year) {
            $name .= "_" . $this->year;
        }
        $name .= ".xlsx";
        return Excel::download(new TransactionExport($this->date, $this->month, $this->year), $name);
    }

    public function exportPDF()
    {
        $transactions = Transaction::with('staff', 'detail')->when($this->date, function ($q) {
            return $q->whereDate("created_at", Carbon::parse($this->date));
        })
            ->when($this->month, function ($q) {
                return $q->whereYear("created_at", Carbon::parse($this->month))->whereMonth("created_at", Carbon::parse($this->month));
            })
            ->when($this->year, function ($q) {
                return $q->whereYear("created_at", $this->year);
            })
            ->latest()->get();

//        $transactions = Transaction::where('company_id', 28)->get();


        $name = "dudukan";
        if ($this->date) {
            $name .= "_" . $this->date;
        }
        if ($this->month) {
            $name .= "_" . $this->month;
        }
        if ($this->year) {
            $name .= "_" . $this->year;
        }
        $name .= ".pdf";

        $pdf = PDF::loadView('pdf.transaction_pdf', ['transactions' => $transactions])->output();
        return response()->streamDownload(
            function () use ($pdf) {
                return print($pdf);
            }, $name
        );
    }
}
