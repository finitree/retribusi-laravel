<?php

namespace App\Http\Livewire\Profile;

use App\Models\CloudinaryTransformation;
use App\Models\User;
use Illuminate\Validation\Rule;
use Livewire\Component;
use Livewire\WithFileUploads;

class Index extends Component
{
    use WithFileUploads;
    public $user_id, $name, $phone, $email, $password, $photo;
    public $old_photo;

    public function render()
    {
        $user = User::find(auth()->id());
        $this->user_id = $user->id;
        $this->name = $user->name;
        $this->phone = $user->phone;
        $this->email = $user->email;
        $this->old_photo = $user->photo;
        return view('livewire.profile.index')
            ->layout("layouts.admin-livewire", [
                "title" => "Profile"
            ]);
    }

    public function patch()
    {
        $this->validate([
            'name' => 'required',
            'phone' => [
                'required',
                'min:6',
                Rule::unique('users', 'phone')->ignore($this->user_id)
            ],
            'email' => [
                'required',
                'email',
                Rule::unique('users', 'email')->ignore($this->user_id)
            ],
            'password' => 'nullable|min:6',
            "photo" => "nullable|image|mimes:jpeg,png,jpg|max:1024",
        ]);


        $user = User::find($this->user_id);
        $user->name = $this->name;
        $user->phone = $this->phone;
        $user->email = $this->email;
        if ($this->password) {
            $user->password = bcrypt($this->password);
        }
        if ($this->photo) {
            $photoUrl = cloudinary()->upload($this->photo->getRealPath(), [
                'transformation' => CloudinaryTransformation::IMAGE,
                'public_id' => $user->role . "/" . uniqid()
            ])->getSecurePath();
            $user->photo = $photoUrl;
        }
        $user->save();
        $this->dispatchBrowserEvent('swal:toast', [
            'title' => "Berhasil diubah!",
            "type" => "success"
        ]);
    }
}
