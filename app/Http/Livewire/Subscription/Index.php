<?php

namespace App\Http\Livewire\Subscription;

use App\Models\CompanyType;
use App\Models\Subscription;
use App\Models\SubscriptionType;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use WithPagination;

    protected $paginationTheme = 'bootstrap';
    protected $listeners = ['confirm' => 'delete'];

    public $search;
    protected $queryString = ['search'];

    public $company_type_id, $daily_amount, $monthly_amount;
    public $id_edit, $company_type_id_edit, $daily_amount_edit, $monthly_amount_edit;

    protected $validationAttributes = [
        'company_type_id' => 'Jenis Usaha',
        'daily_amount' => 'Harian',
        'monthly_amount' => 'Bulanan',
        'company_type_id_edit' => 'Jenis Usaha',
        'daily_amount_edit' => 'Harian',
        'monthly_amount_edit' => 'Bulanan',
    ];

    public function updatingSearch()
    {
        $this->resetPage();
    }


    public function render()
    {
        $list = Subscription::get();
        $ctList = $list->unique('company_type_id');
        $fnList = collect();
        foreach ($ctList as $item) {
            $find = $list->where('company_type_id', $item->company_type_id)->values();
            $item['type'] = $item->company_type->type;
            $item['daily_subscription'] = $find->first(function ($val) {
                return $val->subscription_type_id == 1;
            })->subscription_amount;
            $item['monthly_subscription'] = $find->first(function ($val) {
                return $val->subscription_type_id == 2;
            })->subscription_amount;
            $fnList[] = $item;
        }

        $subscriptions = $fnList->filter(function ($item) {
            // replace stristr with your choice of matching function
            return false !== stristr($item->type, $this->search);
        })->values()->sortByDesc('id')->paginate(10);
        $companyTypes = CompanyType::get();
        $companyTypeOptions = CompanyType::doesntHave('subscriptions')->get();
        $subscriptionTypes = SubscriptionType::get();
        return view('livewire.subscription.index', compact("subscriptions", "companyTypes", "subscriptionTypes", "companyTypeOptions"))->layout("layouts.admin-livewire", [
            "title" => "Iuran"
        ]);
    }

    public function post()
    {
        $this->validate([
            "company_type_id" => 'required',
            "daily_amount" => "required|numeric|min:1",
            "monthly_amount" => "required|numeric|min:1"
        ]);
        Subscription::create([
            'company_type_id' => $this->company_type_id,
            'subscription_type_id' => 1,
            'subscription_amount' => $this->daily_amount
        ]);

        Subscription::create([
            'company_type_id' => $this->company_type_id,
            'subscription_type_id' => 2,
            'subscription_amount' => $this->monthly_amount
        ]);

        $this->reset(['company_type_id', 'daily_amount', 'monthly_amount']);
        $this->dispatchBrowserEvent('formModal:close');
        $this->dispatchBrowserEvent('swal:toast', [
            'title' => "Berhasil ditambahkan!",
            "type" => "success"
        ]);
    }

    public function deleteConfirmation($id)
    {
        $this->dispatchBrowserEvent('swal:deleteConfirmation', [
            'id' => $id
        ]);
    }

    public function delete(Subscription $subscription)
    {
        Subscription::where('company_type_id', $subscription->company_type_id)->delete();
        $this->dispatchBrowserEvent('swal:toast', [
            'title' => "Berhasil dihapus!",
            "type" => "success"
        ]);
    }

    public function openEdit(Subscription $subscription)
    {
        $ss = Subscription::where('company_type_id', $subscription->company_type->id)->get();
        $daily = $ss->first(function ($val) {
            return $val->subscription_type_id == 1;
        });
        $monthly = $ss->first(function ($val) {
            return $val->subscription_type_id == 2;
        });

        $this->id_edit = $subscription->id;
        $this->company_type_id_edit = $subscription->company_type_id;
        $this->daily_amount_edit = $daily->subscription_amount;
        $this->monthly_amount_edit = $monthly->subscription_amount;
        $this->emit("editModal:open");
    }

    public function edit()
    {
        $this->validate([
            "company_type_id_edit" => 'required',
            "daily_amount_edit" => "required|numeric|min:1",
            "monthly_amount_edit" => "required|numeric|min:1"
        ]);
        $ss = Subscription::where('company_type_id', $this->company_type_id_edit)->get();
        $daily = $ss->first(function ($val) {
            return $val->subscription_type_id == 1;
        });
        $monthly = $ss->first(function ($val) {
            return $val->subscription_type_id == 2;
        });
        $daily->subscription_amount = $this->daily_amount_edit;
        $daily->save();

        $monthly->subscription_amount = $this->monthly_amount_edit;
        $monthly->save();

        $this->reset(['company_type_id_edit', 'id_edit', 'monthly_amount_edit', 'daily_amount_edit']);
        $this->dispatchBrowserEvent('editModal:close');
        $this->dispatchBrowserEvent('swal:toast', [
            'title' => "Berhasil diubah!",
            "type" => "success"
        ]);
    }

}
