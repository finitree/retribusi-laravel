<?php

namespace App\Http\Livewire\Setting;

use App\Models\Setting;
use Livewire\Component;

class Index extends Component
{
    public $is_instalment_available;

    public function render()
    {
        $setting = Setting::first();
        $this->is_instalment_available = $setting->is_instalment_available == 1;
        return view('livewire.setting.index', compact("setting"))
            ->layout('layouts.admin-livewire', [
                "title" => "Pengaturan"
            ]);
    }

    public function submit()
    {
        $willEdited = Setting::first();
        $willEdited->is_instalment_available = $this->is_instalment_available ? 1 : 0;
        $willEdited->save();
        $this->dispatchBrowserEvent('swal:toast', [
            'title' => "Berhasil disimpan!",
            "type" => "success"
        ]);

    }
}
