<?php

namespace App\Http\Livewire\Owner;

use App\Models\User;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use WithPagination;

    protected $paginationTheme = 'bootstrap';
    public $search;
    protected $queryString = ['search'];
    protected $listeners = ['confirm' => 'delete'];

    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function render()
    {

        $owners = User::with('companies')
            ->where('role', 'owner')
            ->where("name", "like", "%" . $this->search . "%")
            ->latest()
            ->paginate(10);

        return view('livewire.owner.index',
            compact("owners")
        )->layout("layouts.admin-livewire", [
            "title" => "Pemilik Usaha"
        ]);
    }


    public function deleteConfirmation($id)
    {
        $this->dispatchBrowserEvent('swal:deleteConfirmation', [
            'id' => $id
        ]);
    }

    public function delete(User $user)
    {
        $user->delete();
        $this->dispatchBrowserEvent('swal:toast', [
            'title' => "Berhasil dihapus!",
            "type" => "success"
        ]);
    }

}
