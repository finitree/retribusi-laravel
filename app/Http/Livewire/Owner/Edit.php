<?php

namespace App\Http\Livewire\Owner;

use App\Models\CloudinaryTransformation;
use App\Models\Company;
use App\Models\Transaction;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Validation\Rule;
use Livewire\Component;
use Livewire\WithFileUploads;
use Livewire\WithPagination;

class Edit extends Component
{
    use WithFileUploads;
    use WithPagination;

    protected $paginationTheme = 'bootstrap';
    public $search, $date, $transactionShow;
    protected $queryString = ['search', 'date'];


    public $idOwner, $name, $email, $photo, $ktp, $password, $phone;
    public $old_photo, $old_ktp;

    protected $validationAttributes = [
        'name' => 'Nama',
        'photo' => "Foto",
        "password" => "Kata Sandi",
        "phone" => "Telepon"
    ];


    public function rules()
    {
        return [
            "name" => "required",
            "email" => [
                "required",
                "email",
                Rule::unique("users", "email")->ignore($this->idOwner)
            ],
            "photo" => "nullable|image|mimes:jpeg,png,jpg|max:1024",
            "ktp" => "nullable|image|mimes:jpeg,png,jpg|max:1024",
            "password" => "nullable|min:6",
            "phone" => [
                "required",
                Rule::unique("users", "phone")->ignore($this->idOwner)
            ]
        ];
    }

    public function render()
    {
        $companies = Company::where('user_id', $this->idOwner)
            ->where("name", "like", "%" . $this->search . "%")
            ->latest()->paginate(10);

        $transactions = Transaction::with( 'staff', 'detail')
            ->whereHas('detail', function ($q) {
                return $q->whereHas('company', function ($q1){
                    return $q1->where('user_id', $this->idOwner);
                });
            })
            ->when($this->date, function ($q) {
                return $q->whereDate('created_at', Carbon::parse($this->date));
            })
            ->latest()->paginate(10);

        return view('livewire.owner.edit',
            compact("companies", "transactions")
        )
            ->layout("layouts.admin-livewire", [
                "title" => "Ubah Pemilik Usaha"
            ]);
    }

    public function mount($id)
    {
        $user = User::find($id);
        $this->idOwner = $id;
        $this->name = $user->name;
        $this->phone = $user->phone;
        $this->email = $user->email;
        $this->old_photo = $user->photo;
        $this->old_ktp = $user->ktp;
    }

    public function patch()
    {
        $this->validate();
        $user = User::find($this->idOwner);
        $user->name = $this->name;
        $user->phone = $this->phone;
        $user->email = $this->email;
        if ($this->password) {
            $user->password = bcrypt($this->password);
        }
        if ($this->ktp) {
            $ktpUrl = cloudinary()->upload($this->ktp->getRealPath(), [
                'transformation' => CloudinaryTransformation::IMAGE,
                'public_id' => "ktp/" . uniqid()
            ])->getSecurePath();
            $user->ktp = $ktpUrl;
        }
        if ($this->photo) {
            $photoUrl = cloudinary()->upload($this->photo->getRealPath(), [
                'transformation' => CloudinaryTransformation::IMAGE,
                'public_id' => "owner/" . uniqid()
            ])->getSecurePath();
            $user->photo = $photoUrl;
        }
        $user->save();
        session()->flash("success", "Berhasil diubah!");
        $this->redirectRoute("owner.index");
    }

    public function removeFilter()
    {
        $this->date = "";
    }

    public function showDetailTransaction($id)
    {
        $this->transactionShow = Transaction::find($id);
        $this->emit("showModal:open");
    }
}
