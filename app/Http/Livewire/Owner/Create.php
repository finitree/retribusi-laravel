<?php

namespace App\Http\Livewire\Owner;

use App\Models\CloudinaryTransformation;
use App\Models\User;
use Livewire\Component;
use Livewire\WithFileUploads;

class Create extends Component
{
    use WithFileUploads;

    public $name, $email, $photo, $ktp, $password, $phone;

    public function rules()
    {
        return [
            "name" => "required",
            "email" => "required|email|unique:users",
            "photo" => "required|image|mimes:jpeg,png,jpg|max:1024",
            "ktp" => "required|image|mimes:jpeg,png,jpg|max:1024",
            "password" => "required|min:6",
            "phone" => "required|unique:users"
        ];
    }

    protected $validationAttributes = [
        'name' => 'Nama',
        'photo' => "Foto",
        "password" => "Kata Sandi",
        "phone" => "Telepon"
    ];


    public function post()
    {
        $this->validate();
        $imgUrl = cloudinary()->upload($this->photo->getRealPath(), [
            'transformation' => CloudinaryTransformation::IMAGE,
            'public_id' => "owner/" . uniqid()
        ])->getSecurePath();
        $ktpUrl = cloudinary()->upload($this->ktp->getRealPath(), [
            'transformation' => CloudinaryTransformation::IMAGE,
            'public_id' => "ktp/" . uniqid()
        ])->getSecurePath();
        User::create([
            "role" => "owner",
            "name" => $this->name,
            "email" => $this->email,
            "password" => bcrypt($this->password),
            "phone" => $this->phone,
            "ktp" => $ktpUrl,
            "photo" => $imgUrl
        ]);
        $this->reset();
        session()->flash("success", "Berhasil ditambahkan!");
        $this->redirectRoute("owner.index");
    }

    public function render()
    {
        return view('livewire.owner.create')
            ->layout("layouts.admin-livewire", [
                "title" => "Tambah Pemilik Usaha"
            ]);
    }
}
