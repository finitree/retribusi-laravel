<?php

namespace App\Http\Livewire\Balance;

use App\Models\Balance;
use App\Models\Staff;
use Carbon\Carbon;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use WithPagination;

    protected $paginationTheme = 'bootstrap';
    protected $listeners = ['confirm' => 'delete'];


    public $date;
    protected $queryString = ['date'];

    public $staffId, $amount, $staffAmount, $description, $balanceDate;
    public $idEdit, $staffIdEdit, $amountEdit, $staffAmountEdit, $descriptionEdit, $balanceDateEdit;

    protected $validationAttributes = [
        'staffId' => 'Petugas',
        'amount' => 'Nominal Setoran',
        'staffAmount' => 'Fee Petugas',
        'description' => 'Deskripsi',
        "balanceDate" => "Tanggal",
        'staffIdEdit' => 'Petugas',
        'amountEdit' => 'Nominal Setoran',
        'staffAmountEdit' => 'Fee Petugas',
        'descriptionEdit' => 'Deskripsi',
        "balanceDateEdit" => "Tanggal",
    ];

    public function render()
    {
        $staffs = Staff::with('user')->latest()->get();
        $balances = Balance::with('staff')->where('type', 'out')->when($this->date, function ($q) {
            return $q->whereDate('created_at', $this->date);
        })->latest()->paginate(10);

        return view('livewire.balance.index',
            compact(
                "balances",
                "staffs",
            )
        )->layout("layouts.admin-livewire", [
            "title" => "Setoran Petugas"
        ]);
    }

    public function post()
    {
        $this->validate([
            "staffId" => "required",
            "amount" => "required|numeric|min:1",
            "staffAmount" => "nullable|numeric|min:0",
            "balanceDate" => "required",
            "description" => "nullable"
        ]);

        Balance::create([
            "staff_id" => $this->staffId,
            "amount" => $this->amount,
            "staff_amount" => $this->staffAmount,
            "notes" => $this->description,
            "type" => "out",
            "created_at" => Carbon::parse($this->balanceDate)
        ]);

        $this->reset();
        $this->dispatchBrowserEvent('formModal:close');
        $this->dispatchBrowserEvent('swal:toast', [
            'title' => "Berhasil ditambahkan!",
            "type" => "success"
        ]);

    }

    public function openEdit(Balance $balance)
    {
        $this->idEdit = $balance->id;
        $this->staffIdEdit = $balance->staff_id;
        $this->amountEdit = $balance->amount;
        $this->staffAmountEdit = $balance->staff_amount;
        $this->descriptionEdit = $balance->notes;
        $this->balanceDateEdit = $balance->created_at->format("Y-m-d");
        $this->emit("editModal:open");
    }

    public function removeFilter()
    {
        $this->date = "";
    }

    public function edit()
    {
        $this->validate([
            "staffIdEdit" => "required",
            "amountEdit" => "required|numeric|min:1",
            "staffAmountEdit" => "nullable|numeric|min:0",
            "balanceDateEdit" => "required",
            "descriptionEdit" => "nullable"
        ]);

        $balance = Balance::find($this->idEdit);
        $balance->staff_id = $this->staffIdEdit;
        $balance->amount = $this->amountEdit;
        $balance->staff_amount = $this->staffAmountEdit;
        $balance->notes = $this->descriptionEdit;
        $balance->created_at = Carbon::parse($this->balanceDateEdit);
        $balance->save();

        $this->reset();
        $this->dispatchBrowserEvent('editModal:close');
        $this->dispatchBrowserEvent('swal:toast', [
            'title' => "Berhasil diubah!",
            "type" => "success"
        ]);
    }


    public function deleteConfirmation($id)
    {
        $this->dispatchBrowserEvent('swal:deleteConfirmation', [
            'id' => $id
        ]);
    }

    public function delete(Balance $balance)
    {
        $balance->delete();
        $this->dispatchBrowserEvent('swal:toast', [
            'title' => "Berhasil dihapus!",
            "type" => "success"
        ]);
    }


}
