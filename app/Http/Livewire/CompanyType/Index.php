<?php

namespace App\Http\Livewire\CompanyType;

use App\Models\CompanyType;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use WithPagination;

    protected $paginationTheme = 'bootstrap';
    public $search;
    protected $queryString = ['search'];

    public $type, $is_document_required;
    public $type_edit, $is_document_required_edit, $id_edit;
    protected $listeners = ['confirm' => 'delete'];


    protected $validationAttributes = [
        'type' => 'Jenis Usaha',
        'is_document_required' => 'Surat Ijin Usaha',
        'type_edit' => 'Jenis Usaha',
        'is_document_required_edit' => 'Surat Ijin Usaha'
    ];

    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function render()
    {
        $company_types = CompanyType::where('type', 'like', '%' . $this->search . '%')->latest()->paginate(10);
        return view('livewire.company-type.index', compact("company_types"))->layout("layouts.admin-livewire", [
            "title" => "Jenis Usaha"
        ]);
    }

    public function post()
    {
        $this->validate([
            'type' => 'required|min:4',
            'is_document_required' => 'required'
        ]);
        CompanyType::create([
            "type" => $this->type,
            "is_document_required" => $this->is_document_required
        ]);
        $this->reset(['type', 'is_document_required']);
        $this->dispatchBrowserEvent('formModal:close');
        $this->dispatchBrowserEvent('swal:toast', [
            'title' => "Berhasil ditambahkan!",
            "type" => "success"
        ]);
    }

    public function openEdit(CompanyType $companyType)
    {
        $this->type_edit = $companyType->type;
        $this->id_edit = $companyType->id;
        $this->is_document_required_edit = $companyType->is_document_required;
        $this->emit("editModal:open");

//        $this->dispatchBrowserEvent('editModal:open', [
////            "banjar" => $banjar
//        ]);
    }

    public function edit()
    {
        $this->validate([
            'type_edit' => 'required|min:4',
            'is_document_required_edit' => 'required'
        ]);

        $companyType = CompanyType::find($this->id_edit);
        $companyType->type = $this->type_edit;
        $companyType->is_document_required = $this->is_document_required_edit;
        $companyType->save();
        $this->reset(['type_edit', 'id_edit', 'is_document_required_edit']);
        $this->dispatchBrowserEvent('editModal:close');
        $this->dispatchBrowserEvent('swal:toast', [
            'title' => "Berhasil diubah!",
            "type" => "success"
        ]);

    }

    public function deleteConfirmation($id)
    {
        $this->dispatchBrowserEvent('swal:deleteConfirmation', [
            'id' => $id
        ]);
    }

    public function delete($id)
    {
        CompanyType::find($id)->delete();
        $this->dispatchBrowserEvent('swal:toast', [
            'title' => "Berhasil dihapus!",
            "type" => "success"
        ]);
    }


}
