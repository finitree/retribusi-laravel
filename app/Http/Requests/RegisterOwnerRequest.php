<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterOwnerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:55',
            'email' => 'email|unique:users|nullable',
            'password' => 'required',
            'phone' => 'required|unique:users',
            'company_name' => 'required',
            'company_type_id' => 'required',
            'subscription_type_id' => 'required',
            'latitude' => 'required',
            'longitude' => 'required',
            'address' => 'required',
            'photos' => 'required',
            'ktp' => 'required',

            'company_operational_schedule' => 'required|array|min:7',
            'company_operational_schedule.*.day' => 'required',
            'company_operational_schedule.*.opening_hours' => 'required',
            'company_operational_schedule.*.closing_hours' => 'required',
            'company_operational_schedule.*.is_open' => 'required'

        ];
    }
}
