<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CompanyCloseResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'is_close' => $this->is_close,
            'is_cuti' => $this->is_cuti,
            'schedule' => $this->schedule,
            'current_schedule' => new CompanyOperationalScheduleResource($this->current_schedule)
        ];
    }
}
