<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CompanyOperationalScheduleResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'company_id' => $this->company_id,
            'day' => $this->day,
            'opening_hours' => date_format(date_create($this->opening_hours),'H:i'),
            'closing_hours' => date_format(date_create($this->closing_hours),'H:i'),
            'is_open' => $this->is_open
        ];
    }
}
