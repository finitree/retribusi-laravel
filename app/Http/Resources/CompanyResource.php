<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CompanyResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "user_id" => $this->user_id,
            "subscription_id" => $this->subscription_id,
            "subscription_amount" => $this->subscription_amount,
            "banjar_id" => $this->banjar_id,
            "company_type_id" => $this->subscription->company_type_id,
            "name" => $this->name,
            "latitude" => $this->latitude,
            "longitude" => $this->longitude,
            "address" => $this->address,
            "photos" => $this->photos,
            "documents" => $this->documents,
            "status" => $this->status,
            "created_at" => $this->created_at,
            "updated_at" => $this->updated_at,
            "deleted_at" => $this->deleted_at,
            "verified_at" => $this->verified_at,
            "transaction_status" => $this->transaction_status,
            "next_transaction" => $this->next_transaction,
            "company_close" => new CompanyCloseResource($this->company_close),
            "subscription" => $this->subscription,
            "banjar" => $this->banjar,
            "company_type" => $this->companyType,
            "user" => $this->user,
            "unpaid_transaction" => UnpaidTransactionResource::collection($this->unpaid_transaction),
            "unpaid_amount" => $this->unpaid_amount,
            "company_operational_schedule" => CompanyOperationalScheduleResource::collection($this->company_operational_schedule),
        ];
    }
}
