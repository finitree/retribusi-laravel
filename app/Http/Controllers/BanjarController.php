<?php

namespace App\Http\Controllers;

use App\Models\Banjar;
use Illuminate\Http\Request;

class BanjarController extends Controller
{
    public function index()
    {
        $banjars = Banjar::latest()->get();
        return view("banjar.index", compact("banjars"));
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        Banjar::create([
            'name' => $request->banjar,
            'address' => $request->address,
        ]);
        $this->successAddToast();
        return redirect()->back();
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
