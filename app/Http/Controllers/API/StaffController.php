<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserUpdateRequest;
use App\Http\Resources\ModelResource;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class StaffController extends Controller
{
    public function show()
    {
        $user = User::with('staff')->find(auth()->id());
        return new ModelResource($user);
    }

    public function update(UserUpdateRequest $request)
    {
        $user = User::find(Auth::id());
        $user->name = $request->name;
        if ($request->email) {
            $user->email = $request->email;
        }
        if ($request->password) {
            $user->password = bcrypt($request->password);
        }
        $user->phone = $request->phone;
        $user->save();
        return new ModelResource($user);
    }
}
