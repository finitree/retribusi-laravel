<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\ModelResource;
use App\Http\Resources\SuccessResource;
use App\Models\CompanyCloseSchedule;
use Illuminate\Http\Request;

class CompanyCloseScheduleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($companyId)
    {
        $schedules = CompanyCloseSchedule::where('company_id', $companyId)->latest()->get();
        return ModelResource::collection($schedules);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return SuccessResource
     */
    public function store(Request $request, $companyId)
    {
        CompanyCloseSchedule::create([
            'company_id' => $companyId,
            'date_start' => $request->date_start,
            'date_end' => $request->date_end,
            'description' => $request->description
        ]);
        return new SuccessResource([]);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\CompanyCloseSchedule $companyCloseSchedule
     * @return \Illuminate\Http\Response
     */
    public function show(CompanyCloseSchedule $companyCloseSchedule)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\CompanyCloseSchedule $companyCloseSchedule
     * @return \Illuminate\Http\Response
     */
    public function edit(CompanyCloseSchedule $companyCloseSchedule)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\CompanyCloseSchedule $companyCloseSchedule
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $companyId, CompanyCloseSchedule $companyCloseSchedule)
    {
        $companyCloseSchedule->update([
            'company_id' => $companyId,
            'date_start' => $request->date_start,
            'date_end' => $request->date_end,
            'description' => $request->description
        ]);
        return new ModelResource([]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\CompanyCloseSchedule $companyCloseSchedule
     * @return \Illuminate\Http\Response
     */
    public function destroy($companyId, CompanyCloseSchedule $companyCloseSchedule)
    {
        $companyCloseSchedule->delete();
        return new SuccessResource([]);
    }
}
