<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\CompanyCollection;
use App\Http\Resources\CompanyResource;
use App\Models\Company;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use function response;

class CompanyController extends Controller
{
    public function index()
    {
        $companies = Company::all();
        return new CompanyCollection($companies);
    }

    public function show(Company $company)
    {
        return new CompanyResource($company);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'name' => 'required',
            'company_type_id' => 'required',
            'latitude' => 'required',
            'longitude' => 'required',
            'address' => 'required',
            'photos' => 'required',
        ]);

        if($validator->fails()){
            return response()->json([
                'error' => [
                    'code' => 422,
                    'title' => 'Validation Error',
                    'errors' => $validator->errors(),
                ]
            ], 422);
        }

        Company::create([
            'user_id' => $request->user_id,
            'name' => $request->name,
            'latitude' => $request->latitude,
            'longitude' => $request->longitude,
            'address' => $request->address,
            'photos' => $request->photos,
            'documents' => $request->documents,
            'status' => Company::WAIT_VERIFIED,
        ]);

        $response = Company::latest()->first();

        return new CompanyResource($response);
    }
    public function update(Request $request, Company $company)
    {
        $validator = Validator::make($request->all(),[
            'name' => 'required',
            'company_type_id' => 'required',
            'latitude' => 'required',
            'longitude' => 'required',
            'address' => 'required',
            'photos' => 'required',
        ]);

        if($validator->fails()){
            return response()->json([
                'error' => [
                    'code' => 422,
                    'title' => 'Validation Error',
                    'errors' => $validator->errors(),
                ]
            ], 422);
        }

        $company->update([
            'tempekan_id' => $request->tempekan_id,
            'company_type_id' => $request->company_type_id,
            'name' => $request->name,
            'latitude' => $request->latitude,
            'longitude' => $request->longitude,
            'address' => $request->address,
            'photos' => $request->photos,
            'documents' => $request->documents,
            'status' => Company::WAIT_VERIFIED,
        ]);

        return response()->json([
            'data' => 'success'
        ], 200);
    }
}
