<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\ModelResource;
use App\Models\BaseModel;
use App\Models\Staff;
use App\Models\Transaction;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ExecutiveTransactionController extends Controller
{
    public function getDailyTransaction(Request $request)
    {
        if ($request->date) {
            $data = Transaction::with('company', 'staff')->whereDate('created_at', Carbon::parse($request->date))->latest()->get();
        } else {
            $data = Transaction::with('company')->whereDate('created_at', Carbon::now())->whereMonth('created_at', Carbon::now())->whereYear('created_at', Carbon::now())->get();
        }
        $model = new BaseModel();
        $model->total_transaction = $data->sum('total_amount');
        $model->total_qty = $data->count();
        $model->transactions = $data;
        return new ModelResource($model);
    }

    public function getMonthlyTransaction(Request $request)
    {
        if ($request->month) {
            $data = Transaction::with('company', 'staff')->whereMonth('created_at', Carbon::parse($request->month))->whereYear('created_at', Carbon::parse($request->month))->latest()->get();
        } else {
            $data = Transaction::whereYear('created_at', Carbon::now())->get();
        }
        $model = new BaseModel();
        $model->total_transaction = $data->sum('total_amount');
        $model->total_qty = $data->count();
        $model->transactions = $data;
        return new ModelResource($model);
    }

    public function getStaffTransaction()
    {
        $staffs = Staff::with([
            'transactions' => function ($q) {
                return $q->orderBy('id', 'desc');
            },
            'companies'
        ])->latest()->get();
        return ModelResource::collection($staffs);
    }

    public function getStaffDetailTransaction(Request $request)
    {
        $data = Transaction::with('company', 'staff')
            ->where('staff_id', $request->id)
            ->when($request->date, function ($q) use ($request) {
                return $q->whereDate('created_at', Carbon::parse($request->date));
            })
            ->latest()->get();
        $model = new BaseModel();
        $model->total_transaction = $data->sum('total_amount');
        $model->total_qty = $data->count();
        $model->transactions = $data;
        return new ModelResource($model);
    }
}
