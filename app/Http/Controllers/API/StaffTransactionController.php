<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\ModelResource;
use App\Http\Resources\SuccessResource;
use App\Models\Balance;
use App\Models\Company;
use App\Models\CompanyUnpaidTransaction;
use App\Models\helper\OwnerTransaction;
use App\Models\Staff;
use App\Models\Transaction;
use App\Models\TransactionDetail;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use mysql_xdevapi\Exception;

class StaffTransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $companyId = $request->company_id;
        $company = Company::find($companyId);
        $array = array();
        if ($request->type == Transaction::PAID) {
            $transactions = TransactionDetail::where('company_id', $companyId)
                ->when($request->date_from, function ($q) use ($request) {
                    $from = date($request->date_from);
                    $to = Carbon::parse($request->date_to)->addDay();
                    return $q->whereBetween('transaction_at', [$from, $to]);
                })
                ->latest()->get();
            foreach ($transactions as $transaction) {
                $tr = new OwnerTransaction();
                $tr->company_name = $transaction->company->name;
                $tr->amount = $transaction->amount;
                $tr->date = Carbon::parse($transaction->transaction_at);
//                $tr->money_pay = $transaction->money_pay;
//                $tr->money_changes = $transaction->money_changes;
                array_push($array, $tr);
            }
        } else if ($request->type == Transaction::UNPAID) {
            $unpaids = CompanyUnpaidTransaction::where('company_id', $company->id)
                ->when($request->date_from, function ($q) use ($request) {
                    $from = date($request->date_from);
                    $to = Carbon::parse($request->date_to)->addDay();
                    return $q->whereBetween('unpaid_at', [$from, $to]);
                })
                ->get();
            foreach ($unpaids as $unpaid) {
                $tr = new OwnerTransaction();
                $tr->company_name = $company->name;
                $tr->amount = $unpaid->amount;
                $tr->date = Carbon::parse($unpaid->unpaid_at);
                array_push($array, $tr);
            }
        }
        $collec = collect($array);
        return ModelResource::collection($collec->sortByDesc('date')->values());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $company = Company::find($request->company_id);
        if ($company->transaction_status == Transaction::UNPAID) {
            $total = $company->unpaid_amount;
            $staffId = Staff::where('user_id', auth()->id())->first()->id;

            $transaction = Transaction::create([
                'staff_id' => $staffId,
                'total_amount' => $total,
                'money_pay' => $request->money_pay,
                'money_changes' => $request->money_changes
            ]);

            foreach ($company->unpaid_transaction as $unpaid) {
                TransactionDetail::create([
                    "transaction_id" => $transaction->id,
                    'company_id' => $request->company_id,
                    'subscription_type_id' => $company->subscription->subscription_type_id,
                    'amount' => $company->subscription_amount,
                    "transaction_at" => $unpaid->unpaid_at
                ]);
            }

            $staff = Staff::find($staffId);
            $staff->balance = $staff->balance + $total;
            $staff->save();
            Balance::create([
                'staff_id' => $staffId,
                'type' => 'in',
                'amount' => $total,
                'notes' => "Retribusi " . $company->name
            ]);

            $unpaidHistories = CompanyUnpaidTransaction::where('company_id', $company->id)->get();
            foreach ($unpaidHistories as $i) {
                $i->delete();
            }

            return new SuccessResource([]);
        } else {
            abort(422, "Already Paid");
        }
    }

    public function bulkStoreTransaction(Request $request)
    {
        DB::beginTransaction();
        try {
            $companies = $request->companies;
            $unpaid_total = $companies->sum('unpaid_amount');
            $staffId = Staff::where('user_id', auth()->id())->first()->id;
            $transaction = Transaction::create([
                'staff_id' => $staffId,
                'total_amount' => $unpaid_total,
                'money_pay' => $unpaid_total,
                'money_changes' => 0
            ]);
            foreach ($companies as $company) {
                foreach ($company->unpaid_transaction as $unpaid) {
                    TransactionDetail::create([
                        "transaction_id" => $transaction->id,
                        'company_id' => $company->id,
                        'subscription_type_id' => $company->subscription->subscription_type_id,
                        'amount' => $company->subscription_amount,
                        "transaction_at" => $unpaid->unpaid_at
                    ]);
                }
                $total = $company->unpaid_amount;
                $staff = Staff::find($staffId);
                $staff->balance = $staff->balance + $total;
                $staff->save();
                Balance::create([
                    'staff_id' => $staffId,
                    'type' => 'in',
                    'amount' => $total,
                    'notes' => "Retribusi " . $company->name
                ]);

                $unpaidHistories = CompanyUnpaidTransaction::where('company_id', $company->id)->get();
                foreach ($unpaidHistories as $i) {
                    $i->delete();
                }
            }


            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
            abort(500, $e->getMessage());
        }
        return new SuccessResource([]);
    }

    public function storeInstalmentTransaction(Request $request)
    {
        DB::beginTransaction();
        try {
            $unpaidIds = $request->unpaid_ids;
            $totals = 0;
            $unpaidData = CompanyUnpaidTransaction::with('company')->whereIn('id', $unpaidIds)->get();
            foreach ($unpaidData as $unpaid) {
                $totals += $unpaid->amount;
            }
            $companyName = $unpaidData->first()->company->name;
            $staffId = Staff::where('user_id', auth()->id())->first()->id;
            $transaction = Transaction::create([
                'staff_id' => $staffId,
                'total_amount' => $totals,
                'money_pay' => $request->money_pay,
                'money_changes' => $request->money_changes
            ]);
            foreach ($unpaidData as $unpaid) {
                TransactionDetail::create([
                    "transaction_id" => $transaction->id,
                    'company_id' => $unpaid->company_id,
                    'subscription_type_id' => $unpaid->subscription_type_id,
                    'amount' => $unpaid->amount,
                    "transaction_at" => $unpaid->unpaid_at
                ]);
            }
            $unpaidData->each(function ($i) {
                $i->delete();
            });

            $staff = Staff::find($staffId);
            $staff->balance = $staff->balance + $totals;
            $staff->save();
            Balance::create([
                'staff_id' => $staffId,
                'type' => 'in',
                'amount' => $totals,
                'notes' => "Retribusi " . $companyName
            ]);

            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
            abort(500, $e->getMessage());
        }
        return new SuccessResource([]);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
