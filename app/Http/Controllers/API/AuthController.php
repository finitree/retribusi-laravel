<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\RegisterOwnerRequest;
use App\Http\Resources\CompanyResource;
use App\Models\Company;
use App\Models\CompanyOperationalSchedule;
use App\Models\Subscription;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function registerOwner(RegisterOwnerRequest $request)
    {
        $request['password'] = bcrypt($request->password);
        $request['role'] = User::OWNER;

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => $request->password,
            'phone' => $request->phone,
            'role' => $request->role,
            'photo' => $request->photo,
            'ktp' => $request->ktp
        ]);
        $accessToken = $user->createToken('authToken')->accessToken;

        $subscription_id = Subscription::where('subscription_type_id', '=', $request->subscription_type_id)->where('company_type_id', $request->company_type_id)->first()->id;

        $companyStore = Company::create([
            'user_id' => $user->id,
            'banjar_id' => $request->banjar_id,
            'subscription_id' => $subscription_id,
            'name' => $request->company_name,
            'latitude' => $request->latitude,
            'longitude' => $request->longitude,
            'address' => $request->address,
            'photos' => $request->photos,
            'documents' => $request->documents,
            'status' => Company::WAIT_VERIFIED,
        ]);

        $company = Company::find($companyStore->id);
        foreach ($request->company_operational_schedule as $operational) {
            CompanyOperationalSchedule::create([
                'company_id' => $company->id,
                'day' => $operational['day'],
                'opening_hours' => $operational['opening_hours'],
                'closing_hours' => $operational['closing_hours'],
                'is_open' => $operational['is_open']
            ]);
        }


        return response([
            'data' => [
                'user' => $user, 'access_token' => $accessToken, 'company' => new CompanyResource($company)
            ]
        ]);
    }

    public function registerStaff(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required|max:55',
            'email' => 'email|required|unique:users',
            'password' => 'required|confirmed|min:8',
            'phone' => 'required',
        ]);

        $validatedData['password'] = bcrypt($request->password);
        $validatedData['role'] = User::STAFF;

        $user = User::create($validatedData);

        $accessToken = $user->createToken('authToken')->accessToken;

        return response()->json([
            'data' => [
                'user' => $user, 'access_token' => $accessToken
            ]
        ]);
    }

    public function login(Request $request)
    {
        //dd($request);
        $loginData = $request->validate([
            'phone' => 'required',
            'password' => 'required'
        ]);
        $user = User::where('phone', $request->phone)->first();

        if (!auth()->attempt($loginData) || $user->role != "owner") {
            return response()->json([
                'error' => [
                    'code' => 422,
                    'title' => 'Error',
                    'errors' => [
                        [
                            'title' => 'Data not Matched',
                            'message' => 'Wrong phone or password'
                        ]
                    ]
                ]
            ], 422);
        }

        $accessToken = auth()->user()->createToken('authToken')->accessToken;

        return response()->json([
            'data' => [
                'user' => auth()->user(),
                'access_token' => $accessToken
            ]
        ]);

        //return response(['data' => auth()->user(), 'access_token' => $accessToken]);

    }

    public function loginStaff(Request $request)
    {
        $loginData = $request->validate([
            'phone' => 'required',
            'password' => 'required'
        ]);
        $user = User::with('staff')->where('phone', $request->phone)->first();

        $userCount = User::where('phone', $request->phone)->where('role', User::STAFF)->get()->count();
        if ($userCount == 0 && !$user) {
            return response()->json([
                'error' => [
                    'code' => 422,
                    'title' => 'Error',
                    'errors' => [
                        [
                            'title' => 'Data not Matched',
                            'message' => 'Wrong phone or password'
                        ]
                    ]
                ]
            ], 422);
        }
        if (!auth()->attempt($loginData) || $user->role == "owner") {
            return response()->json([
                'error' => [
                    'code' => 422,
                    'title' => 'Error',
                    'errors' => [
                        [
                            'title' => 'Data not Matched',
                            'message' => 'Wrong phone or password'
                        ]
                    ]
                ]
            ], 422);
        }

        $accessToken = auth()->user()->createToken('authToken')->accessToken;
        return response()->json([
            'data' => [
                'user' => auth()->user(),
                'access_token' => $accessToken
            ]
        ]);
    }

    public function loginExecutive(Request $request)
    {
        $loginData = $request->validate([
            'phone' => 'required',
            'password' => 'required'
        ]);

        $user = User::where('phone', $request->phone)->first();

        if (!auth()->attempt($loginData) || $user->role != "executive") {
            return response()->json([
                'error' => [
                    'code' => 422,
                    'title' => 'Error',
                    'errors' => [
                        [
                            'title' => 'Data not Matched',
                            'message' => 'Wrong phone or password'
                        ]
                    ]
                ]
            ], 422);
        }

        $accessToken = auth()->user()->createToken('authToken')->accessToken;

        return response()->json([
            'data' => [
                'user' => auth()->user(),
                'access_token' => $accessToken
            ]
        ]);

    }

    public function detail()
    {
        $user = Auth::user();
        return response()->json(['success' => $user], 200);
    }
}
