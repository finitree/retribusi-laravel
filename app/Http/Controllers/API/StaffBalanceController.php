<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\StaffBalanceResource;
use App\Http\Resources\SuccessResource;
use App\Models\Balance;
use App\Models\Staff;
use Illuminate\Http\Request;

class StaffBalanceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $staff = Staff::where('user_id', auth()->id())->first();
        $balances = Balance::where('staff_id', $staff->id)->get()->sortDesc()->values();
        return StaffBalanceResource::collection($balances);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $staff = Staff::where('user_id', auth()->id())->first();

//        if ($request->amount <= $staff->balance) {
        Balance::create([
            'staff_id' => $staff->id,
            'type' => 'out',
            'amount' => $request->setor_amount,
            'staff_amount' => $request->staff_amount,
            'notes' => $request->notes
        ]);

        $staff->balance = $staff->balance - ($request->setor_amount + $request->staff_amount);
        $staff->save();
        return new SuccessResource([]);
//        } else {
//            abort(422, "Uang tidak cukup");
//        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
