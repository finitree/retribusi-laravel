<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\CompanyOperationalSchedule;
use Illuminate\Http\Request;

class CompanyOperationalScheduleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\CompanyOperationalSchedule  $companyOperationalSchedule
     * @return \Illuminate\Http\Response
     */
    public function show(CompanyOperationalSchedule $companyOperationalSchedule)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\CompanyOperationalSchedule  $companyOperationalSchedule
     * @return \Illuminate\Http\Response
     */
    public function edit(CompanyOperationalSchedule $companyOperationalSchedule)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\CompanyOperationalSchedule  $companyOperationalSchedule
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CompanyOperationalSchedule $companyOperationalSchedule)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\CompanyOperationalSchedule  $companyOperationalSchedule
     * @return \Illuminate\Http\Response
     */
    public function destroy(CompanyOperationalSchedule $companyOperationalSchedule)
    {
        //
    }
}
