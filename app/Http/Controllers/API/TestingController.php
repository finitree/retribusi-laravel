<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Company;
use App\Models\Transaction;
use App\Models\TransactionDetail;
use Barryvdh\DomPDF\Facade\Pdf;
use Webklex\PDFMerger\Facades\PDFMergerFacade as PDFMerger;

class TestingController extends Controller
{

    public function migrateCompanySubscription()
    {
        $companies = Company::latest()->get();
        foreach ($companies as $company) {
            $company->subscription_amount = $company->subscription->subscription_amount;
            $company->save();
        }
        dd($companies);
    }

    public function checkTransaction()
    {
        $transactions = Transaction::get();
        foreach ($transactions as $tr) {
            $tr->money_pay = $tr->total_amount;
            $tr->money_changes = 0;
            $tr->save();
        }

//        $company = Company::find(25);
//        $total = $company->next_transaction->subscription_amount * $company->next_transaction->subscription_qty;
//        return $total;
//
//        $unpaid = CompanyUnpaidTransaction::where('company_id', 25)->get()->count();
//        return $unpaid;
//        $weekMap = [
//            0 => 7,
//            1 => 1,
//            2 => 2,
//            3 => 3,
//            4 => 4,
//            5 => 5,
//            6 => 6,
//        ];
//        $currentTime = Carbon::parse(Carbon::now()->format('H:i'))->addHours(8);
//        $day = Carbon::now()->addDays(1)->dayOfWeek;
//        return $weekMap[$day];
//        return $day . " - " . $dayAddHour;
//        $currentSchedule = CompanyOperationalSchedule::where('company_id', 20)->where('day', $day)->first();
//        return $currentSchedule;
////        $currentMonth = Carbon::now()->format('m');
////        $companies = Company::active()->get();
////        $openedCompanies = $companies->filter(function ($company) {
////            $day = Carbon::now()->addHours(8)->dayOfWeek;
////            $currentSchedule = CompanyOperationalSchedule::where('company_id', $company->id)->where('day', $day)->first();
////            return ($company->company_close->is_cuti == false) && (!$currentSchedule->is_close);
////        })->values();
////        $arr = [];
////        foreach ($openedCompanies as $company) {
////            $subscriptionType = strtolower($company->subscription->subscription_type->category);
////            $newObj = new NextTransaction();
////            $newObj->company_id = $company->id;
////            $newObj->name = $company->name;
////            $newObj->status = $company->transactionStatus;
////            $newObj->subscription_type = $subscriptionType;
////            $newObj->next_transaction = $company->nextTransaction;
//////            $newObj->last_transaction = $company->lastTransaction;
////            if ($company->transactionStatus == Transaction::UNPAID) {
////                if ($subscriptionType == "harian") {
////                    CompanyUnpaidTransaction::create([
////                        'company_id' => $company->id,
////                        'subscription_type_id' => $company->subscription->subscription_type_id,
////                        'company_scale_id' => $company->subscription->company_scale_id,
////                        'amount' => $company->subscription->subscription_amount,
////                        'unpaid_at' => Carbon::parse($company->nextTransaction->date)
////                    ]);
////                } else {
////                    $unpaidDate = CompanyUnpaidTransaction::where('company_id', $company->id)->whereMonth('created_at', $currentMonth)->latest()->first();
////                    if (!$unpaidDate) {
////                        CompanyUnpaidTransaction::create([
////                            'company_id' => $company->id,
////                            'subscription_type_id' => $company->subscription->subscription_type_id,
////                            'company_scale_id' => $company->subscription->company_scale_id,
////                            'amount' => $company->subscription->subscription_amount,
////                            'unpaid_at' => Carbon::parse($company->nextTransaction->date)
////                        ]);
////                    }
////                }
////                array_push($arr, $newObj);
////            }
////        }
////        return "ANJAY";
//
//
////        return Carbon::now()->addHours(8);
//        $currentTime = Carbon::parse(Carbon::now()->format('H:i'))->addHours(8);
//        $day = Carbon::now()->addHours(8)->dayOfWeek;
//        $currentSchedule = CompanyOperationalSchedule::where('company_id', 37)->where('day', $day)->first();
//        $open = Carbon::parse($currentSchedule->opening_hours)->addHours(8);
//        $close = Carbon::parse($currentSchedule->closing_hours)->addHours(8);
//        $isScheduleOpen = ($open->lessThan($currentTime)) && ($close->greaterThan($currentTime));
//        $isOpenBySchedule = $currentSchedule->is_open == 1 && $isScheduleOpen;
//        if ($isOpenBySchedule) {
//            return "open";
//        } else {
//            return "close";
//        }
//        return "Schedule " . $isScheduleOpen;
//        return $currentSchedule;
////        insert operational schedule
//        $companies = Company::first();
//        return $companies;
//
//        foreach ($companies as $company) {
//            CompanyOperationalSchedule::create([
//                'company_id' => $company->id,
//                'day' => 1,
//                'opening_hours' => '00:00',
//                'closing_hours' => '23:59',
//                'is_open' => true
//            ]);
//
//            CompanyOperationalSchedule::create([
//                'company_id' => $company->id,
//                'day' => 2,
//                'opening_hours' => '00:00',
//                'closing_hours' => '23:59',
//                'is_open' => true
//            ]);
//            CompanyOperationalSchedule::create([
//                'company_id' => $company->id,
//                'day' => 3,
//                'opening_hours' => '00:00',
//                'closing_hours' => '23:59',
//                'is_open' => true
//            ]);
//            CompanyOperationalSchedule::create([
//                'company_id' => $company->id,
//                'day' => 4,
//                'opening_hours' => '00:00',
//                'closing_hours' => '23:59',
//                'is_open' => true
//            ]);
//            CompanyOperationalSchedule::create([
//                'company_id' => $company->id,
//                'day' => 5,
//                'opening_hours' => '00:00',
//                'closing_hours' => '23:59',
//                'is_open' => true
//            ]);
//            CompanyOperationalSchedule::create([
//                'company_id' => $company->id,
//                'day' => 6,
//                'opening_hours' => '00:00',
//                'closing_hours' => '23:59',
//                'is_open' => true
//            ]);
//            CompanyOperationalSchedule::create([
//                'company_id' => $company->id,
//                'day' => 7,
//                'opening_hours' => '00:00',
//                'closing_hours' => '23:59',
//                'is_open' => true
//            ]);
//        }
//        return new ModelResource([]);
//
////        $unpaidDate = CompanyUnpaidTransaction::where('company_id', 22)->whereMonth('created_at', '03')->latest()->first();
////        if (!$unpaidDate) {
////            return "TIDAK ADA TRANSAKSI";
////        }
////        return $unpaidDate;
//
//
////        $currentMonth = Carbon::now()->format('m');
////        $companies = Company::active()->get();
////        $openedCompanies = $companies->filter(function ($company) {
////            return $company->company_close->is_close == false;
////        })->values();
////        $arr = [];
////        foreach ($openedCompanies as $company) {
////            $subscriptionType = strtolower($company->subscription->subscription_type->category);
////            $newObj = new NextTransaction();
////            $newObj->company_id = $company->id;
////            $newObj->name = $company->name;
////            $newObj->status = $company->transactionStatus;
////            $newObj->subscription_type = $subscriptionType;
////            $newObj->next_transaction = $company->nextTransaction;
//////            $newObj->last_transaction = $company->lastTransaction;
////            if ($company->transactionStatus == Transaction::UNPAID) {
////                if ($subscriptionType == "harian") {
////                    CompanyUnpaidTransaction::create([
////                        'company_id' => $company->id,
////                        'subscription_type_id' => $company->subscription->subscription_type_id,
////                        'company_scale_id' => $company->subscription->company_scale_id,
////                        'amount' => $company->subscription->subscription_amount,
////                        'unpaid_at' => Carbon::parse($company->nextTransaction->date)
////                    ]);
////                } else {
////                    $unpaidDate = CompanyUnpaidTransaction::where('company_id', $company->id)->whereMonth('created_at', $currentMonth)->latest()->first();
////                    if (!$unpaidDate) {
////                        CompanyUnpaidTransaction::create([
////                            'company_id' => $company->id,
////                            'subscription_type_id' => $company->subscription->subscription_type_id,
////                            'company_scale_id' => $company->subscription->company_scale_id,
////                            'amount' => $company->subscription->subscription_amount,
////                            'unpaid_at' => Carbon::parse($company->nextTransaction->date)
////                        ]);
////                    }
////                }
////                array_push($arr, $newObj);
////            }
////        }
////        return $arr;
////        return $companies;
//
//
////        Delete
////        $unpaid = CompanyUnpaidTransaction::get();
////        foreach ($unpaid as $i){
////            $i->delete();
////        }
    }

    function download_remote_file($file_url, $save_to)
    {
        $content = file_get_contents($file_url);
        file_put_contents($save_to, $content);
    }

    public function transaction_pdf()
    {
        $transactions = Transaction::with('company', 'staff')->latest()->get();
        return view("pdf.transaction_pdf", compact("transactions"));
    }

    public function company_agreement_export($id)
    {
        $company = Company::with('user')->find($id);
//        return view('pdf.company_agreement', compact("company"));
        $pdf = PDF::loadView('pdf.company_agreement', ['company' => $company]);
        return $pdf->stream();
    }

    public function migrate_company_id()
    {
        $transactions = Transaction::get();
        foreach ($transactions as $tr) {
            $trDetails = TransactionDetail::where('transaction_id', $tr->id)->get();
            foreach ($trDetails as $trDetail) {
                $trDetail->company_id = $tr->company_id;
                $trDetail->save();
            }
        }
        dd("migrated");
    }

    public function company_export($id)
    {
        $oMerger = PDFMerger::init();
        $company = Company::with('user')->find($id);
        $dataPdf = "pdf/" . $company->id . ".pdf";
        $documentPdf = "pdf/document_" . $company->id . ".pdf";
        $agreementPdf = "pdf/agreement_" . $company->id . ".pdf";
        $pdf = PDF::loadView('pdf.company_detail', ['company' => $company]);
        $pdf->render();
        $output = $pdf->output();
        file_put_contents($dataPdf, $output);
        $oMerger->addPDF($dataPdf);
        if ($company->documents) {
            $this->download_remote_file($company->documents, $documentPdf);
            $oMerger->addPDF($documentPdf);
        }
        if ($company->agreement_document) {
            $this->download_remote_file($company->agreement_document, $agreementPdf);
            $oMerger->addPDF($agreementPdf);
        }

        $oMerger->merge();
        $oMerger->save(public_path($dataPdf));
//        $oMerger->download();
//        return redirect()->away(public_path($dataPdf));
        return response()->file(
            public_path($dataPdf)
        );
//        return $oMerger->stream();
    }
}
