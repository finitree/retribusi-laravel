<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\CompanyPatchRequest;
use App\Http\Requests\CompanyPostRequest;
use App\Http\Resources\CompanyResource;
use App\Http\Resources\SuccessResource;
use App\Models\Company;
use App\Models\CompanyOperationalSchedule;
use App\Models\Subscription;
use Illuminate\Support\Facades\DB;

class OwnerCompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $companies = Company::ownership()->latest()->get();
        return CompanyResource::collection($companies);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(CompanyPostRequest $request)
    {
        DB::beginTransaction();
        try {
            $subscription = Subscription::where('company_type_id', $request->company_type_id)
                ->where('subscription_type_id', $request->subscription_type_id)->first();
            $subscription_id = $subscription->id;

            $company = Company::create([
                'user_id' => auth()->id(),
                'subscription_id' => $subscription_id,
                'name' => $request->name,
                'latitude' => $request->latitude,
                'longitude' => $request->longitude,
                'address' => $request->address,
                'photos' => $request->photos,
                'documents' => $request->documents,
                'subscription_amount' => $subscription->subscription_amount,
                'company_type_id' => $request->company_type_id,
                'status' => Company::WAIT_VERIFIED,
            ]);

            foreach ($request->company_operational_schedule as $operational) {
                CompanyOperationalSchedule::create([
                    'company_id' => $company->id,
                    'day' => $operational['day'],
                    'opening_hours' => $operational['opening_hours'],
                    'closing_hours' => $operational['closing_hours'],
                    'is_open' => $operational['is_open']
                ]);
            }

            $response = Company::latest()->first();
            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
            abort(500, $e->getMessage());
        }
        return new CompanyResource($response);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show(Company $company)
    {
        $response = Company::find($company->id);
        return new CompanyResource($response);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(CompanyPatchRequest $request, Company $company)
    {
        $company->update([
            'name' => $request->name,
            'address' => $request->address,
            'photos' => $request->photos,
            'documents' => $request->documents,
            'company_type_id' => $request->company_type_id
        ]);

        foreach ($request->company_operational_schedule as $operational) {
            $obj = CompanyOperationalSchedule::where('company_id', $company->id)->where('day', $operational['day'])->first();
            $obj->opening_hours = $operational['opening_hours'];
            $obj->closing_hours = $operational['closing_hours'];
            $obj->is_open = $operational['is_open'];
            $obj->save();
        }


        $response = Company::find($company->id);
        return new CompanyResource($response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Company $company)
    {
        $company->delete();
        return new SuccessResource([]);
    }
}
