<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\ModelResource;
use App\Models\Company;
use App\Models\CompanyUnpaidTransaction;
use App\Models\helper\OwnerTransaction;
use App\Models\Transaction;
use App\Models\TransactionDetail;
use Carbon\Carbon;
use Illuminate\Http\Request;

class OwnerTransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $array = array();
        if ($request->company_id) {
            $companyId = $request->company_id;
            $company = Company::find($companyId);
            if ($request->type == Transaction::PAID) {
                $transactions = TransactionDetail::where('company_id', $companyId)
                    ->when($request->date_from, function ($q) use ($request) {
                        $from = date($request->date_from);
                        $to = Carbon::parse($request->date_to)->addDay();
                        return $q->whereBetween('transaction_at', [$from, $to]);
                    })
                    ->with('company')
                    ->latest()->get();
                // return $transactions;
                foreach ($transactions as $transaction) {
                    $tr = new OwnerTransaction();
                    $tr->company_name = $transaction->company->name;
                    $tr->amount = $transaction->amount;
                    $tr->date = Carbon::parse($transaction->transaction_at);
//                    $tr->money_pay = $transaction->money_pay;
//                    $tr->money_changes = $transaction->money_changes;
                    array_push($array, $tr);
                }
            } else if ($request->type == Transaction::UNPAID) {
                $unpaids = CompanyUnpaidTransaction::where('company_id', $company->id)
                    ->when($request->date_from, function ($q) use ($request) {
                        $from = date($request->date_from);
                        $to = Carbon::parse($request->date_to)->addDay();
                        return $q->whereBetween('unpaid_at', [$from, $to]);
                    })
                    ->get();
                foreach ($unpaids as $unpaid) {
                    $tr = new OwnerTransaction();
                    $tr->company_name = $company->name;
                    $tr->amount = $unpaid->amount;
                    $tr->date = Carbon::parse($unpaid->unpaid_at);
                    array_push($array, $tr);
                }
            }
        } else {
            $companies = auth()->user()->companies->where('status', Company::VERIFIED)->reverse();
            $companiesId = $companies->map(function ($company) {
                return $company->id;
            });
            if ($request->type == Transaction::PAID) {
                $transactions = TransactionDetail::whereIn('company_id', $companiesId)
                    ->when($request->date_from, function ($q) use ($request) {
                        $from = date($request->date_from);
                        $to = Carbon::parse($request->date_to)->addDay();
                        return $q->whereBetween('transaction_at', [$from, $to]);
                    })
                    ->with('company')
                    ->latest()->get();

                foreach ($transactions as $transaction) {
                    $tr = new OwnerTransaction();
                    $tr->company_name = $transaction->company->name;
                    $tr->amount = $transaction->amount;
                    $tr->date = Carbon::parse($transaction->transaction_at);
                    array_push($array, $tr);
                }
            } else if ($request->type == Transaction::UNPAID) {
                foreach ($companies as $company) {
                    $unpaids = CompanyUnpaidTransaction::where('company_id', $company->id)
                        ->when($request->date_from, function ($q) use ($request) {
                            $from = date($request->date_from);
                            $to = Carbon::parse($request->date_to)->addDay();
                            return $q->whereBetween('unpaid_at', [$from, $to]);
                        })
                        ->get();
                    foreach ($unpaids as $unpaid) {
                        $tr = new OwnerTransaction();
                        $tr->company_name = $company->name;
                        $tr->amount = $unpaid->amount;
                        $tr->date = Carbon::parse($unpaid->unpaid_at);
                        array_push($array, $tr);
                    }
                }
            }
        }
        $collec = collect($array);
        return ModelResource::collection($collec->sortByDesc('date')->values());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
