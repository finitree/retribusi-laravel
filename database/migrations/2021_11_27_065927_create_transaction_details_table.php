<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaction_details', function (Blueprint $table) {
            $table->id();
            $table->foreignId('transaction_id')->nullable();
            $table->foreignId('subscription_id')->nullable();
            $table->foreignId('penalty_id')->nullable();
            $table->integer('subscription_amount')->nullable();
            $table->integer('penalty_amount')->nullable();
            $table->integer('subscription_qty')->nullable();
            $table->integer('penalty_qty')->nullable();
            $table->integer('transaction_amount')->nullable();
            $table->timestamps();

            $table->foreign('transaction_id')->references('id')->on('transactions')->onDelete('CASCADE');
            $table->foreign('subscription_id')->references('id')->on('subscriptions')->onDelete('CASCADE');
            $table->foreign('penalty_id')->references('id')->on('penalties')->onDelete('CASCADE');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaction_details');
    }
}
