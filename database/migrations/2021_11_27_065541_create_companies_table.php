<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->nullable();
            $table->foreignId('tempekan_id')->nullable();
            $table->foreignId('company_scale_id')->nullable();
            $table->foreignId('subscription_id')->nullable();
            $table->string('name')->nullable();
            $table->double('latitude');
            $table->double('longitude');
            $table->string('address')->nullable();
            $table->string('photos')->nullable();
            $table->string('documents')->nullable();
            $table->enum('status', ['verified', 'wait_verified', 'blocked'])->nullable();
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('CASCADE');
            $table->foreign('tempekan_id')->references('id')->on('tempekans')->onDelete('CASCADE');
            $table->foreign('company_scale_id')->references('id')->on('company_scales')->onDelete('CASCADE');
            $table->foreign('subscription_id')->references('id')->on('subscriptions')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companies');
    }
}
