<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTempekansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tempekans', function (Blueprint $table) {
            $table->id();
            $table->foreignId('banjar_id')->nullable();
            $table->string('name');
            $table->timestamps();

            $table->foreign('banjar_id')->references('id')->on('banjars')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tempekans');
    }
}
