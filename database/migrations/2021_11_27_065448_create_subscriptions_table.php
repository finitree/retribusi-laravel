<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubscriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subscriptions', function (Blueprint $table) {
            $table->id();
            $table->foreignId('subscription_type_id')->nullable();
            $table->foreignId('company_scale_id')->nullable();
            $table->integer('subscription_amount')->nullable();
            $table->timestamps();

            $table->foreign('subscription_type_id')->references('id')->on('subscription_types')->onDelete('CASCADE');
            $table->foreign('company_scale_id')->references('id')->on('company_scales')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subscriptions');
    }
}
