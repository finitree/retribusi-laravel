<?php

namespace Database\Seeders;

use App\Models\CompanyScale;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\App;

class CompanyScaleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $companyScale = new CompanyScale();
        $companyScale->insert([
            [
                'scale' => 'Kecil'
            ],
            [
                'scale' => 'Menengah'
            ],
            [
                'scale' => 'Besar'
            ],
        ]);
    }
}
